<?php

class Api_Erp {

    const GET = 0;
    const POST = 1;
    const TIMEOUT = 60;

    private static $configs = array(
        'development' => array(
            'path' => 'https://esb-uat.vng.com.vn:8888',
            'key'  => 'Basic bWlzZXNiOlZuZ0AxMjM=',
        ),
        'production' => array(
            'path' => 'https://esb.vng.com.vn:8443',
            'key'  => 'Basic bWlzZXNiOlZuZ0AxMjM=',
        )
    );

    public static function getConfig($env = APPLICATION_ENV) {
        return (isset(self::$configs[$env]) ? self::$configs[$env] : self::$configs['development']);
    }

    public static function mergeQueryString($url, $params = null) {
        return $url . (empty($params) ? '' : (parse_url($url, PHP_URL_QUERY) === null ? '?' : '&') . http_build_query($params));
    }

    public static function request($api, $data, $method = self::GET) {
        $config = self::getConfig();
        $url = $config['path'] . $api;
        $key = $config['key'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIE, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIESESSION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: '.$key
        ));
        if ($method == self::POST) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        } else {
            $url = self::mergeQueryString($url, $data);
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        $result = curl_exec($ch);

        $error = curl_error($ch);

        curl_close($ch);

        $result_decode = Zend_Json::decode($result, Zend_Json::TYPE_OBJECT);

        if (!$result || !isset($result_decode->data) || $result_decode->code !=1) {
            return false;
        }

        try {
            return $result_decode->data;
        } catch (Exception $e) {
        }

        return false;
    }

     public static function postform($api, $data, $method = self::GET) {
        $config = self::getConfig();
        $url = $config['path'] . $api;
        $key = $config['key'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIE, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIESESSION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: '.$key
        ));
        if ($method == self::POST) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        } else {
            $url = self::mergeQueryString($url, $data);
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        $result = curl_exec($ch);

        $error = curl_error($ch);
        curl_close($ch);


        $result_decode = Zend_Json::decode($result, Zend_Json::TYPE_OBJECT);

        if (!$result) {
            return false;

        }

        return $result_decode;
    }

    public static function test($data) {
        return self::request('/hrsystem/http/vhr/getProgramEvent', $data, self::POST);
    }

    public static function getInterface($data) {
        return self::request('/hrsystem/http/vhr/getTypicalFace', $data, self::POST);
    }

    public static function getRecruitmentProgram($data = null) {
        if($data == null){
            $data = array('fields' => array("name","short_description","image","id"));
            return self::request('/hrsystem/http/vhr/getRecruitmentProgram', $data,  self::POST);
        }

        return self::request('/hrsystem/http/vhr/getRecruitmentProgram', $data, self::POST);
    }

    public static function getProgramEvent($data) {
       $result = self::request('/hrsystem/http/vhr/getProgramEvent', $data, self::POST);
       if($result &&  isset($result[0]->description)) return $result[0];
       return false;
    }

    public static function receiveApplicationFromFe($data){
        return self::postform('/hrsystem/http/vhr/receiveApplicationFromFeV1', $data, self::POST);
    }

    public static function getSubscribe($data){
        return self::postform('/hrsystem/http/vhr/getSubscribe', $data, self::POST);
    }

    public static function activeEmailSubscription($data){
        return self::postform('/hrsystem/http/vhr/activeEmailSubscription', $data, self::POST);
    }

    public static function getSchool($data = null){
        if($data == null){
            $data = new stdClass();
            return self::request('/hrsystem/http/vhr/getCareerProgram', $data, self::POST);
        }

        return self::request('/hrsystem/http/vhr/getCareerProgram', $data, self::POST);
    }

    public static function getDynamicInfo($data = null){
        if($data == null){
            $data = new stdClass();
            return self::request('/hrsystem/http/vhr/getDynamicInfo', $data, self::POST);
        }

        return self::request('/hrsystem/http/vhr/getDynamicInfo', $data, self::POST);
    }



}

