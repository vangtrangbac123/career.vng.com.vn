<?php

class Utility_Image {

    /**
     * @param  $_FILES $file
     * @return string $imageUrl | False
     */
    public static function uploadImageToServer($file) {

        try {

            // prepare
            $fileName = pathinfo($file['name'], PATHINFO_FILENAME); // PHP 5.2.0

            // read file
            $handle = fopen($file['tmp_name'], 'r');
            $fileData = fread($handle, $file['size']);
            fclose($handle);

            // request data
            $params = array(
                'file_data' => $fileData,
                'file_name' => $fileName,
                'seckey'    => md5($fileName . Config::API_IMG_KEY),
            );

            // request
            $ch = curl_init(Config::API_IMG_UPLOAD);
            curl_setopt($ch, CURLOPT_HTTP_VERSION,   CURL_HTTP_VERSION_1_0);
            curl_setopt($ch, CURLOPT_TIMEOUT,        60);
            curl_setopt($ch, CURLOPT_HEADER,         false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST,           true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($params, null, '&'));
            curl_setopt($ch, CURLOPT_PROXY,          Config::PROXY_IP);
            curl_setopt($ch, CURLOPT_PROXYPORT,      Config::PROXY_PORT);

            // respone
            $response = curl_exec($ch);

            if (!empty($response)) {

                $response = json_decode($response, true);

                if (is_array($response) && isset($response[0])
                    && isset($response[0]['url']) && !empty($response[0]['url'])) {

                    return $response[0]['url'];
                }
            }

        } catch (Zend_Exception $e) {
            // TODO: write log
        }

        return false;
    }

    /**
     * @param  string $imageUrl
     * @param  array  $params (url, height, width, crop, quality)
     * @return string $newImageUrl
     */
    public static function requestResize($params) {

        try {

            if (!isset($params['url'])) return false;

            $config = App::getConfig('proxy');
            $url = 'http://upload.123phim.vn/go123/resize/?' . http_build_query($params);
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT,        60);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_PROXY,          $config['host']);
            curl_setopt($ch, CURLOPT_PROXYPORT,      $config['port']);

            $data = curl_exec($ch);
            curl_close($ch);

            if (empty($data)) return $imageUrl;

            $data = json_decode($data, true);

            if (is_array($data) && isset($data['src']) && !empty($data['src'])
                && isset($data['error']) && $data['error'] == 0) {

                return $data['src'];
            }
        } catch (Zend_Exception $e) {
            // TODO: write log
        }

        return $params['url'];

    }

    public static function resize($imageUrl, $width, $height, $quality = 80, $crop = 1){
        return self::requestResize(array(
            'url'     => $imageUrl,
            'width'   => $width,
            'height'  => $height,
            'quality' => $quality,
            'crop'    => $crop,
        ));
    }

    public static function ranger($url) {
        $config = App::getConfig('proxy_alt');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_PROXY,          $config['host']);
        curl_setopt($ch, CURLOPT_PROXYPORT,      $config['port']);
        $result = curl_exec($ch);
        $img = imagecreatefromstring($result);
        return $img;
    }
}
