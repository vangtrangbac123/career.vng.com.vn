<style>
.debug h3:hover {
    cursor: pointer;
    background-color: rgb(255, 219, 155);
}
.debug .stt {
    width: 50px;
    text-align: center;
}
.debug .info-key {
    width: 150px;
    text-align: right;
    padding-right: 10px;
}
.debug table.border td {
    border: 1px solid #ccc;
}
.debug .db .key {
    width: 100px;
}
.debug .db {
    width: 100%;
}
.debug .db .alert {
    color: red;
}
.debug .action {
    text-align: right;
    width: 200px;
}
.debug a {
    color: inherit;
}
.debug a:hover {
    color: inherit;
    background-color: rgb(255, 219, 155);
}
.debug thead {
    background-color: #ddd;
}
.debug .cache-group {
    width: 100px;
}
.debug .cache-time {
    width: 100px;
}
.debug .cache-memory {
    width: 200px;
}
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
$(function() {
    $('.clicktoshow').click(function() {
        $(this).next().toggle();
    });
});
</script>
<pre class="debug">
<table>
    <tbody>
        <tr>
            <td class="info-key">APPLICATION_ENV</td>
            <td class="value"><?php echo APPLICATION_ENV?></td>
        </tr>
        <tr>
            <td class="info-key">PROCESS_TIME</td>
            <td class="value"><?php echo $PROCESS_TIME?></td>
        </tr>
        <tr>
            <td class="info-key">MEMORY_USAGE</td>
            <td class="value"><?php echo $MEMORY_USAGE?></td>
        </tr>
        <tr>
            <td class="info-key">MEMCACHE_ENABLE</td>
            <td class="value"><?php
            if (My_Memcache::getInstance()->isEnable()) {
                ?>1 (<a href="#">Disable</a>)<?php
            } else {
                ?>0 (<a href="#">Enable</a>)<?php
            }
            ?></td>
        </tr>
        <tr>
            <td class="info-key">MEMCACHE_CONNECTED</td>
            <td class="value"><?php echo My_Memcache::getInstance()->isConnected()?></td>
        </tr>
        <tr>
            <td class="info-key">STATIC_VERSION</td>
            <td class="value"><?php echo Config::STATIC_VERSION?></td>
        </tr>
    </tbody>
</table>

<?php $profile = My_Profiler::getQueryProfiles(); ?>
<h3 class="clicktoshow">DB (<?php echo $profile['total_query']?> hits / <?php echo round($profile['total_sec'], 4)?> sec)</h3>
<table class="border db">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th>Time</th>
            <th>Query</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($profile['queries'] as $i => $query): ?>
        <tr class="<?php echo ($query['time'] > 0.05) ? 'alert' : ''?>">
            <td class="stt"><?php echo ($i + 1)?></td>
            <td class="key"><?php echo $query['time']?></td>
            <td class="value"><?php echo str_replace("                ", '', $query['sql'])?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php $profile = My_Memcache::getProfiles(); ?>
<h3 class="clicktoshow">Caches (<?php echo Utility_Converter::toMemorySize($profile['total_memory'])?> / <?php echo $profile['total_query']?> hits / <?php echo round($profile['total_sec'], 4)?> sec)</h3>
<table class="border db">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th class="cache-group">Group</th>
            <th class="cache-time">Time</th>
            <th class="cache-memory">Memory</th>
            <th class="cache-key">Key</th>
            <th class="cache-group"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach ($profile['queries'] as $type => $rows) foreach ($rows as $query): ?>
        <tr class="<?php echo ($query['time'] > 0.05) ? 'alert' : ''?>">
            <td class="stt"><?php echo ++$i?></td>
            <td class="cache-group"><?php echo $type?></td>
            <td class="cache-time"><?php echo round($query['time'], 4)?></td>
            <td class="cache-memory"><?php echo Utility_Converter::toMemorySize($query['memory'])?></td>
            <td class="cache-key"><?php echo $query['key']?></td>
            <td class="action"><a href="#">Delete</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<h3 class="clicktoshow">Included Files (<?php echo count($INCLUDED_FILES)?>)</h3>
<div style="display:none">
<?php print_r($INCLUDED_FILES)?>
</div>
</pre>