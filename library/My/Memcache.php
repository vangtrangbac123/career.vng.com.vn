<?php

class My_Memcache {

    protected static $_instance;
    protected $logs = array();
    protected $isLog = Config::MEMCACHE_LOG;
    protected $isUpdate = false;
    protected $isEnable = true;
    protected $isConnected = false;
    protected $memcache;

    public function __construct()
    {
        $this->isEnable = !$this->flag('cache', 0);
        $this->isUpdate = $this->flag('cache_refresh', 1);
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function getProfiles()
    {
        $time = 0;
        $memory = 0;
        $logs = self::getInstance()->getLogs();
        $totalQuery = count($logs);
        $queries = array();
        foreach ($logs as $log) {
            $time += $log['time'];
            $memory += $log['memory'];
            $type = $log['type'];
            if (!isset($queries[$type])) $queries[$type] = array();
            $queries[$type][] = $log;
        }

        return array(
            'total_query'  => $totalQuery,
            'total_memory' => $memory,
            'total_sec'    => $time,
            'queries'      => $queries,
        );
    }

    private function connect()
    {
        $this->isConnected = false;
        $this->memcache = new Memcache();

        if (!$this->isEnable) {
            return;
        }

        if ($this->isLog) {
            $t1 = microtime(true);
        }

        try {
            if (@$this->memcache->connect(Config::MEMCACHE_IP, Config::MEMCACHE_PORT, 1)) {
                $this->isConnected = true;
            }
        } catch (Exception $e) {
        }

        if ($this->isLog) {
            $this->logs[] = array(
                'type'   => 'connect',
                'time'   => microtime(true) - $t1,
                'memory' => 0,
                'key'    => Config::MEMCACHE_IP . ':' . Config::MEMCACHE_PORT,
                'data'   => $this->isConnected,
            );
        }
    }

    public function getLogs()
    {
        return $this->logs;
    }

    public function set($key, $value, $expire = Config::MEMCACHE_TIME)
    {
        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected) {
            return false;
        }

        if (!$this->isLog) {
            return $this->memcache->set($key, $value, MEMCACHE_COMPRESSED, $expire);
        }

        $t1 = microtime(true);
        $results = $this->memcache->set($key, $value, MEMCACHE_COMPRESSED, $expire);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'set',
            'time'   => $t2 - $t1,
            'memory' => strlen(serialize($value)),
            'expire' => $expire,
            'key'    => $key,
            'data'   => $value,
        );

        return $results;
    }

    public function get($key)
    {
        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected || $this->isUpdate) {
            return false;
        }

        if (!$this->isLog) {
            return $this->memcache->get($key);
        }

        $t1 = microtime(true);
        $results = $this->memcache->get($key);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'get',
            'time'   => $t2 - $t1,
            'memory' => strlen(serialize($results)),
            'key'    => $key,
            'data'   => $results,
        );

        return $results;
    }

    public function delete($key)
    {
        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected) {
            return false;
        }

        if (!$this->isLog) {
            return $this->memcache->delete($key);
        }

        $t1 = microtime(true);
        $results = $this->memcache->delete($key);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'delete',
            'time'   => $t2 - $t1,
            'key'    => $key,
            'data'   => $results,
        );

        return $results;
    }

    function deletekeyall($prefix = false) {

        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected) {
            return false;
        }

        $t1 = microtime(true);
        $results = $this->memcache->flush();
        $t2 = microtime(true);
        return $results;

    }

    private function flag($flag, $value = 1)
    {
        return (isset($_GET[$flag]) && $_GET[$flag] == $value) || (isset($_COOKIE[$flag]) && $_COOKIE[$flag] == $value);
    }

    public function isConnected()
    {
        return $this->isConnected;
    }

    public function isEnable()
    {
        return $this->isEnable;
    }

}