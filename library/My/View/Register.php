<?php

class My_View_Register {

    public static function textbox($data){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                  <input type="text" autocomplete="off" name="member['.$data->field_model.']" data-attribute="'.$data->attribute.'" data-error="'.$data->error.' '.$data->name.'" data-required="'.$is_required.'" value="" placeholder="'.$data->placeholder.'">
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

    public static function textboxfloat($data){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                  <input type="text" autocomplete="off" name="member['.$data->field_model.']" data-attribute="'.$data->field_type.'" data-error="'.$data->error.' '.$data->name.'" data-required="'.$is_required.'" value="" placeholder="'.$data->placeholder.'">
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

    public static function datetime($data){
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                  <input type="text" readonly name="member['.$data->field_model.']" data-error="'.$data->error.' '.$data->name.'" data-required="'.$is_required.'" value="" class="datepicker" placeholder="'.$data->placeholder.'">
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

    public static function time($data){
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                  <input type="text" autocomplete="off" name="member['.$data->field_model.']" data-error="'.$data->error.' '.$data->name.'" data-required="'.$is_required.'" value="" class="timepicker" placeholder="'.$data->placeholder.'">
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

    public static function file($data){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                     <input type="hidden" class="url_file" name="file['.$data->field_model.'][url]" value="">
                     <input type="hidden" class="name_file" name="file['.$data->field_model.'][filename]" value="">
                    <form action="/upload/go123">
                     <input type="hidden" name="upload[file]['.$data->field_model.']" value="'.$data->limit_size.'">
                     <input type="hidden" name="upload[filter]['.$data->field_model.']" value="'.$data->filter.'">
                     <input type="file" name="upload['.$data->field_model.']" data-error="'.$data->error.' '.$data->name.'" data-limit= "'.$data->limit_size.'" data-filter="'.$data->filter.'" data-required="'.$is_required.'"  class="file"><br/>
                    <div class="notice">
                        <div class="progressbar"></div>
                        <span>Đang tải 1%</span>
                    </div>
                    <p class="required"></p>
                    <p class="Critical">'.$data->description.'</p>
                    </form>
                  </td>
                </tr>';
    }

    public static function selectbox($data){

        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        $str = "";
        if(isset($data->data))
        foreach ($data->data as $key => $option) {
            $str .= "<option value='".$option->id."'>".$option->name."</option>";
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                  <select  name="member['.$data->field_model.']" class="SelectUI Theme_GP Theme_Human"  data-error="'.$data->error.' '.$data->name.'" data-required="'.$is_required.'" title="Chọn '.$data->name.'">
                        <option value="">Chọn '.$data->name.'</option>
                        '.$str.'
                    </select>
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

    public static function captcha(){

        return '<tr>
                  <td class="LeftText"></td>
                  <td>
                   <img src="/index/gencaptcha" class="captcha_image" height="60px"/>
                   <img src="/layouts/web/img/refresh.png" height="35px" class="captcha_refresh"  />
                  </td>
                </tr>
                <tr>
                  <td class="LeftText">Mã xác nhận (<span style="color:red">*</span>)</td>
                  <td>
                   <input type="text" value="" id="captcha" name="captcha[input]"  class="Capcha">
                   <p class="required"> </p>
                  </td>
                </tr>';
    }

    public static function program_type($id){
        return '<tr>
                  <td class="LeftText"></span> </td>
                  <td>
                  <input type="hidden" name="member[program_type]" value="'.$id.'" >
                  <p class="required">  </p>
                  </td>
                </tr>';
    }
     public static function program_event_id($id){
        return '<tr>
                  <td class="LeftText"></span> </td>
                  <td>
                  <input type="hidden" name="member[program_event_id]" value="'.$id.'" >
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

     public static function checkbox($data){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        $str = "";
        if(isset($data->data))
        foreach ($data->data as $key => $option) {
            $str .= '<input type="checkbox" style="display:inline;margin-left:5px" value="'.$option->id.'" name="member['.$data->field_model.'][]"><label>'.$option->name.'</label>';
        }
        return '<tr>
                  <td class="LeftText">'.$data->name.' <span>'.$required.'</span> </td>
                  <td>
                   '.$str.'
                  </td>
                </tr>';
    }

    public static function button_submit($ques){
      if(!$ques){
        return '<tr>
                  <td></td>
                 <td>
                   <img class="sloading"  src="/layouts/web/img/loading.gif" width="30px" style="display:none;position:absolute;margin-top:-30px;margin-left:10px">
                   <input id="reg_btn" type="button" value="ĐĂNG KÝ" class="BtSubmit" title="">
                  </td>
                </tr>';
      }else{
         return '<tr style="text-align:right">
                  <td></td>
                 <td>
                   <input id="reg_btn_next" type="button" value="Tiếp" class="BtSubmit" title="">
                  </td>
                </tr>';
      }
    }

    public static function button_back(){
        return '<tr>
                  <td></td>
                 <td>
                   <input id="reg_btn_back" type="button" value="Trở lại" class="BtSubmit" title="">
                   <input id="reg_btn_question" type="button" value="ĐĂNG KÝ" class="BtSubmit" title="">
                  </td>
                </tr>';
    }

     public static function popup_success($mess){
        if(!$mess) $mess = 'success';
        return '<tr>
                  <td></td>
                 <td>
                   <input  type="hidden"   name="popup[success]" value="'.$mess.'" title="">
                  </td>
                </tr>';
    }


     public static function popup_fail($mess){
        if(!$mess) $mess = 'fail';
        return '<tr>
                  <td></td>
                 <td>
                   <input  type="hidden"  name="popup[fail]" value="'.$mess.'" title="">
                  </td>
                </tr>';
    }

    public static function event_name($name){
        return '<tr>
                  <td></td>
                 <td>
                   <input name="event_name" type="hidden" value="'.$name.'"  title="">
                  </td>
                </tr>';
    }

    public static function part($part){
         return '<tr>
                  <td class="LeftText"><h8 style="color:red">'.$part->name.'</h8></td>
                  <td>
                   '.$part->description.'
                  </td>
                </tr>';
    }

        public static function title($title){
         return '<tr>
                  <td class="LeftText"></h8></td>
                  <td>
                      <h2 style="color:red">'.$title.'</h2>
                  </td>
                </tr>';
    }

    public static function checkboxquestion($data, $i){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        $str = "";
        if(isset($data->answer_ids))
        foreach ($data->answer_ids as $key => $option) {
            $str .= '<input type="checkbox" style="display:inline" value="'.$option->id.'-'.$option->name.'" name="question['.$data->id.'][]"><label>'.$option->name.'</label><br>';
        }
        return '<tr>
                  <td class="LeftText"></span> </td>
                  <td>
                  <b>Câu'.$i.':</b>'.$data->name.'<span style="color:red">'.$required.'</span> <br>
                   '.$str.'
                  </td>
                </tr>';
    }

    public static function checkboxrichtextquestion($data, $i){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        $str = "";
        if(isset($data->answer_ids))
        foreach ($data->answer_ids as $key => $option) {
            $str .= '<div><input type="checkbox" style="display:inline" class="richtext" data-value="'.$option->id.'-'.$option->name.'" value="'.$option->id.'-'.$option->name.'" name="question['.$data->id.'][]"><label>'.$option->name.'</label><br>';
            $str .= '<textarea style="display:none" class="arearichtext"></textarea></div>';
        }
        return '<tr>
                  <td class="LeftText"></span> </td>
                  <td>
                  <b>Câu'.$i.':</b>'.$data->name.'<span style="color:red">'.$required.'</span> <br>
                   '.$str.'
                  </td>
                </tr>';
    }


    public static function selectquestion($data, $i){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        $str = "";
        if(isset($data->answer_ids))
        foreach ($data->answer_ids as $key => $option) {
            $str .= "<option value='".$option->id.'-'.$option->name."'>".$option->name."</option>";
        }
        return '<tr>
                  <td class="LeftText"></span> </td>
                  <td>
                  <b>Câu'.$i.':</b>'.$data->name.'<span style="color:red">'.$required.'</span> <br>
                  <select  name="question['.$data->id.']" class="SelectUI Theme_GP Theme_Human"  data-error="Vui lòng chọn câu trả lời" data-required="'.$is_required.'" title="'.$data->name.'">
                        <option value="">Chọn câu trả lời</option>
                        '.$str.'
                    </select>
                  <p class="required">  </p>
                  </td>
                </tr>';
    }

    public static function textareaquestion($data, $i){
        $required ="";
        $is_required = "false";

        if($data->is_required){
            $required = "*";
            $is_required = "required";
        }
        return '<tr>
                  <td class="LeftText"></span> </td>
                  <td>
                  <b>Câu'.$i.':</b>'.$data->name.'<span style="color:red">'.$required.'</span><br>
                  <textarea  name="question['.$data->id.']" class="SelectUI Theme_GP Theme_Human"  data-error="Vui lòng chọn câu trả lời" data-required="'.$is_required.'" title="'.$data->name.'">
                  </textarea>
                  <p class="required">  </p>
                  </td>
                </tr>';

    }


}

