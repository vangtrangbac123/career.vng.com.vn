<?php

class My_View_Meta {

    public static function getMeta($view) {

        $list = array();

        $list['student']['fresher']['title']       = 'Chương trình VNG Fresher';
        $list['student']['fresher']['description'] = 'VNG Fresher là chương trình Ðào Tạo & Tuyển Dụng do VNG tổ chức dành cho sinh viên năm cuối tại các trường Ðại học tại Việt Nam';
        $list['student']['fresher']['keywords']    = 'Gioi thieu, VNG Fresher, vng cuoc song, phuc loi';

        $list['student']['fresher']['properties']['og:title']       = 'Chương trình VNG Fresher';
        $list['student']['fresher']['properties']['og:url']         = 'http://fresher.vng.com.vn/';


        $list['student']['index']['title']       = 'Giới thiệu - Chương trình dành cho sinh viên';
        $list['student']['index']['description'] = 'Gioi thieu, VNG Fresher, vng cuoc song, phuc loi';
        $list['student']['index']['keywords']    = 'Gioi thieu, VNG Fresher, vng cuoc song, phuc loi';

        $list['student']['index']['properties']['og:title']       = 'Chương trình dành cho sinh viên';
        $list['student']['index']['properties']['og:url']         = 'http://hrs.vng.com.vn/danh-cho-sinh-vien';


        $list['life']['index']['title']       = 'Cuộc sống tại VNG';
        $list['life']['index']['description'] = 'VNG có hơn 1800 thành viên làm việc tại 3 thành phố lớn nhất Việt Nam phục vụ hơn 20 triệu khách hàng
    ';
        $list['life']['index']['keywords']    = 'vng cuoc song, phuc loi
    ';

        $list['life']['index']['properties']['og:title']       = 'Cuộc sống tại VNG';
        $list['life']['index']['properties']['og:url']         = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);


        $list['job']['index']['title']       = 'Ngành nghề đang tuyển dụng - Tuyển dụng ở VNG - Tuyển dụng việc làm mới nhất ở VNG - Tuyen dung 2015';
        $list['job']['index']['description'] = 'Ngành nghề đang tuyển dụng, Danh sách tuyển dụng các việc làm mới nhất ở VNG. Nộp đơn tuyển dụng 2015, gia nhập VNG ngay hôm nay!';
        $list['job']['index']['keywords']    = 'Ngành nghề đang tuyển dụng, tuyen dung vng, viec lam vng, viec lam moi vng, tuyen dung 2015
    ';

        $list['job']['index']['properties']['og:title']       = 'Ngành nghề đang tuyển dụng - Tuyển dụng ở VNG - Tuyển dụng việc làm mới nhất ở VNG - Tuyen dung 2015';
        $list['job']['index']['properties']['og:url']         = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        $list['job']['search']['title']       = 'Ngành nghề đang tuyển dụng - Tuyển dụng ở VNG - Tuyển dụng việc làm mới nhất ở VNG - Tuyen dung 2015';
        $list['job']['search']['description'] = 'Ngành nghề đang tuyển dụng, Danh sách tuyển dụng các việc làm mới nhất ở VNG. Nộp đơn tuyển dụng 2015, gia nhập VNG ngay hôm nay!';
        $list['job']['search']['keywords']    = 'Ngành nghề đang tuyển dụng, tuyen dung vng, viec lam vng, viec lam moi vng, tuyen dung 2015
    ';

        $list['job']['search']['properties']['og:title']       = 'Ngành nghề đang tuyển dụng - Tuyển dụng ở VNG - Tuyển dụng việc làm mới nhất ở VNG - Tuyen dung 2015';
        $list['job']['search']['properties']['og:url']         = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);


        $list['job']['category']['title']       = 'Ngành nghề đang tuyển dụng - Tuyển dụng ở VNG - Tuyển dụng việc làm mới nhất ở VNG - Tuyen dung 2015';
        $list['job']['category']['description'] = 'Ngành nghề đang tuyển dụng, Danh sách tuyển dụng các việc làm mới nhất ở VNG. Nộp đơn tuyển dụng 2015, gia nhập VNG ngay hôm nay!';
        $list['job']['category']['keywords']    = 'Ngành nghề đang tuyển dụng, tuyen dung vng, viec lam vng, viec lam moi vng, tuyen dung 2015
    ';

        $list['job']['category']['properties']['og:title']       = 'Ngành nghề đang tuyển dụng - Tuyển dụng ở VNG - Tuyển dụng việc làm mới nhất ở VNG - Tuyen dung 2015';
        $list['job']['category']['properties']['og:url']         = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        $list['job']['advise']['title']       = 'Lời khuyên dành cho ứng viên - Lời khuyên cho ứng viên';
        $list['job']['advise']['description'] = 'Lời khuyên dành cho ứng viên - ';
        $list['job']['advise']['keywords']    = 'Loi khuyen danh cho ung vien, VNG Fresher, vng cuoc song, phuc loi
    ';

        $list['job']['advise']['properties']['og:title']       = 'Lời khuyên dành cho ứng viên - Lời khuyên cho ứng viên';
        $list['job']['advise']['properties']['og:url']         = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);


        return isset($list[$view->controllerName][$view->actionName]) ? $list[$view->controllerName][$view->actionName] : false;
    }
}