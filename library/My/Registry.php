<?php

class My_Registry {

    protected static $_instance;
    protected static $_data = array();


    static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    function __construct() {
        
    }
    function getRouter() {
        $router = new Zend_Controller_Router_Rewrite();
        $router->addRoutes(require APPLICATION_PATH . '/configs/routes.php');
        return $router;
    }
}