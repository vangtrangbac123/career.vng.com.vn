<?php

class My_Controller_Dev extends My_Controller_Action {

    function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->getParam('passcode') == 'tpfdev') {
            $_COOKIE['debug'] = 1;
            setcookie('debug', 1, time() + 3600, '/', '.123phim.vn');
            $this->_redirect('/dev/');
        }

        if (App::isProduction() && !App::isDebugging()) {
            $this->_redirect('/');
        }

        $this->_init();
    }

    function _init() {

    }

    function indexAction() {
        echo '<pre>';
        echo 'APPLICATION_ENV: ' . APPLICATION_ENV . PHP_EOL;
        echo 'Server IP: ' . $_SERVER['SERVER_ADDR'] . PHP_EOL;
        echo 'Client IP: ' . $_SERVER['REMOTE_ADDR'] . PHP_EOL;
        echo 'Agent: ' . $_SERVER['HTTP_USER_AGENT'] . PHP_EOL;
        echo 'Mobile: ' . App::isMobile() . PHP_EOL;
        echo 'Cookie: ';
        print_r($_COOKIE);
        echo 'Session: ';
        print_r($_SESSION);
        echo 'xhprof: ' . (extension_loaded('xhprof') ? 'YES' : 'NO') . PHP_EOL;
        echo 'MODULE_PATH: ' . (isset($GLOBALS['application_module_directory']) ? $GLOBALS['application_module_directory'] : '') . PHP_EOL;
    }

    function phpinfoAction() {
        phpinfo();
    }

    function apcAction() {
        include 'My/Monitor/apc.php';
    }

    function memcacheAction() {
        include 'My/Monitor/memcache.php';
    }


    function cacheAction() {
        header('content-type: text/html; charset=utf-8');
        $memcache = My_Memcache::getInstance();
        $memcache->setPrefix('');
        $params = $this->_request->getParams();
        $key = $params['key'];
        switch (isset($params['do']) ? $params['do'] : null) {
            case 'delete':
                if (!is_array($key)) {
                    $key = array($key);
                }
                echo '<pre>';
                foreach ($key as $k) {
                    $results = $memcache->delete($k);
                    echo 'Delete: ' . $k;
                    echo PHP_EOL;
                    echo 'Result: ' . $results;
                    echo PHP_EOL . PHP_EOL;
                }
                echo '</pre>';
                break;

            default:
                $results = $memcache->get($params['key']);
                if (is_string($results)) {
                    $results = htmlentities($results);
                }
                echo '<pre>';
                print_r($results);
                echo '</pre>';
                break;
        }
    }

    function apcuAction() {
        header('content-type: text/html; charset=utf-8');
        $apc = My_Apc::getInstance();
        $apc->setPrefix('');
        $params = $this->_request->getParams();
        $key = $params['key'];
        switch (isset($params['do']) ? $params['do'] : null) {
            case 'delete':
                if (!is_array($key)) {
                    $key = array($key);
                }
                echo '<pre>';
                foreach ($key as $k) {
                    $results = $apc->delete($k);
                    echo 'Delete: ' . $k;
                    echo PHP_EOL;
                    echo 'Result: ' . $results;
                    echo PHP_EOL . PHP_EOL;
                }
                echo '</pre>';
                break;

            default:
                $results = $apc->get($params['key']);
                if (is_string($results)) {
                    $results = htmlentities($results);
                }
                echo '<pre>';
                print_r($results);
                echo '</pre>';
                break;
        }
    }
}
