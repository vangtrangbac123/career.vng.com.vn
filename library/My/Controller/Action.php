<?php

class My_Controller_Action extends Zend_Controller_Action {

    public $auth;
    public $user;
    public $model;

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        $this->setRequest($request)
             ->setResponse($response)
             ->_setInvokeArgs($invokeArgs);
        $this->_helper = new Zend_Controller_Action_HelperBroker($this);

        $this->auth = Zend_Auth::getInstance();
        $this->user = $this->auth->getIdentity();
        $this->model = My_Model_Helper::getInstance();
        $this->moduleName     = $this->_request->getModuleName();
        $this->actionName     = $this->_request->getActionName();
        $this->controllerName = $this->_request->getControllerName();

        $this->init();
    }

    public function postDispatch()
    {
        $this->view->model          = $this->model;
        $this->view->auth           = $this->auth;
        $this->view->user           = $this->user;
        $this->view->head           = new My_View_Head($this->view);
        $this->view->requestUri     = urlencode($_SERVER['REQUEST_URI']);
        $this->view->moduleName     = $this->moduleName;
        $this->view->actionName     = $this->actionName;
        $this->view->controllerName = $this->controllerName;
        $this->_postDispatch();
    }

    public function _postDispatch() {}

    public function returnData($code, $message){
        $arrResult = array('code'=> $code, 'message' => $message);
        echo json_encode($arrResult);die;
    }

    public function returnMessage($code, $rs, $eventName){
        $success = 'Chào bạn,<br><br>Cám ơn bạn đã quan tâm đến '.$eventName.'.
                            <br>Chúng tôi đã nhận được hồ sơ ứng tuyển của bạn. Nếu hồ sơ của bạn phù hợp với chương trình này,
                            chúng tôi sẽ liên hệ với bạn trong vòng 4 tuần.<br><br>Trân trọng,<br>Nhóm Tuyển dụng – phòng Nhân sự<br>';
        $error = 'Có  lỗi! Đăng ký không thành công';

        $message = $error;

        if($rs == 1){
            $message = $success;
        }
        $arrResult = array('code'=> $code, 'message' => $message);
        echo json_encode($arrResult);die;
    }
}