<?php

class My_Model_Abstract extends Zend_Db_Table_Abstract {

    protected $_pk;
    protected $memcache;

    public function __call($name, $args) {
        // $args[0] time
        // $args[1] key
        // $args[2] params
        if (substr($name, -5) != 'Cache') return;
        $method = substr($name, 0, -5);
        if (!method_exists($this, $method)) return false;
        $key = isset($args[1]) ? $args[1] : get_class($this) . '::' . $method;
        //$results = $this->memcache->get($key);
        if ($results === false || $results === null) {
            $results = call_user_func(array(&$this, $method), isset($args[3]) ? $args[0] : null);
            //$this->memcache->set($key, $results, isset($args[0]) ? $args[0] : Config::MEMCACHE_TIME);
        }
        return $results;
    }

    public function _init() {}

    public function init() {
        $this->_pk = $this->_primary;
        //$this->memcache = My_Memcache::getInstance();
        $this->_init();
    }
    public function _excute($sql) {
        try {

            $this->_db->query($sql);

        } catch (Zend_Db_Exception $ex) {
            throw $ex;
        }
    }


    public function getCache($callback, $key, $time = Config::MEMCACHE_TIME, $params = null) {
        $results = $this->memcache->get($key);
        if ($results === false || $results === null) {
            $results = call_user_func($callback, parent::$this, $params);
            $this->memcache->set($key, $results, $time);
        }
        return $results;
    }

    public function fetchObject($sql, $bind = null) {
        return $this->_db->query($sql, $bind)->fetchObject();
    }

    public function save($data) {
        try {
            $primary = null;
            if (isset($data[$this->_pk])) {
                $primary = $data[$this->_pk];
                unset($data[$this->_pk]);
            }
            if (empty($primary)) {
                return $this->insert($data);
            }
            $this->update($data, array("{$this->_pk} = ?" => $primary));
            return $primary;
        } catch (Zend_Exception $e) {
            // write log
        }
        return false;
    }

    public function insertIgnore($data) {
        $fields = array();
        $values = array();
        foreach ($data as $key => $value) {
            $fields[] = "`$key`";
            $values[] = $this->_db->quote($value);
        }
        $fields = join(', ', $fields);
        $values = join(', ', $values);
        $sql = "INSERT IGNORE INTO {$this->_name} ($fields) VALUES($values)";

        try {
            $this->_db->query($sql);
            return $this->_db->lastInsertId();
        } catch (Exception $e) {
            // write log
        }

        return false;
    }

    public function insertOnDuplicate($data, $onDuplicate) {
        $fields = array();
        $values = array();
        foreach ($data as $key => $value) {
            $fields[] = "`$key`";
            $values[] = $this->_db->quote($value);
        }
        $fields = join(', ', $fields);
        $values = join(', ', $values);
        $sql = "INSERT INTO {$this->_name} ($fields) VALUES($values) ON DUPLICATE KEY UPDATE $onDuplicate";

        try {
            $this->_db->query($sql);
            return $this->_db->lastInsertId();
        } catch (Exception $e) {
            // write log
        }

        return false;
    }

    public function remove($id) {
        try {
            return $this->delete("`{$this->_primary}` = '$id'");
        } catch (Zend_Exception $e) {
            // write log
        }
        return false;
    }

    /**
     * @param int | array $id
     * @return object
     */
    public function get($id) {
        if (is_array($id)) return $this->_find($id);
        $sql = "SELECT * FROM {$this->_name} WHERE {$this->_pk} = :{$this->_pk}";
        return $this->_db->fetchRow($sql, array($this->_pk => $id));
    }

    public function _find($params) {
        $and = '';
        $where = '';
        foreach ($params as $key => $value) {
            $value = $this->_db->quote($value);
            $where .= " $and `$key` = $value";
            $and = 'AND';
        }
        if ($where == '') return false;
        $sql = "SELECT * FROM `{$this->_name}` WHERE $where";
        return $this->_db->fetchRow($sql);
    }

    public function getOne($sql, $bind = null) {
        try {
            return $this->_db->fetchOne($sql, $bind);
        } catch (Zend_Exception $e) {
            // write log
        }
        return false;
    }

    public function getRow($sql, $bind = null) {
        try {
            return $this->_db->fetchRow($sql, $bind);
        } catch (Zend_Exception $e) {
            // write log
        }
        return false;
    }

    public function getRows($sql, $bind = null) {
        try {
            return $this->_db->fetchAll($sql, $bind);
        } catch (Zend_Exception $e) {
            // write log
        }
        return false;
    }

    public function getRowsAndTotal($sql, $bind = array()) {
        try {
            $rows  = $this->_db->fetchAll($sql, $bind);
            $total = $this->_db->fetchOne('SELECT FOUND_ROWS()');
            return array('rows' => $rows, 'total' => $total);
        } catch (Zend_Exception $e) {
            // write log
        }
        return false;
    }

    public function selectAll($where = '') {
        if (!empty($where)) {
            $where = " WHERE $where ";
        }
        return $this->getRows("SELECT * FROM {$this->_name} $where");
    }

    public function toObject($array) {
        if (is_array($array)) {
            foreach ($array as &$value) {
                $value = (object)$value;
            }
        }
        return $array;
    }

    public function addUserItem($userId, $itemId) {
        return $this->insertOnDuplicate(array(
            $this->_pk  => $itemId,
            'user_id'   => $userId,
            'is_active' => 1,
            'date_add'  => date('Y-m-d H:i:s'),
        ), 'is_active = 1');
    }

    public function removeUserItem($userId, $itemId) {
        return $this->update(array(
            'is_active' => 0,
        ), array(
            "{$this->_pk} = ?" => $itemId,
            'user_id = ?' => $userId,
        ));
    }
    public function _updateActive($where){
        return $this->update(array(
            'is_active' => 0,
        ), $where);
    }
    public function updateActive($data,$id){
       return $this->update($data, array(
            "{$this->_pk} = ?" => $id
        ));
    }
}