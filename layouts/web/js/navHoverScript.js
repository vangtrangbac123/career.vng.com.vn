// JavaScript Document
var htmlBlank = "<div style='height:40px;width: 100%;' class='htmlBlank'></div>"

jQuery(document).ready(function() {

    var winH = $(window).height();
    var contentH = $('.content').height();
    var headerH = $('.header').height();

    // if (!jQuery('body.homepage').size()) {
    //     if ((winH - contentH) > 0) {
    //         jQuery('.footer').css({
    //             position: 'absolute',
    //             width: '100%',
    //             bottom:0
    //         });
    //     }
    // }

    jQuery(window).scroll(function() {
        if (jQuery(document).scrollTop() > 125) {
            if (!$(".htmlBlank").size()) {
                $(".topHeader").after(htmlBlank);
            };

            $(".nav").css({
                position: "fixed",
                width: '100%',
                margin: '0 auto',
                'z-index': '1000',

                top: 0
            });

            $(".subNav").css({
                position: "fixed",
                width: '100%',
                margin: '0 auto',
                'z-index': '1000',
                top: '40px'
            });
        } else {



            $(".htmlBlank").remove();
            $(".nav").css({
                position: "relative",
                width: '100%',
                margin: '0 auto',
                'z-index': '10',
                top: 0
            });

            $(".subNav").css({
                position: "relative",
                width: '100%',
                margin: '0 auto',
                'z-index': '10',
                top: '0px'
            });
        }

    })

})

function doOver(num) {
    if (num == "1") {
        var d = document.getElementById("navSpan_1");
        d.className = d.className + " navSpanActive";
    }
    if (num == "2") {
        var d = document.getElementById("navSpan_2");
        d.className = d.className + " navSpanActive";
    }
    if (num == "3") {
        var d = document.getElementById("navSpan_3");
        d.className = d.className + " navSpanActive";
    }
    if (num == "4") {
        var d = document.getElementById("navSpan_4");
        d.className = d.className + " navSpanActive";
    }
    if (num == "5") {
        var d = document.getElementById("navSpan_5");
        d.className = d.className + " navSpanActive";
    }
    if (num == "6") {
        var d = document.getElementById("navSpan_6");
        d.className = d.className + " navSpanActive";
    }
}

function doOut(num) {
    if (num == "1") {
        var d = document.getElementById("navSpan_1");
        d.className = d.className + " navSpanDeactive";
        d.className = d.className - " navSpanActive";
    }
    if (num == "2") {
        var d = document.getElementById("navSpan_2");
        d.className = d.className + " navSpanDeactive";
        d.className = d.className - " navSpanActive";
    }
    if (num == "3") {
        var d = document.getElementById("navSpan_3");
        d.className = d.className + " navSpanDeactive";
        d.className = d.className - " navSpanActive";
    }
    if (num == "4") {
        var d = document.getElementById("navSpan_4");
        d.className = d.className + " navSpanDeactive";
        d.className = d.className - " navSpanActive";
    }
    if (num == "5") {
        var d = document.getElementById("navSpan_5");
        d.className = d.className + " navSpanDeactive";
        d.className = d.className - " navSpanActive";
    }
    if (num == "6") {
        var d = document.getElementById("navSpan_6");
        d.className = d.className + " navSpanDeactive";
        d.className = d.className - " navSpanActive";
    }
}