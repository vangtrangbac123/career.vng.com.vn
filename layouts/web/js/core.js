
var inProgess = false;
var submit = false;
var xhr;
function popupCenter(pageURL, title) {
    w      =  626,
    h      =  436,
    left   = (screen.width - w)  / 2,
    top    = (screen.height - h) / 2;
   r = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function isValid(type, value) {
    var re;
    value = $.trim(value);
    switch (type) {
        case 'interger':
             re = /^\d+$/;
            break;

        case 'email':
            re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            break;

        case 'phone':
            re = /^([0-9]){9,11}$/;
            break;
        case 'float':
            re = /^[+-]?\d+(\.\d+)?$/;
            break;
    }
    return re.test(value);
}


function readFile(input) {
    if (input.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
            console.log(e.target.result);
             $(this).parent().find('.files').val(e.target.result);
        };
        FR.readAsDataURL( input.files[0] );
    }
}

function timeOut(){
    setTimeout(
        function(){
            if(inProgess != false){
                inProgess = false;
                showMessage('Quá trình đăng ký vượt quá thời gian cho phép, vui lòng đăng ký lại', false);
                if(xhr){
                    xhr.abort();
                    $('.sloading').hide();
                }
            }
        }
     ,30000);
 }

function checkinfoForm(){
       var res = true;
       $('.required').html('');
       $('#form_error').html('');
       $("input[type='text'],select,input[type='password'],input[type='file']",$('#member_info')).each(function() {
           if($(this).val().trim() == "" && $(this).data('required') == "required") {
               res = false;
               $(this).parent().find('.required').html($(this).data('error'));
           }
            if($(this).val().trim() != "" && ($(this).data('attribute') == 'phone' || $(this).data('attribute') == 'email' || $(this).data('attribute') == 'interger' || $(this).data('attribute') == 'float') ){
                var re_type = $(this).data('attribute');
                var re_value = $(this).val();
                if(!isValid(re_type, re_value)){
                    $(this).parent().find('.required').html('Sai định dạng ');
                    res = false;
                }else{
                    $(this).parent().find('.required').html('');
                }
           }
       });

        return res;
}

function showMessage(message, success){
    if(success){
        $('.modal-body').css({'text-align':'left'});
    }
    $('.modal-body').html(message);
    $('.overlay').fadeIn(1000);
}
function checkquestionForm(){
    var res = true;
       $('.required').html('');
       $('#form_error').html('');
       $("input[type='text'],select,input[type='password'],input[type='file'],textarea",$('#member_question')).each(function() {
           if($(this).val().trim() == "" && $(this).data('required') == "required") {
               res = false;
               $(this).parent().find('.required').html($(this).data('error'));
           }
       });
        return res;
}

function submitRegisterForm(){
     var formData = new FormData($('form#frmRegister')[0]);
     $('.sloading').show();
     showMessage('Vui lòng đợi ... ',false);
        xhr = $.post('/student/register', $('form#frmRegister').serialize(), function(data) {
                $('.sloading').hide();
                data = JSON.parse(data);
                if(data.code) {
                    showMessage(data.message, true);
                    submit = true;
                }
                else{
                    showMessage(data.message, false);
                }
                inProgess = false;
                console.log(data);
        });
}

function loadRegbuttonFirstPage(reg_btn, tbody_info, tbody_question, strYear){
          params = {
                        event_id: recruit_program['program_event_ids'][0]['id'],
                        program_type: recruit_program['code'],
                    };
         Core123F.ajax.get('Common.getEventDetail', params, function(results) {
                reg_btn.hide();
                if(results.renderform){
                    tbody_info.html('');
                    for(i=0 ; i < results.renderform.length; i++){
                        tbody_info.append(results.renderform[i]);
                    }
                    reg_btn.show();
                }else{
                    tbody_info.html('Không tìm thấy dữ liệu');
                }

                if(results.questionform){
                    tbody_question.html('');
                    for(i=0 ; i < results.questionform.length; i++){
                        tbody_question.append(results.questionform[i]);
                    }
                }
            if ( $( ".datepicker" ).length >0 ){
                $( ".datepicker" ).datepicker({ dateFormat: 'mm/dd/yy',yearRange: strYear,changeMonth: true,changeYear: true }).val();
            }

         });
}

function checkIE() {
   if (navigator.userAgent.indexOf('MSIE') != -1)
     var detectIEregexp = /MSIE (\d+\.\d+);/ //test for MSIE x.x
    else // if no "MSIE" string in userAgent
     var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/ //test for rv:x.x or rv x.x where Trident string exists

    if (detectIEregexp.test(navigator.userAgent)){ //if some form of IE
      var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
      if (ieversion>=10){
        return false;
        } else{return true;}
    }
    else{
     return false;
    }
}


function searchJob(keyword, job_category_id, city_id ){
    var list_job = [];
    if(job_category_id == 0 && city_id == 0 ){
        list_school['job_program_ids'].forEach(function(item) {
            if(item.name.search(keyword) != -1){
                list_job.push(item);
            }
        });
    }

    if(job_category_id != 0 && city_id == 0){
        list_school['job_program_ids'].forEach(function(item) {
            if(item.name.search(keyword) != -1 && job_category_id == item.vhr_job_id[0]){
                list_job.push(item);
            }
        });
    }

    if(job_category_id == 0 && city_id != 0){
        list_school['job_program_ids'].forEach(function(item) {
            if(item.name.search(keyword) != -1 && city_id == item.city_id[0]){
                list_job.push(item);
            }
        });
    }

    if(job_category_id != 0 && city_id != 0){

        list_school['job_program_ids'].forEach(function(item) {
            if(item.name.search(keyword) != -1 && city_id == item.city_id[0] && job_category_id == item.vhr_job_id[0]){
                list_job.push(item);
            }
        });
    }
    $('#job_available').html('');
    data = {'data':list_job}

    $("#tmpl-job-search").tmpl(data).prependTo($('#job_available'));

}




var Core123F = {
    // Dev
    ENV: 'production',
    DEBUG: false,
    domain: document.host,
    controllerName: '',
    actionName: '',
    callStack: [],
    data: {
    },
    account: {
    },

    // init
    init: function() {
    },

    // call method by module
    exec: function(controller, action) {
        var action = (action === undefined) ? 'init' : action;
        var module = this['modules'];
        if (controller !== '' && module[controller]
            && typeof module[controller][action] == 'function') {
            module[controller][action]();
        }
    },
    ajax: {
        baseUrl: '/default/ajax/',
        get: function(api, params, callback) {
            if (typeof params == 'function') {
                callback = params;
                params = null;
            }
            params = $.extend({'method': api}, params);
            return $.getJSON(Core123F.ajax.baseUrl, params, function(results) {
                if (results != false && results != null) {
                    callback.call(Core123F.ajax, results);
                } else {
                    callback.call(Core123F.ajax, null);
                }
            });
        },
        post: function(api, params, callback) {
            return $.post(Core123F.ajax.baseUrl + '?method=' + api, params, callback);
        }
    },
    // DOM ready
    ready: function() {
        Core123F.init();
        this.controllerName = $('body').data('controller');
        this.actionName     = $('body').data('action');
        this.exec(this.controllerName);
        this.exec(this.controllerName, this.actionName);

        // call stack
        if (this.callStack[this.controllerName]
            && this.callStack[this.controllerName][this.actionName]
            && this.callStack[this.controllerName][this.actionName].length > 0) {
            for (var i = 0, f; f = this.callStack[this.controllerName][this.actionName][i]; i++) {
                f.call(this);
            };
        }
    },

    // function
    cookie: function(key, value, option) {
        if (value === undefined && option === undefined) {
            return $.cookie(key);
        }
        var defaults = {path: '/', domain: this.domain, expires: 365};
        var options = $.extend(defaults, option);
        return $.cookie(key, value, options);
    },

    // extend
    extend: function(func, controller, action) {
        controller = controller || 'index';
        action = action || 'index';
        if (!this.callStack[controller]) {
            this.callStack[controller] = {};
        }
        if (!this.callStack[controller][action]) {
            this.callStack[controller][action] = [];
        }
        this.callStack[controller][action].push(func);
    },
    hashUrl:{
        init:function(){
            var hashArr = {};
             if (window.location.hash.length > 2) {
                var hash      = window.location.hash.substr(1);
                var arr       = hash.split('.');
                for (i= 0 ;i<arr.length; i++) {
                    strL    = arr[i].substring(0,1);
                    strR = arr[i].substring(1,arr[i].length);
                    if( strL =='E')  hashArr['event_id']  = strR;
                    if( strL =='P')  hashArr['program_type']      = strR;
                }

            }
            return hashArr;
        },
        update:function(params){
            var Lhash   = '';
            var seq     = '';
            var hashArr = {};
            if(params['event_id'])   hashArr['event_id']   = 'E' + params['event_id'];
            if(params['program_type']) hashArr['program_type']   = 'P' + params['program_type'];
            jQuery.each( hashArr, function( i, val ) {
                Lhash += seq + val;
                seq = '.';
            });
            window.location.hash  = Lhash;
        }
     },
     popup:{
        center:function(pageURL, title){
            w      =  626,
            h      =  436,
            left   = (screen.width - w)  / 2,
            top    = (screen.height - h) / 2;
            r = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
        }
     },

    plugins: {
      },
    // modules
    modules: {
        index: {
            index: function(i){
            },
        },
        student: {
            index: function(){
             $('#reg_btn_rc').click(function(){
                var reg_rc = true;
                $("input[type='text']",$('#frmRegisterReceive')).each(function() {
                   if($(this).val().trim() == "") {
                       $(this).parent().find('.Critical').html($(this).data('error'));
                       reg_rc = false;
                   }

               });
                var atLeastOneIsChecked = $('input[name="chk[]"]:checked').length,
                i = 0,
                arr = [];
                 $('input[name="chk[]"]:checked').each(function(){
                         arr[i++] = $(this).val();
                });

                if(atLeastOneIsChecked == 0){
                    $('#msg_program').html($('#tb_checkbox').data('error'));
                    reg_rc = false;
                }

                if(reg_rc){
                    params = {
                        'email':$('input[name="email"]').val(),
                        'name':$('input[name="name"]').val(),
                        'program_type_ids':arr
                    };

                    if(xhr){
                        xhr.abort();
                    }
                    $('.sloading').show();

                    xhr = Core123F.ajax.post('Common.regReceiveNews', {'member' : params}, function(results) {
                        $('.sloading').hide();
                        if(results.code) {
                            $('#msg_sucss').html(results.message);
                        }
                        else{
                            $('#msg_sucss').html(results.message);
                        }
                    });

                }
                return false;
             });

            },
            fresher: function(){

            },
            detail:function(){
                var arrEvent = Core123F.hashUrl.init();
                var liEvent;
                if(arrEvent.event_id){
                    liEvent = $(".program_event[data-id='" + arrEvent.event_id +"']");
                    liEvent.trigger( "click" );
                    liEvent.find('a').addClass('active');
                }

            }
        },
        job: {
            index: function(){
                var job_category_id,
                    search_form = $("#reg_btn_search"),
                    job_category = $('.job_category');

                job_category.click(function(){
                    job_category_id = $(this).data('id');
                    $('#job_available').html('');
                    list_school['job_cat_ids'].forEach(function(item) {
                        if(item.id == job_category_id){
                             $("#tmpl-job").tmpl(item).prependTo($('#job_available'));
                        }
                    });

                });

                // $(document).on('click', '.job_category_item', function(e){
                //     var job_item,
                //         list_job_related =  [],
                //         job_category_id = $(this).data('category'),
                //         job_id = $(this).data('id');
                //     $('#job_available').html('');
                //     list_school['job_cat_ids'].forEach(function(cat) {
                //         if(cat.id == job_category_id){
                //              cat['job_program_vn_ids'].forEach(function(item) {
                //                 if(item.id == job_id){
                //                     job_item = item;
                //                 }else{
                //                     list_job_related.push(item);
                //                 }
                //              });
                //         }
                //     });
                //     console.log(job_item);
                //     console.log(list_job_related);
                //     $("#tmpl-job-detail").tmpl(job_item).prependTo($('#job_available'));
                // });

            },
            category: function(){},
            detail: function(){},
        }
    }
};

$(document).on('click', '.captcha_refresh', function(e){
    $(".captcha_image").attr("src","/index/gencaptcha?rnd=" + Math.random());
});

//Next button
$(document).on('click', '#reg_btn_next', function(e){
       var info_success = checkinfoForm();
        if(info_success){
            // Core123F.ajax.post('Common.checkcaptchaRegister', $('#frmRegister').serialize(), function(results) {
            //     $('#form_error').html('');
            //     if(results.error) {
            //         $('#form_error').html(results.error);
            //         showMessage(results.error, false);
            //     }else{
                    $('#form_error').html('');
                    $('#member_info').hide();
                    $('#member_question').show();
            //     }
            // });
        }

    });
//Back button
$(document).on('click', '#reg_btn_back', function(e){
            $('#member_info').show();
            $('#member_question').hide();
        });
//info submit

 $(document).on('click', '#reg_btn', function(e){
       var info_success = checkinfoForm();
        if(info_success && !inProgess){
            inProgess = true;
            // Core123F.ajax.post('Common.checkcaptchaRegister', $('#frmRegister').serialize(), function(results) {
            //     $('#form_error').html('');
            //     if(results.error) {
            //         $('#form_error').html(results.error);
            //         showMessage(results.error, false);
            //     }else{
                    submitRegisterForm();
                     timeOut();
            //     }

            //     inProgess = false;
            // });
        }
    });

//question submit
 $(document).on('click', '#reg_btn_question', function(e){
       var info_success = checkquestionForm();
        if(info_success && !inProgess){
            inProgess = true;
            submitRegisterForm();
            timeOut();
        }

});



 //richtexxt
 //question submit
 $(document).on('change', '.richtext', function(e){
       $(this).parent().find('textarea').toggle();
});

  //richtexxt
 //question submit
 $(document).on('mouseout', '.arearichtext', function(e){
       var checkbox = $(this).parent().find('input');
       checkbox.val(checkbox.data('value') +'-'+ $(this).val());
});

 //Convert base 64
 $(document).on('change', '.file', function(e){
    var form = $(this).parent();
    var inputFileUrl = $(this).parent().parent().find('.url_file');
    var inputFileName = $(this).parent().parent().find('.name_file');
    var UploadNotice = $(this).parent().find('.notice');
    var formData = new FormData(form[0]);
    var file_required = $(this).data('required');

    if($(this)[0].files[0]){
        if($(this)[0].files[0].size > 20*1024*1024){
            showMessage('Dung lượng file upload quá lớn (>20MB)', false);
            form.get(0).reset();
        }else{
            UploadNotice.show();
            inProgess = true;
            $.ajax({
                    xhr: function()
                      {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function(evt){
                          if (evt.lengthComputable) {
                            var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
                            //Do something with upload progress
                            console.log(percentComplete);
                            UploadNotice.find('.progressbar').width(percentComplete);
                            UploadNotice.find('span').text('Đang tải ' + percentComplete );
                          }
                        }, false);
                        return xhr;
                      },
                    url: '/go123/upload',
                    data: formData,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                        data = JSON.parse(data);
                        UploadNotice.hide();
                        UploadNotice.find('.progressbar').width('1%');
                        UploadNotice.find('span').text('Đang tải 1%');

                        inProgess = false;
                       if(data.code) {
                            inputFileUrl.val(data.message.url);
                            inputFileName.val(data.message.filename);

                            if(file_required == "required"){
                                $('#reg_btn_next').removeAttr("style");
                                $('#reg_btn').removeAttr("style");
                            }
                        }
                        else{
                            showMessage(data.message, false);
                            //For IE
                            if(file_required == "required"){
                                $('#reg_btn_next').css('background-color','rgb(207, 207, 207)');
                                $('#reg_btn').css('background-color','rgb(207, 207, 207)');
                            }

                            form.get(0).reset();
                        }
                    }
            });
        }
    }
});



$(document).ready(function() {
    var table = $('#tbody'),//Body form reg
        des_content = $('.hrText'),//content description
        reg_btn = $('.hrSubmit'),//reg button and share
        program_content = $('.hrMajorList .program_content'),//Program content
        program_event = $('.hrMajorList .program_event'),//Program event
        tbody_info = $('#member_info'),
        tbody_question = $('#member_question');

    var d = new Date();
    var n = d.getFullYear()-14;
    var strYear="1950:"+n;

    var overlay = $('#overlay-book');

    Core123F.ready();

    if (checkIE()) {
        showMessage('Vui lòng sử dụng các trình duyệt: Chrome, Firefox, Cờ rôm + để được hỗ trợ tốt nhất.', false);
        //jQuery.cookie('ie', 1, {domain: '.123phim.vn', path: '/', expires: 1});
    }
    //loadRegbuttonFirstPage(reg_btn, tbody_info, tbody_question, strYear);

    overlay.find('.btn-close').click(function() {
        if(submit){
            location.reload();
        }
        overlay.fadeOut();
        $(".captcha_image").attr("src","/index/gencaptcha?rnd=" + Math.random());
    });


    $('.hrMajorList a').click(function(){
        $('.hrMajorList a').removeClass('active');
        $(this).addClass('active');
    });
    //GET content
    program_content.click(function(){
         var key = $(this).data('key');
        des_content.html(recruit_program['program_content_ids'][key]['description']);
        reg_btn.hide();
    });

    //GET event
    program_event.click(function(){
          params = {
                    event_id: $(this).data('id'),
                    program_type: recruit_program['code'],
                };
        Core123F.hashUrl.update(params);
        des_content.html('Đang tải ...');
         Core123F.ajax.get('Common.getEventDetail', params, function(results) {
            console.log(results);
                reg_btn.hide();
                if(!results){
                    des_content.html('Không tìm thấy dữ liệu');
                }else{
                    des_content.html(results.description);
                }

                if(results.renderform){
                    tbody_info.html('');
                    for(i=0 ; i < results.renderform.length; i++){
                        tbody_info.append(results.renderform[i]);
                    }
                    reg_btn.show();
                }else{
                    tbody_info.html('Không tìm thấy dữ liệu');
                }

                if(results.questionform){
                    tbody_question.html('');
                    for(i=0 ; i < results.questionform.length; i++){
                        tbody_question.append(results.questionform[i]);
                    }
                }
            if ( $( ".datepicker" ).length > 0 ){
                $( ".datepicker" ).datepicker({ dateFormat: 'mm/dd/yy',yearRange: strYear,changeMonth: true,changeYear: true,defaultDate: '01/01/1950'}).val();
            }
            var file_required = $('.file[data-required="required"]');
            if(file_required.length > 0){
                $('#reg_btn_next').css('background-color','rgb(207, 207, 207)');
                $('#reg_btn').css('background-color','rgb(207, 207, 207)');

            }

         });
    });


    //Show popup
    $(".various").fancybox({
        maxWidth: 640,
        maxHeight: 660,
        minHeight:500,
        fitToView: false,
        width: '70%',
        /*height: '96%', */
        height: 'auto',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });

    var slides = $('.jcarousel li'),
    counter = 0;

    setInterval(function() {
      counter = (counter + 1) % slides.length; // Increment counter
       $('.jcarousel').jcarousel('scroll', counter);
    }, 10000); // setInterval


    // $("#reg_btn_search").click(function(){
    //     var keyword = $("#keyword").val(),
    //         job_category_id  = $("#cate_id").val(),
    //         city_id = $("#city_id").val();

    //     searchJob(keyword, job_category_id, city_id);
    // });

    $('#reg_btn_rc_job').click(function(){
        $('#frmRegisterReceive .Critical').html('');
        var reg_rc = true;
        $("input[type='text']",$('#frmRegisterReceive')).each(function() {
           if($(this).val().trim() == "") {
               $(this).parent().find('.Critical').html($(this).data('error'));
               reg_rc = false;
           }

       });

        if(reg_rc){
            params = {
                'email':$('input[name="email"]').val(),
                'name':$('input[name="name"]').val(),
                'program_type_ids':['CANDIDATE']
            };

            if(xhr){
                xhr.abort();
            }
            $('.sloading').show();

            xhr = Core123F.ajax.post('Common.regReceiveNews', {'member' : params}, function(results) {
                $('.sloading').hide();
                if(results.code) {
                    $('#msg_sucss').html(results.message);
                }
                else{
                    $('#msg_sucss').html(results.message);
                }
            });

        }
        return false;
     });

    $(".zm_share").click(function(){
        var share_url = document.URL,
            share_title = $(this).data('title'),
            share_desc = $(this).data('desc');
       

        if($(this).data('type') == 'zing'){
            Core123F.popup.center('http://link.apps.zing.vn/pro/view/conn/share?u='+share_url+'&t='+share_title+'&desc='+share_desc+'&images=https://img.zing.vn/vng/skin/vng-2014/image/vng-logo-share-v2.jpg', 'Zingme share');
        }
        if($(this).data('type') == 'fb'){
            Core123F.popup.center('http://www.facebook.com/sharer/sharer.php?u='+share_url+'&t='+share_title,' Facebook share');
        }
        if($(this).data('type') == 'linke'){
            Core123F.popup.center('https://www.linkedin.com/shareArticle?mini=true&amp;title='+share_title+'&amp;url='+share_url+'&amp;summary='+share_desc,'mywindow');
        }
    });



});