
(function ($) {

    $.fn.myuploader = function(options) {

        this.each(function() {

            var uploader;
            var $this = $(this);
            var form = $this.parent();
            var inputFileUrl  = $this.parent().parent().find('.url_file');
            var inputFileName = $this.parent().parent().find('.name_file');
            var UploadNotice  = $this.parent().find('.notice');success_upload
            var UploadSuccess = $this.parent().find('.success_upload');
            var file_required = $this.data('required');

            var opts = {
                browse_button: 'iu' + $.now(),
                url: '/go123/upload',
                flash_swf_url: '/resources/js/plupload/plupload.flash.swf',
                silverlight_xap_url: '/resources/js/plupload/plupload.silverlight.xap',
                file_data_name: $this.attr('name'),
                multi_selection: false,
                max_file_size:  $this.data('limit')+'mb',
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                filters: [
                    {
                        title: "files",
                        extensions: $this.data('filter')
                    }
                ],

                multipart_params:{
                    limit: $this.data('limit'),
                    filter: $this.data('filter')
                }
            };


            opts = $.extend(opts, options);

            $this.attr('id', opts.browse_button);

            uploader = new plupload.Uploader(opts);

            uploader.init();

            uploader.bind('FilesAdded', function(up, plfiles) {
                UploadNotice.show();
                UploadSuccess.hide();
                inputFileName.val('');
                inProgess = true;
                uploader.start();
            });

            uploader.bind('UploadProgress',function(up, file){
                UploadNotice.find('.progressbar').width(file.percent);
                UploadNotice.find('span').text('Đang tải ' + file.percent );

            });

            uploader.bind('Error',function(up, err){
                UploadNotice.hide();
                UploadSuccess.hide();
                inputFileName.val('');
                UploadNotice.find('.progressbar').width('1%');
                UploadNotice.find('span').text('Đang tải 1%');
                showMessage(err.message, false);

                //For IE
                if(file_required == "required"){
                    $('#reg_btn_next').css('background-color','rgb(207, 207, 207)');
                    $('#reg_btn').css('background-color','rgb(207, 207, 207)');
                }

            });


            uploader.bind('FileUploaded', function(up, file, data) {
                data = JSON.parse(data['response']);

                UploadNotice.hide();
                UploadNotice.find('.progressbar').width('1%');
                UploadNotice.find('span').text('Đang tải 1%');

                inProgess = false;
               if(data.code) {
                    inputFileUrl.val(data.message.url);
                    inputFileName.val(data.message.filename);
                    UploadSuccess.show();
                    UploadSuccess.html(file.name+' '+Math.round(file.size/1024) + 'kb');

                    if(file_required == "required"){
                        $('#reg_btn_next').removeAttr("style");
                        $('#reg_btn').removeAttr("style");
                    }
                }
                else{
                    UploadSuccess.hide();
                    inputFileName.val('');
                    showMessage(data.message, false);
                    //For IE
                    if(file_required == "required"){
                        $('#reg_btn_next').css('background-color','rgb(207, 207, 207)');
                        $('#reg_btn').css('background-color','rgb(207, 207, 207)');
                    }

                    form.get(0).reset();
                }
            });

        });
    };

}(jQuery));

