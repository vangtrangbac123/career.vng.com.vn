<?php
/*if(!isset($_COOKIE['dev'])){
    include("maintain.php");die;
}

*/
// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(-1);
// Test
define('MEMORY_USAGE', memory_get_usage());
define('REQUEST_TIME_START', microtime(true));

// Define base path
define('ROOT', realpath(dirname(__FILE__)));
define('LIBRARY_PATH', ROOT . DIRECTORY_SEPARATOR . 'library');
define('APPLICATION_PATH', ROOT . DIRECTORY_SEPARATOR . 'application');

//define('APPLICATION_PATH', ROOT . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . APPLICATION_HOST);

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

// Include path
set_include_path(implode(PATH_SEPARATOR, array(LIBRARY_PATH, get_include_path())));

require 'App.php';
require 'Config.php';
require 'Zend/Application.php';

$application = new Zend_Application(APPLICATION_ENV);
$application->getAutoloader()->registerNamespace(array('My_', 'Utility_', 'Plugin_', 'Api_'));
$application->setOptions(require APPLICATION_PATH . '/configs/application.php')->bootstrap()->run();
