$(function() {
	$("#showGreeting").click(showGreeting);
	$("#showProduct1").click(showProduct);
});

function showGreeting(){
	$("#mydiv").load("MyServer.jsp");
}

function showResult(text){
	alert(text);
}

function showProduct(){
	$.ajax({
		url:"ProductServlet",
		success : displayResult,
		dataType : "json"
	});
}

function displayResult(text){
	var resutl = text.id + " - " + text.name + " - " + text.price;
	$("#mydiv2").html(resutl);
}