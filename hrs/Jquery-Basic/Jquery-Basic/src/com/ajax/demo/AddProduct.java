package com.ajax.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class AddProduct
 */
@WebServlet("/AddProduct")
public class AddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Map<String, Product> productMap = new HashMap<String,Product>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProduct() {
    	
        super();
        Product p1 = new Product("1001", "Iphone-4S", 1500);
		Product p2 = new Product("1002", "Iphone-5S", 2500);
		Product p3 = new Product("1003", "Galaxy-4S", 3500);
		Product p4 = new Product("1004", "Lumina-996", 1700);
		
		
		productMap.put(p1.getId(), p1);
		productMap.put(p2.getId(), p2);
		productMap.put(p3.getId(), p3);
		productMap.put(p4.getId(), p4);
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("text/javascript");
		PrintWriter out = response.getWriter();
		
		String data = request.getParameter("product");
		try {
			JSONObject newProduct = new JSONObject(data);
			Product p = new Product(newProduct.getString("id"),
					newProduct.getString("name"), newProduct.getDouble("price"));
			productMap.put(p.getId(), p);
			
			JSONObject sendClient = new JSONObject(productMap,false);
			out.print(sendClient);
		} catch (JSONException e) {		
			e.printStackTrace();
		}
		System.out.println(data);
		
		
	}

}
