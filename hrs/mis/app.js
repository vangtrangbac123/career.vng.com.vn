'use strict';

(function() {
angular.module('app', ['ngRoute', 'ngResource', 'ngCookies', 'ngAnimate', 'ngSanitize', 
                        'angular-loading-bar', 'checklist-model', 'toaster', 'naif.base64', 
                        'ui.date', 'vcRecaptcha', 'angular-data.DSCacheFactory', 'angular-carousel']);

angular.module('app').constant('appConfig', {
    // dev
    //restURL: 'https://esb-dev.vng.com.vn:8888/',
    // programEventId: '31', 
    // uat
    //restURL: 'https://esb-uat.vng.com.vn:8888/',    
    // prod
    restURL: 'https://hrs.vng.com.vn/',
    programType: 'SIMINAR',
    // dev
    faceType: 'SPEAKER_FACE',
    // prod
    //programEventId: '41', 
    responseCode: {
        ok: 1,
        badRequest: 400,
        unauthorized: 401,
        notFound: 404,
        internalServerError: 500
    },
    questionType: {
        multiChoice: 'MULTICHOICE',
        multiChoiceMA: 'MULTICHOICE_MA',
        text: 'TEXT',
        multiChoiceRichText: 'MULTICHOICE_RICHTEXT'
    },
    questionPart: {
        one: 'QUESTION_PART1',
        two: 'QUESTION_PART2',
        three: 'QUESTION_PART3',
        four: 'QUESTION_PART4'
    },
    maximumAttachmentSizeInByte: 3145728,
    cache: {
        key: 'hrsCache', 
        programEvent: 'programEvent', 
        typicalFace: 'typicalFace'
    }
});

// declare app level module which depends on filters, and services
angular.module('app').config(['$httpProvider', 'cfpLoadingBarProvider', 
    function($httpProvider, cfpLoadingBarProvider) {
        
    // initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }    
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';    
    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8';
    $httpProvider.defaults.headers.common['Authorization'] = 'Basic bWlzZXNiOlZuZ0AxMjM=';

    //loading bar
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;

}]);

angular.module('app').run(['DSCacheFactory', 'appConfig', function(DSCacheFactory, appConfig) {
    
    DSCacheFactory(appConfig.cache.key, {
        maxAge: 1800000, // Items added to this cache expire after 30 minutes.
        cacheFlushInterval: 3600000, // This cache will clear itself every hour.
        deleteOnExpire: 'aggressive' // Items will be deleted from this cache right when they expire.
    });

}]);

angular.module('app').filter('unsafe', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}]);

})();
