'use strict';

 function TypicalFaceController($scope, $http, $log, $filter, $location, toaster, vcRecaptchaService, appConfig, appService) {

 	$scope.appConfig = appConfig;
    $scope.typicalFaces = [];
    $scope.carouselIndex = 0;

	$scope.toasterOption = {
		'close-button': true,
		'debug': false,
		'position-class': 'toast-top-left',
		'onclick': null,
		'show-duration': '300',
		'hide-duration': '1000',
		'time-out': '5000',
		'extended-timeOut': '1000',
		'show-easing': 'swing',
		'hide-easing': 'linear',
		'show-method': 'fadeIn',
		'hide-method': 'fadeOut'
	};

    $scope.initialize = function() {
    	appService.getTypicalFace(appConfig.faceType).then(function(response) {
    		$scope.typicalFaces = response;            
    	});
    };
    
    $scope.initialize();
	
}