'use strict';

 function AppController($scope, $http, $log, $filter, $location, toaster, vcRecaptchaService, appConfig, appService) {

 	$scope.appConfig = appConfig;
    $scope.programEvents = {};
    $scope.selectProgramEvent = null;

	$scope.toasterOption = {
		'close-button': true,
		'debug': false,
		'position-class': 'toast-top-left',
		'onclick': null,
		'show-duration': '300',
		'hide-duration': '1000',
		'time-out': '5000',
		'extended-timeOut': '1000',
		'show-easing': 'swing',
		'hide-easing': 'linear',
		'show-method': 'fadeIn',
		'hide-method': 'fadeOut'
	};

    $scope.initialize = function() {
    	appService.getProgramEvent(appConfig.programType).then(function(response) {
    		$scope.programEvents = response;
            var programEventId = $location.search()['id'];
            if (appService.isUndefinedOrNull(programEventId) || appService.isUndefinedOrNull($scope.programEvents)) {
                $scope.selectProgramEvent = null;
            } else {
                var programEvents = $.grep($scope.programEvents, function(programEvent) {
                    return programEvent.id.toString() === programEventId;
                });
                if (programEvents.length > 0) {
                    $scope.selectProgramEvent = programEvents[0];
                }
            }            
    	});
    };

    $scope.updateContent = function(programEventId) {
        var programEvents = $.grep($scope.programEvents, function(programEvent) {
            return programEvent.id === programEventId;
        });
        if (programEvents.length > 0) {
            $scope.selectProgramEvent = programEvents[0];
        }
    };
    
    $scope.initialize();
	
}