'use strict';

 function RegisterController($scope, $http, $log, $filter, $location, toaster, vcRecaptchaService, appConfig, appService) {

    $scope.submitted = false;
 	$scope.appConfig = appConfig;
 	$scope.genders = ['male', 'female'];
 	$scope.data = {};
 	$scope.data.studentStaffs = [];
 	$scope.response = null;
    $scope.widgetId = null;
    $scope.model = {
        key: '6LfuUwATAAAAACvLetv80eo8-689Bs0-8iR1Gj5U'
    };
	$scope.toasterOption = {
		'close-button': true,
		'debug': false,
		'position-class': 'toast-top-left',
		'onclick': null,
		'show-duration': '300',
		'hide-duration': '1000',
		'time-out': '5000',
		'extended-timeOut': '1000',
		'show-easing': 'swing',
		'hide-easing': 'linear',
		'show-method': 'fadeIn',
		'hide-method': 'fadeOut'
	};
    $scope.invalidQuestionPartTwo = false;

    var programEventId = appConfig.programEventId;
	var invalidRegisterInformationMessage = 'Thông tin đăng ký không hợp lệ';

    $scope.removeFileCV = function() {
    	$scope.data.fileCV = null;
    };

    $scope.removeFileOther = function() {
    	$scope.data.fileOther = null;
    };     	 	
	
    $scope.initialize = function() {
        var tmpProgramEventId = $location.search()['id'];
    	if (!appService.isUndefinedOrNull(tmpProgramEventId)) {
            programEventId = tmpProgramEventId;
        }
    	var d = new Date();
		var n = d.getFullYear() - 14;
		var yearRange = "1950:" + n;
    	$scope.dateOptions = {
    		dateFormat: 'dd/mm/yy',
	        changeYear: true,
	        changeMonth: true,
	        yearRange: yearRange
	    };

    	appService.getSchool(programEventId).then(function(response) {
    		$scope.schools = response;
    	});

    	appService.getSpeciality().then(function(response) {
    		$scope.specialities = response;
    	});

 		appService.getRecruitmentDegree().then(function(response) {
    		$scope.recruitmentDegrees = response;
    	});

 		appService.getTimeGraduation().then(function(response) {
    		$scope.timeGraduations = response;
    	});

 		appService.getStudentStaff().then(function(response) {
    		$scope.studentStaffs = response;
    	});

 		appService.getKnowFresher(programEventId).then(function(response) {
    		$scope.knowFreshers = response;
    	});

    	appService.getListQuestion(programEventId).then(function(response) {
    		$scope.questions = response;
    	});    	 	    	  	
    };

    $scope.handleCheckStudentStaff = function(checkStudentStaff)  {
    	var studentStaffs = $.grep($scope.data.studentStaffs, function(studentStaff) {
    		return studentStaff.id === checkStudentStaff.id;
    	});
    	if (studentStaffs.length > 0) {
			studentStaffs = $.grep($scope.data.studentStaffs, function(studentStaff) {
    			return studentStaff.id !== checkStudentStaff.id;
    		});
    		angular.copy(studentStaffs, $scope.data.studentStaffs);    		
    	} else {
    		$scope.data.studentStaffs.push(angular.copy(checkStudentStaff));
    	}
    };

	$scope.handleSelectAnswer = function(question) {
		if (appService.isUndefinedOrNull(question.answers)) {
			question.answers = [];
		} else {
			question.answers.length = 0;
		}
		if (!appService.isUndefinedOrNull(question.selectAnswer)) {
			$log.debug('Select answer id=' + question.selectAnswer.id + ', name=' + question.selectAnswer.name + 
				' of question id=' + question.id + ', name=' + question.name);
			question.answers.push(angular.copy(question.selectAnswer));
		}
	}; 

    $scope.handleCheckAnswer = function(question, checkAnswer)  {
    	if (appService.isUndefinedOrNull(question.answers)) {
			question.answers = [];
		}
    	var answers = $.grep(question.answers, function(answer) {
    		return answer.id === checkAnswer.id;
    	});
    	if (answers.length > 0) {
    		$log.debug('Uncheck answer id=' + checkAnswer.id + ', name=' + checkAnswer.name + 
				' of question id=' + question.id + ', name=' + question.name);
			answers = $.grep(question.answers, function(answer) {
    			return answer.id !== checkAnswer.id;
    		});
    		angular.copy(answers, question.answers);
    		checkAnswer.isSelect = false;   		
    	} else {
    		$log.debug('Check answer id=' + checkAnswer.id + ', name=' + checkAnswer.name + 
				' of question id=' + question.id + ', name=' + question.name);
    		question.answers.push(checkAnswer);
    		checkAnswer.isSelect = true;
    	}
    };

    $scope.handleTextAnswer = function(question)  {
    	if (angular.isUndefined(question.answers)) {
			question.answers = [];
		} else {
			question.answers.length = 0;
		}
        if (!appService.isUndefinedOrNull(question.textAnswer) && question.textAnswer.length > 0) {
            $log.debug('Text set answer id=' + question.id + ', name=' + question.textAnswer + 
                    ' of question id=' + question.id + ', name=' + question.name);
            question.answers.push({id: question.id, name: question.textAnswer});
        }
    };

    $scope.setResponse = function (response) {
        $log.debug('Captcha response available');
        $scope.response = response;
    };

    $scope.setWidgetId = function (widgetId) {
        $log.debug('Captcha created widget ID: ' + widgetId);
        $scope.widgetId = widgetId;
    };

    function checkOptionQuestionPart(questionPart, questionAnswers) {
    	var valid = true;
		var questionOptions = $.grep($scope.questions, function(question) {
    		return question.question_part_code === questionPart;
    	});
    	//if (questionOptions.length > 0 && questionAnswers.length > 0 && questionOptions.length !== questionAnswers.length) {
        if (questionOptions.length !== questionAnswers.length) {
    		//toaster.pop('error', invalidRegisterInformationMessage);
    		valid = false;
    	}
    	return valid;
    }      

    $scope.submit = function(invalid, error) {
        $scope.invalidQuestionPartTwo = false;
        $scope.submitted = true;
        // file CV
        if (!appService.isUndefinedOrNull($scope.data.fileCV)) {
            // file size
            if ($scope.data.fileCV.filesize > appConfig.maximumAttachmentSizeInByte) {
                toaster.pop('error', 'Tập tin đính kèm không quá ' + $filter('number')((appConfig.maximumAttachmentSizeInByte/1024/1024), 2) + 'M');
                return;
            }
            // file type
            if (!appService.acceptFileCV($scope.data.fileCV.filename)) {
                toaster.pop('error', 'File CV không hợp lệ');
                return;
            }
        }
        // file other
        if (!appService.isUndefinedOrNull($scope.data.fileOther)) {
            // file size
            if ($scope.data.fileOther.filesize > appConfig.maximumAttachmentSizeInByte) {
                toaster.pop('error', 'Tập tin đính kèm không quá ' + $filter('number')((appConfig.maximumAttachmentSizeInByte/1024/1024), 2) + 'M');
                return;
            }
            // file type
            if (!appService.acceptFileOther($scope.data.fileOther.filename)) {
                toaster.pop('error', 'Hồ sơ khác không hợp lệ');
                return;
            }
        }        
        // check logic part 1 or 2
        /*var questionPartTwoAnswers = $.grep($scope.questions, function(question) {
            return ((question.question_part_code === appConfig.questionPart.two) && 
                (!appService.isUndefinedOrNull(question.answers)) && (question.answers.length > 0));
        });
        var questionPartThreeAnswers = $.grep($scope.questions, function(question) {
            return ((question.question_part_code === appConfig.questionPart.three) && 
                (!appService.isUndefinedOrNull(question.answers)) && (question.answers.length > 0));
        });         
        if (questionPartTwoAnswers.length > 0 && questionPartThreeAnswers.length > 0) {
            toaster.pop('error', invalidRegisterInformationMessage);
            $scope.invalidQuestionPartTwo = true;
            return;             
        }
        //
        var questionPartTwo = checkOptionQuestionPart(appConfig.questionPart.two, questionPartTwoAnswers);
        var questionPartThree = checkOptionQuestionPart(appConfig.questionPart.three, questionPartThreeAnswers);
        if (!questionPartTwo || !questionPartThree) {
            $scope.invalidQuestionPartTwo = true;
            toaster.pop('error', invalidRegisterInformationMessage);
            return;
        }*/

        // part two
        var questionPartTwo = $.grep($scope.questions, function(question) {
            return (question.question_part_code === appConfig.questionPart.two);
        });
        var answerPartTwo = $.grep($scope.questions, function(question) {
            return ((question.question_part_code === appConfig.questionPart.two) && 
                (!appService.isUndefinedOrNull(question.answers)) && (question.answers.length > 0));
        });
        // part three
        var questionPartThree = $.grep($scope.questions, function(question) {
            return (question.question_part_code === appConfig.questionPart.three);
        });
        var answerPartThree = $.grep($scope.questions, function(question) {
            return ((question.question_part_code === appConfig.questionPart.three) && 
                (!appService.isUndefinedOrNull(question.answers)) && (question.answers.length > 0));
        });
        if (answerPartTwo.length > 0 && questionPartTwo.length != answerPartTwo.length) {
            $scope.invalidQuestionPartTwo = true;
            toaster.pop('error', invalidRegisterInformationMessage);
            return;
        }
        if (answerPartThree.length > 0 && questionPartThree.length != answerPartThree.length) {
            $scope.invalidQuestionPartTwo = true;
            toaster.pop('error', invalidRegisterInformationMessage);
            return;
        }
        if (answerPartTwo.length === 0 && answerPartThree.length === 0) {
            $scope.invalidQuestionPartTwo = true;
            toaster.pop('error', invalidRegisterInformationMessage);
            return;
        }                                


    	// captcha
        $log.debug('Sending the captcha response to the server ' +  $scope.response);
		if (invalid) {
    		$log.debug('Form error=' + error);
    		toaster.pop('error', invalidRegisterInformationMessage);
    		return;
    	}
    	// update post data
    	var studentStaffs = [];
    	angular.forEach($scope.data.studentStaffs, function(studentStaff) {
    		studentStaffs.push(studentStaff.id.toString());
    	});
    	var answers = [];
    	var tmpAnswer = null;
    	var values = [];
    	var multiChoiceRichText = false;
    	var tmpAnswers = null;
    	angular.forEach($scope.questions, function(question) {
            if (!appService.isUndefinedOrNull(question.answers) && question.answers.length > 0) {
                values.length = 0;
                tmpAnswer = {
                    id: question.id.toString()
                };
                if (appConfig.questionType.multiChoiceRichText == question.question_type_code) {
                    multiChoiceRichText = true;
                } else {
                    multiChoiceRichText = false;
                }
                $log.debug('Question id=' + question.id + ', name=' + question.name + ' multiChoiceRichText=' + multiChoiceRichText);           
                angular.forEach(question.answers, function(answer) {
                    $log.debug('Answer id=' + answer.id + ', name=' + answer.name);
                    if (multiChoiceRichText) {
                        tmpAnswers = $.grep(question.answer_ids, function(defautAnswer) {
                            return defautAnswer.id === answer.id;
                        });
                        if (tmpAnswers.length > 0) {
                            $log.debug('Text=' + tmpAnswers[0].text);
                            values.push(answer.name + " '-' " + tmpAnswers[0].text);
                        }
                    } else {
                        values.push(answer.name);
                    }
                });
                tmpAnswer.value = angular.copy(values);
                answers.push(angular.copy(tmpAnswer));
            }
    	});    	
    	var data = {
    		program_event_id: programEventId, // confirm later
    		program_type: appConfig.programType,
			last_name: $scope.data.lastName,
			first_name: $scope.data.firstName,
			birthday: $filter('date')($scope.data.birthday, 'yyyy-MM-dd'),
			gender: $scope.data.gender,
			email: $scope.data.email,
			mobile_phone: $scope.data.mobilePhone,
			facebook_link: $scope.data.facebookLink,
			permanent_address: $scope.data.permanentAddress,
			identification_no: $scope.data.identificationNo,
			school_id: $scope.data.school.id.toString(),
			speciality_id: $scope.data.speciality.id.toString(),
			student_code: $scope.data.studentCode,
			recruitment_degree_id: $scope.data.recruitmentDegree.id.toString(),
			time_graduation_id: $scope.data.timeGraduation.id.toString(),
			cumulative_gpa: $scope.data.cumulativeGpa.toString(),
			student_staff_ids: studentStaffs,
			recruitment_source_id: $scope.data.knowFresher.id.toString(),
			test_result_ids: answers
		};
		// file CV
    	if (!appService.isUndefinedOrNull($scope.data.fileCV)) {
    		data.resume = {
    			filename: $scope.data.fileCV.filename,
    			datas: $scope.data.fileCV.base64
    		};
    	}
		// file other
    	if (!appService.isUndefinedOrNull($scope.data.fileOther)) {
    		data.compress = {
    			filename: $scope.data.fileOther.filename,
    			datas: $scope.data.fileOther.base64
    		};
    	}    	
		$log.debug('Post data=' + angular.toJson(data));
		appService.register(data).then(function(response) {
    		if (appConfig.responseCode.ok === response.code) {
	           toaster.pop('info', 'Bạn đã đăng ký thành công');
	        } else {
	           toaster.pop('error', 'Bạn đã đăng ký không thành công');
	        }
    	});
		// reload captcha
        vcRecaptchaService.reload($scope.widgetId);
    }; 
    
    $scope.initialize();
	
}