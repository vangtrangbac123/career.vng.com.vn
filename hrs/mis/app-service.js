'use strict';

angular.module('app').service('appService', ['$http', '$q', 'DSCacheFactory', 'appConfig', 
	function($http, $q, DSCacheFactory, appConfig) {


	this.getTypicalFace = function(faceType) {
        var deferred = $q.defer();
        var hrsCacheData = DSCacheFactory.get(appConfig.cache.key);
        if (hrsCacheData.get(appConfig.cache.typicalFace)) {
            deferred.resolve(hrsCacheData.get(appConfig.cache.typicalFace));
        } else {
			var formData = {face_type: faceType};
        		$http.post(appConfig.restURL.concat('hrsystem/http/vhr/getTypicalFace'), formData, {
        			headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
            	}
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                hrsCacheData.put(appConfig.cache.typicalFace, data.data);
	                deferred.resolve(data.data);                
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        }); 
        }
        return deferred.promise;        
    };		

	this.getProgramEvent = function(programType) {
        var deferred = $q.defer();
        var hrsCacheData = DSCacheFactory.get(appConfig.cache.key);
        if (hrsCacheData.get(appConfig.cache.programEvent)) {
            deferred.resolve(hrsCacheData.get(appConfig.cache.programEvent));
        } else {
			var formData = {program_type: programType};
	        $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getProgramEvent'), formData, {
	        	headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
	        	}
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                hrsCacheData.put(appConfig.cache.programEvent, data.data);
	                deferred.resolve(data.data);                
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        }); 
        }
        return deferred.promise;       
    };	
	
	this.getSchool = function(programEventId) {
		var formData = {program_event_id: programEventId};
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getSchool'), formData, {
        		headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
            	}
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

 	this.getSpeciality = function() {
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getSpecialities')
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

 	this.getRecruitmentDegree = function() {
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getRecruitmentDegree')
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

 	this.getTimeGraduation = function() {
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getTimeGraduation')
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

	this.getStudentStaff = function() {
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getStudentStaff')
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

	this.getKnowFresher = function(programEventId) {
		var formData = {program_event_id: programEventId};
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getKnowFresher'), formData, {
        		headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
            	}
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

	this.getListQuestion = function(programEventId) {
		var formData = {program_event_id: programEventId};
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/getListQuestion'), formData, {
        		headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
            	}
	        ).then(function success(response) {
	            var data = response.data;
	            if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }
	        }, function error(response) {
	        	// alert message
	        });        
    };

	this.register = function(data) {
        return $http.post(appConfig.restURL.concat('hrsystem/http/vhr/receiveApplicationFromFe'), data, {
        		headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
            	}
	        ).then(function success(response) {
	            var data = response.data;
	            return data;
	            /*if (appConfig.responseCode.ok === data.code) {
	                return data.data;
	            } else {
	                // alert message
	            }*/
	        }, function error(response) {
	        	// alert message
	        });        
    };

	this.acceptFileCV = function(fileName) {
    	fileName = angular.lowercase(fileName);
 		var exp = /^.*\.(doc|docx|xls|xlsx|ppt|pptx|pdf|jpg)$/;         
 		return exp.test(fileName);  
	};

	this.acceptFileOther = function(fileName) {
    	fileName = angular.lowercase(fileName);
 		var exp = /^.*\.(docx|xls|xlsx|ppt|pptx|pdf|jpg|rar|zip|7z)$/;         
 		return exp.test(fileName);  
	};

	this.isUndefinedOrNull = function(obj) {
		return angular.isUndefined(obj) || obj === null;
	};	                                  

}]);