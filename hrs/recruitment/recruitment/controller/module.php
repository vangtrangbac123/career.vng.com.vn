﻿<?php
require("PHPMailer/class.phpmailer.php");
class Application_Feature_Recruitment_Controller_Module extends Application_Feature_Feature
{
    
   /**
    * @access  private
    * @name $_model
    * @var Object
    */
    private $_model;
         
   /**
    * @access  private
    * @name $_isAjax
    * @var Object
    */
    private $_isAjax;

   /**
    * function: __construct
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function __construct( )
    {
        //----------------------------------------------
        // get ajax status
        //----------------------------------------------
        $this->_isAjax = Zend_Registry::get('isAjax');
    }

   /**
    * function: execute
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function execute( )
    {
		$this->_config['siteUrl'] = vPortal_Utility::getConfig( 'application', 'siteUrl' );

        //----------------------------------------------
        // get common info and store in config
        //----------------------------------------------
        $this->_config['sectionPath'] = Zend_Registry::get( 'sectionPath' );                     
		
		//----------------------------------------------
		// check input
		//----------------------------------------------
		if( empty( $this->_config ) )
		{			
			//----------------------------------------------
			// define error message
			//----------------------------------------------
			$errorMsg = 'There has a problem in getting configuration of ' .
						__CLASS__ . '. Maybe its caused by I/O of harddisk ' .
						'or has an error in generating this configuration file. ' .
						'Please check config again.';

			//----------------------------------------------
			// process error
			//----------------------------------------------
			vPortal_Error::userError( 'feature', $this->_config['featureCode'],
									  vPortalX_Error_Code::VPORTAL_MODULE_CONFIG_ERROR,
									  $errorMsg );
		}


		//----------------------------------------------
		// get common info
		//----------------------------------------------
		$this->_config['isAjax'] = $this->_isAjax;

		//----------------------------------------------
		// balancing
		//----------------------------------------------
				
		switch( $this->_config['action'] )
		{
			case 'newsletter':
				$this->_output = $this->_subscribeNewsLetter();
				break;
			case 'stu-newsletter':                
				$this->_output = $this->_subscribeStudentNewsLetter();                                
				break;
			case 'verify':
				$this->_output = $this->_verifyEmail();
				break;
			case 'fresher':
			case 'internship':
			case 'tour':
				$this->_output = $this->_showProgramListByCode();                                
				break;
			case 'face-fresher':
				$this->_output = $this->_showListFaceFresher();                                
				break;
			case 'registertour':
				$this->_output = $this->_showFormRegisterTour();                                
				break;
			case 'registerinternship':
				$this->_output = $this->_showFormRegisterInternship();                                
				break;
			case 'registerfresher':
				$this->_output = $this->_showFormRegisterFresher();                                
				break;
			case 'submitcv':
				$this->_output = $this->_showFormSubmitCv();                                
				break;	
			case 'sharejob':
				$this->_output = $this->_showFormShareJob();                                
				break;
			case 'job':
				$this->_output = $this->_showJobList();                                
				break;
			default:
				break;
		}
        
    }
    
    /**
     * function: _subscribeNewsLetter
     * description: Đăng kí nhận bản tin tuyển dụng
     */     
    function _subscribeNewsLetter(){
        //----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $msg = array();
            $post = Zend_Registry::get('post');
			
			$full_name_check = $this->convertVN($post['member']['full_name']);
            if(empty($post['member']['full_name'])){
               $msg['full_name'] = $this->_config['emptyName']; 
            }elseif(strlen(trim($post['member']['full_name'])) < 2 || strlen(trim($post['member']['full_name']))> 100){
                $msg['full_name'] = $this->_config['errName'];
            }elseif($full_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $full_name_check)) {
			    $msg['full_name']= $this->_config['errName2'];	  
            }
			 
			if(empty($post['member']['email'])){
				$msg['email'] = $this->_config['emptyEmail']; 
            }elseif($post['member']['email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['email']))) {
			    $msg['email']= $this->_config['errEmail'];	  
            }
				
            if(!empty($msg)){
                echo json_encode(array(0,$msg));
            }else{ 
				$post['member']['code'] = $this->_config['code'];
				$params = array(
				   'name' => $post['member']['full_name'],
				   'email' => $post['member']['email'],
				   'program_type_ids' => array($post['member']['code'])	   		   
				);
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->subscribeNewsLetter($postData); 

				if(isset($result['code']) && $result['code'] == 1){
					$resData = (array)$result['data'];
					$tmp = $resData['email'].'#'.$resData['id'].'#'.time();
					$timeEpx = time() + 86400;
					$key = '79301e86dabb1646050580b14e5bf9ee';
					$dataEncode = $this->encodeLinkVerify($tmp, $key);

					$linkVerify = $this->_config['siteUrl'].'/'.$this->_config['sectionVerify'].'/'.$dataEncode.'.html';

					$dataSend = array(
						'from_name' => $this->_config['fromName'],
						'from_email' => $this->_config['fromEmail'],
						'to_name' => $post['member']['full_name'],
						'to_email' => $resData['email'],
						'linkActive' => $linkVerify,
						'timeExp' => $timeEpx,
						'strPr' => ''
					);
					$res = $this->_sendMailVerify($dataSend); 
					if($res){
						$msg['sucss'] = $this->_config['sucssSendMail'];
						echo json_encode(array(0,$msg));
					}else{
						$msg['err'] = $this->_config['errSendMail'];
						echo json_encode(array(0,$msg));
					} 
					
				}else{
					$msg['sucss'] = $this->_config['errSubscribe'];
					echo json_encode(array(0,$msg));
				}               
            }
            exit();
        }
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );
        
        
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
    }
	
	/**
     * function: _subscribeNewsLetter
     * description: Đăng kí nhận bản tin dành cho sinh viên
     */     
    function _subscribeStudentNewsLetter(){
        //----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $msg = array();
            $post = Zend_Registry::get('post');
			
            $full_name_check = $this->convertVN($post['member']['full_name']);
            if(empty($post['member']['full_name'])){
               $msg['full_name'] = $this->_config['emptyName']; 
            }elseif(strlen(trim($post['member']['full_name'])) < 2 || strlen(trim($post['member']['full_name']))> 100){
                $msg['full_name'] = $this->_config['errName'];
            }elseif($full_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $full_name_check)) {
			    $msg['full_name']= $this->_config['errName2'];	  
            } 
			
			if(empty($post['member']['email'])){
				$msg['email'] = $this->_config['emptyEmail']; 
            }elseif($post['member']['email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['email']))) {
			    $msg['email']= $this->_config['errEmail'];	  
            }
			$check = 0;
			if(isset($post['member']['fresher']) 
				|| isset($post['member']['internship']) 
				|| isset($post['member']['vngTour'])){
				$check = 1;
			}	
			if($check == 0){
				$msg['program'] = $this->_config['errProgram'];
			}

            if(!empty($msg)){
                echo json_encode(array(0,$msg));
            }else{ 
				$arrProgram = array();
				if(isset($post['member']['fresher'])){
					$arrProgram[] = $post['member']['fresher'];
				}
				if(isset($post['member']['internship'])){
					$arrProgram[] = $post['member']['internship'];
				}
				if(isset($post['member']['vngTour'])){
					$arrProgram[] = $post['member']['vngTour'];
				}
				$params = array(
				   'name' => $post['member']['full_name'],
				   'email' => $post['member']['email'],
				   'program_type_ids' => $arrProgram	   		   
				);
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->subscribeNewsLetter($postData); 
				if(isset($result['code']) && $result['code'] == 1){
					$resData = (array)$result['data'];
					$tmp = $resData['email'].'#'.$resData['id'].'#'.time();
					$timeEpx = time() + 86400;
					$dataEncode = base64_encode($tmp);
					$linkVerify = $this->_config['siteUrl'].'/'.$this->_config['sectionVerify'].'/'.$dataEncode.'.html';
					
					$arrPr = array();
					
					if(isset($post['member']['fresher'])){
						$arrPr[] = 'VNG Fresher';
					}
					if(isset($post['member']['internship'])){
						$arrPr[] = 'Internship';
					}
					if(isset($post['member']['vngTour'])){
						$arrPr[] = 'VNG Tour';
					}
					if(count($arrPr) > 0){
						$strPr = implode(', ', $arrPr);
					}else{
						$strPr = '';
					}
					
					$dataSend = array(
						'from_name' => $this->_config['fromName'],
						'from_email' => $this->_config['fromEmail'],
						'to_name' => $post['member']['full_name'],
						'to_email' => $resData['email'],
						'linkActive' => $linkVerify,
						'timeExp' => $timeEpx,
						'strPr' => $strPr
					);
					$res = $this->_sendMailVerify($dataSend); 
					if($res){
						$msg['sucss'] = $this->_config['sucssSendMail'];
						echo json_encode(array(0,$msg));
					}else{
						$msg['err'] = $this->_config['errSendMail'];
						echo json_encode(array(0,$msg));
					} 				
				}else{
					$msg['sucss'] = $this->_config['errSubscribe'];
					echo json_encode(array(0,$msg));
				}
            }
            exit();
        }
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );
        
        
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
    }
	
	function _verifyEmail(){
		$output = '';
        $dataOutput = array();
		$this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$dataRaw = isset($this->_params[0]) ? $this->_params[0] : '';
		$key = '79301e86dabb1646050580b14e5bf9ee';
		$dataDecode = $this->decodeLinkVerify($dataRaw, $key);
		$dataArr = explode ('#', $dataDecode);

		if(count($dataArr) != 3){
			$dataOutput['mess'] = $this->_config['urlInvalid'];
		}else{
			$timeExp = isset($dataArr[2]) ? $dataArr[2] : 0;
			if(time() - $timeExp > 86400){ // 24h
				$dataOutput['mess'] = $this->_config['exprTime'];
			}else{
				$params = array(
				   'email' => $dataArr['0'],
				   'id' => $dataArr['1']   		   
				);
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->activeSubscribeNewsLetter($postData); 
				if(isset($result['code']) && $result['code'] == 1){
					$dataOutput['mess'] = $this->_config['sucssActive'];
				}else{
					$dataOutput['mess'] = $this->_config['errActive'];
				}
			}
		}
		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
													 $this->_config['templateOutput'],
													 $dataOutput, $this->_config );
		return $output;											 
	}
	
	function _showProgramListByCode(){
		$output = '';
        $dataOutput = array();
		$this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		if(isset($this->_params[0]) && ($this->_params[0] == 'chi-tiet' || $this->_params[0] == 'detail')){
			$data = array();
			$params = array(
				'program_type' => $this->_config['code']
			);
			$postData = '';
			$postData = json_encode($params);
			$result = $this->_model->getListProgramByCode($postData);
			if(isset($result['code']) && $result['code'] == 1){
				$data = $result['data'];
				foreach($data as $k => $v){
					$data[$k] = (array)$v;			
				}
			}
			$id = isset($this->_params[2]) ? $this->_params[2] : 0;
			foreach($data as $k => $v){
				if($v['id'] == $id){
					$dataOutput = $v;
				}		
			}
			// Convert name SEO
			$dataOutput['date_to'] = strtotime($dataOutput['date_to']);
			$dataOutput['date_from'] = strtotime($dataOutput['date_from']);
			$dataOutput['name_seo'] = $this->convertTitle($dataOutput['name']);
			$dataOutput['name_en_seo'] = $this->convertTitle($dataOutput['name_en']);

			$output = vPortal_Utility::loadView( $this->_config['featureCode'],
													 $this->_config['templateContent'],
													 $dataOutput, $this->_config );
		}else{

			$output = vPortal_Utility::loadView( $this->_config['featureCode'],
													 $this->_config['templateDefault'],
													 $dataOutput, $this->_config );
		}
        		
		return $output;									
	}
	
	function _showListFaceFresher(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		$result = $this->_model->getListFaceFresher();
		
		if(isset($result['code']) && $result['code'] == 1){
			$dataOutput = $result['data'];
			foreach($dataOutput as $k => $v){
				$dataOutput[$k] = (array)$v;
			}
		}

		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateList'],
                                                 $dataOutput, $this->_config );
		return $output;									
	}
	
	
	function _showFormRegisterTour(){
        //----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();
		$dataOutput['err'] = array();
		$dataOutput['sucss'] = array();
		$dataOutput['cv'] = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$dataOutput['captchaurl'] = $this->_config['siteUrl'] . '/captcha/danh-sach.html';
		$tmplastCaptcha = vPortal_Session::get('lastCaptcha');
		$urlCaptcha = PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general/'.$tmplastCaptcha.'.png';
		if(file_exists($urlCaptcha)){
			unlink($urlCaptcha);   
		}

        $captcha=new Zend_Captcha_Image();
        $captcha->setWordLen('6')
                ->setHeight('60')
                ->setFont(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'ariblk.ttf')
                ->setImgDir(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5'); 
				
		$programId = isset($this->_params[1]) ? $this->_params[1] : 0;		
		$params = array(
			'program_event_id' => $programId,
			//$this->_config['code']
		);
		$postData = "";
		$postData = json_encode($params);	
		$school = $this->_model->getListSchool($postData);	
		if(isset($school['code']) && $school['code'] == 1){
			$dataOutput['school'] = $school['data'];
			foreach($dataOutput['school'] as $k => $v){
				$dataOutput['school'][$k] = (array)$v;
			}
		}
		
		foreach($dataOutput['school'] as $k => $v){
			$dataOutput['school'][$k]['name_seo'] = $this->convertTitle($v['name']);
			$dataOutput['school'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
		}
		
		uasort($dataOutput['school'], function($a, $b){
			return strcmp($a["name_seo"], $b["name_seo"]);
		});
		
		$degree = $this->_model->getListDegree();	
		if(isset($degree['code']) && $degree['code'] == 1){
			$dataOutput['degree'] = $degree['data'];
		}else{
			$dataOutput['degree'] = array();
		}
			
		$graduation = $this->_model->getTimeGraduation();	
		if(isset($graduation['code']) && $graduation['code'] == 1){
			$dataOutput['graduation'] = $graduation['data'];
			foreach($dataOutput['graduation'] as $k => $v){
				$dataOutput['graduation'][$k] = (array)$v;
			}
		}
		
		$postData = json_encode($params);		
		$knowfresher = $this->_model->getKnowFresher($postData);	
		if(isset($knowfresher['code']) && $knowfresher['code'] == 1){
			$dataOutput['knowfresher'] = $knowfresher['data'];
			foreach($dataOutput['knowfresher'] as $k => $v){
				$dataOutput['knowfresher'][$k] = (array)$v;
			}
		}

		$params = array(
			'program_type' => $this->_config['code']
		);
		$pData = '';
		$pData = json_encode($params);
		$program = $this->_model->getListProgramByCode($pData);	
		if(isset($program['code']) && $program['code'] == 1){
			$dataOutput['program'] = $program['data'];
			foreach($dataOutput['program'] as $k => $v){
				$dataOutput['program'][$k] = (array)$v;
			}
		}	
		#$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
		$seoName = isset($this->_params[0]) ? $this->_params[0] : '';
		$dataOutput['programId'] = $programId;
		$dataOutput['name-seo'] = $seoName;
		
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $post = Zend_Registry::get('post');
			
			$captchaData = $post['captcha'];
			if(!$captcha->isValid($captchaData)){
				$dataOutput['err']['captcha'] = $this->_config['errCaptcha']; 
			}
            // Get the captcha data
            $captchaDataChar = trim($post['captcha']["input"]);
			if($captchaDataChar == ''){
				$dataOutput['err']['captcha'] = $this->_config['emptCaptcha']; 
			}
            $last_name_check = $this->convertVN($post['member']['last_name']);
            if(empty($post['member']['last_name'])){
               $dataOutput['err']['last_name'] = $this->_config['emptyLName']; 
            }elseif(strlen(trim($post['member']['last_name'])) < 2 || strlen(trim($post['member']['last_name']))> 100){
                $dataOutput['err']['last_name'] = $this->_config['errLName'];
            }elseif($last_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $last_name_check)) {
			    $dataOutput['err']['last_name']= $this->_config['errLName2'];	  
            }
			
			$first_name_check = $this->convertVN($post['member']['first_name']);
            if(empty($post['member']['first_name'])){
               $dataOutput['err']['first_name'] = $this->_config['emptyFName']; 
            }elseif(strlen(trim($post['member']['first_name'])) < 2 || strlen(trim($post['member']['first_name']))> 100){
                $dataOutput['err']['first_name'] = $this->_config['errFName'];
            }elseif($first_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $first_name_check)) {
			    $dataOutput['err']['first_name']= $this->_config['errFName2'];	  
            }
			
			if(empty($post['member']['email'])){
				$dataOutput['err']['email'] = $this->_config['emptyEmail']; 
            }elseif($post['member']['email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['email']))) {
			    $dataOutput['err']['email']= $this->_config['errEmail'];	  
            }
			$birthday = $post['member']['birthday'];
			$arrB = explode ('/', $birthday);
			if($birthday == ''){
				$dataOutput['err']['birthday'] = $this->_config['emptyBirthday']; 
            }elseif(strlen(trim($birthday)) > 10 || strlen(trim($birthday)) < 8 || count($arrB) != 3){
				$dataOutput['err']['birthday'] = $this->_config['errBirthday']; 		
			}
			$birthdayIn = trim($arrB['2']).'-'.trim($arrB['1']).'-'.trim($arrB['0']);
			
			if(empty($post['member']['program'])){
               $dataOutput['err']['program'] = $this->_config['emptyProgram']; 
            }
			
			if(empty($post['member']['school'])){
               $dataOutput['err']['school'] = $this->_config['emptySchool']; 
            }
			
			if(empty($post['member']['degree'])){
               $dataOutput['err']['degree'] = $this->_config['emptyDegree']; 
            }
			
			if(empty($post['member']['graduation'])){
               $dataOutput['err']['graduation'] = $this->_config['emptyGraduation']; 
            }
			
			if(empty($post['member']['gender'])){
               $dataOutput['err']['gender'] = $this->_config['emptyGender']; 
            }
			
			if(empty($post['member']['knowfresher'])){
               $dataOutput['err']['knowfresher'] = $this->_config['emptyKnowfresher']; 
            }
			
			if(empty($post['member']['speciality'])){
                $dataOutput['err']['speciality'] = $this->_config['emptySpeciality']; 
            }elseif(strlen(trim($post['member']['speciality']))<2 || strlen(trim($post['member']['speciality']))>100){
                $dataOutput['err']['first_name'] = $this->_config['errSpeciality'];
            }
				
			if(empty($post['member']['phone'])){
				$dataOutput['err']['phone'] = $this->_config['emptyPhone']; 
            }elseif(!empty($post['member']['phone']) && !preg_match("/^[0-9\-\+]{9,14}$/", $post['member']['phone'])){
                $dataOutput['err']['phone'] = $this->_config['errPhone'];                             
            }
			
			if(strlen(trim($post['member']['pernament_address'])) > 255){
                $dataOutput['err']['pernament_address'] = $this->_config['errAddress'];
            }
			
			if(!empty($post['member']['identification_no']) 
				&& (strlen(trim($post['member']['identification_no']))<9 
					|| strlen(trim($post['member']['identification_no']))>11
					|| !is_numeric($post['member']['identification_no']))){
                $dataOutput['err']['identification_no'] = $this->_config['errIdentificationNo'];
            }
			
			if(empty($post['member']['cumulative_gpa'])){
				$dataOutput['err']['cumulative_gpa'] = $this->_config['emptyCumulativeGpa']; 
            }elseif(strlen(trim($post['member']['cumulative_gpa']))<1 || strlen(trim($post['member']['cumulative_gpa']))> 4){
                $dataOutput['err']['cumulative_gpa'] = $this->_config['errCumulativeGpa'];
            }elseif(!is_numeric($post['member']['cumulative_gpa']) 
				|| (is_numeric($post['member']['cumulative_gpa']) && intval($post['member']['cumulative_gpa']) > 10)){
				$dataOutput['err']['cumulative_gpa'] = $this->_config['errCumulativeGpa'];
			}

			if($_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv'] = $this->_config['errCv']; 
			}
			if($_FILES['file']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg');
				$filename = $_FILES['file']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv'] = $this->_config['errCv2']; 
				}
			}						
			/*
			if($_FILES['file']['error'] != 0){
				$dataOutput['err']['cv'] = $this->_config['emptyCv']; 
			}
			*/
			if($_FILES['file2']['error'] == 0 && $_FILES['file2']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv2'] = $this->_config['errCv3']; 
			}
			if($_FILES['file2']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg', 'RAR', 'rar', 'ZIP', 'zip', '7z', '7Z');
				$filename = $_FILES['file2']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv2'] = $this->_config['errCv4']; 
				}
			}
			
			$dataOutput['member']['first_name'] = $post['member']['first_name'];
			$dataOutput['member']['last_name'] = $post['member']['last_name'];
			$dataOutput['member']['email'] = $post['member']['email'];
			$dataOutput['member']['identification_no'] = $post['member']['identification_no'];
			$dataOutput['member']['birthday'] = $post['member']['birthday'];
			$dataOutput['member']['speciality'] = $post['member']['speciality'];
			$dataOutput['member']['graduation'] = $post['member']['graduation'];
			$dataOutput['member']['phone'] = $post['member']['phone'];
			$dataOutput['member']['cumulative_gpa'] = $post['member']['cumulative_gpa'];
			$dataOutput['member']['knowfresher'] = $post['member']['knowfresher'];
			$dataOutput['member']['school'] = $post['member']['school'];
			$dataOutput['member']['degree'] = $post['member']['degree'];
			$dataOutput['member']['pernament_address'] = $post['member']['pernament_address'];
			$dataOutput['member']['gender'] = $post['member']['gender'];
			$dataOutput['member']['program'] = $post['member']['program'];
			
            $dataOutput['captcha'] = $captcha->generate();
			vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
			// clear captcha
			unset($post['captcha']);
			if(!empty($dataOutput['err'])){
			
			

				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
			}else{		
				$content = file_get_contents($_FILES['file']['tmp_name']);
				$dataCv = base64_encode($content);
				$filename = $_FILES['file']['name'];
				
				$content2 = file_get_contents($_FILES['file2']['tmp_name']);
				$dataCv2 = base64_encode($content2);
				$filename2 = $_FILES['file2']['name'];
				
				
				$params = array(
				   'last_name' => $post['member']['last_name'],
				   'first_name' => $post['member']['first_name'],
				   'birthday' => $birthdayIn,
				   'gender' => $post['member']['gender'],
				   'mobile_phone' => $post['member']['phone'],
				   'school_id' => $post['member']['school'],
				   'recruitment_degree_id' => $post['member']['degree'],
				   'email' => $post['member']['email'],
				   'speciality' => $post['member']['speciality'],
				   'identification_no' => $post['member']['identification_no'],
				   'pernament_address' => $post['member']['pernament_address'],
				   'cumulative_gpa' => $post['member']['cumulative_gpa'],
				   'recruitment_source_id' => $post['member']['knowfresher'],			   
				   'time_graduation_id' => $post['member']['graduation'],
				   'program_event_id' => $post['member']['program'],	
				   'program_type' => $this->_config['code'],						
				);
				if($_FILES['file2']['error'] == 0){
					$params['compress'] = array(
						'datas' => $dataCv2,
						'filename' => $filename2
					);
				}
				
				if($_FILES['file']['error'] == 0){
					$params['resume'] = array(
						'datas' => $dataCv,
						'filename' => $filename
					);
				}
 				
				$postData = '';
				$postData = json_encode($params);

				$result = $this->_model->registerTour($postData); 	

				if(isset($result['code']) && $result['code'] == 1){
					$dataOutput['sucss']['process'] = $this->_config['sucssRegister'];
				}else{
					$dataOutput['err']['process'] = $this->_config['errRegister'];
				}
				$dataOutput['captcha'] = $captcha->generate();
				vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
				// clear captcha
				unset($post['captcha']);
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
				
            }
        }
		
		$dataOutput['captcha'] = $captcha->generate();
		vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
		// clear captcha
		unset($dataOutput['cv']['captcha']);
	
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );
        
        
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
    }
	/*
	function _showFormRegisterTour(){
        //----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $msg = array();
            $post = Zend_Registry::get('post');
			
			$last_name_check = $this->convertVN($post['member']['last_name']);
            if(empty($post['member']['last_name'])){
               $msg['last_name'] = $this->_config['emptyLName']; 
            }elseif(strlen(trim($post['member']['last_name'])) < 2 || strlen(trim($post['member']['last_name']))> 100){
                $msg['last_name'] = $this->_config['errLName'];
            }elseif($last_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $last_name_check)) {
			    $msg['last_name']= $this->_config['errLName2'];	  
            }
			
			$first_name_check = $this->convertVN($post['member']['first_name']);
            if(empty($post['member']['first_name'])){
               $msg['first_name'] = $this->_config['emptyFName']; 
            }elseif(strlen(trim($post['member']['first_name'])) < 2 || strlen(trim($post['member']['first_name']))> 100){
                $msg['first_name'] = $this->_config['errFName'];
            }elseif($first_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $first_name_check)) {
			    $msg['first_name']= $this->_config['errFName2'];	  
            }			
			
			if(empty($post['member']['email'])){
				$msg['email'] = $this->_config['emptyEmail']; 
            }elseif($post['member']['email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['email']))) {
			    $msg['email']= $this->_config['errEmail'];	  
            }
			
			if(empty($post['member']['birthday'])){
               $msg['birthday'] = $this->_config['emptyBirthday']; 
            }
			
			if(empty($post['member']['school'])){
               $msg['school'] = $this->_config['emptySchool']; 
            }
			
			if(empty($post['member']['graduation'])){
               $msg['graduation'] = $this->_config['emptyGraduation']; 
            }
			
			if(empty($post['member']['speciality'])){
               $msg['speciality'] = $this->_config['emptySpeciality']; 
            }elseif(strlen(trim($post['member']['speciality']))<2 || strlen(trim($post['member']['speciality']))>100){
                $msg['first_name'] = $this->_config['errSpeciality'];
            }
			
			if(empty($post['member']['program'])){
               $msg['program'] = $this->_config['emptyProgram']; 
            }
			
			if(empty($post['member']['phone'])){
				$msg['phone'] = $this->_config['emptyPhone']; 
            }elseif(!empty($post['member']['phone']) && !preg_match("/^[0-9\-\+]{9,14}$/", $post['member']['phone'])){
                $msg['phone'] = $this->_config['errPhone'];                             
            }
	
            if(!empty($msg)){
                echo json_encode(array(0,$msg));
            }else{						
				$params = array(
				   'last_name' => $post['member']['last_name'],
				   'first_name' => $post['member']['first_name'],
				   'birthday' => $post['member']['birthday'],
				   'mobile_phone' => $post['member']['phone'],
				   'school_id' => $post['member']['school'],
				   'email' => $post['member']['email'],
				   'speciality' => $post['member']['speciality'],
				   'time_graduation_id' => $post['member']['graduation'],
				   'program_event_id' => $post['member']['program'],	
				   'program_type' => $this->_config['code'],				   
				);
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->registerTour($postData); 
				if(isset($result['code']) && $result['code'] == 1){
					$msg['sucss'] = $this->_config['sucssRegister'];
					echo json_encode(array(0,$msg));
				}else{
					$msg['err'] = $this->_config['errRegister'];
					echo json_encode(array(0,$msg));
				}  
				
            }
            exit();
        }
		
		$school = $this->_model->getListSchool();	
		if(isset($school['code']) && $school['code'] == 1){
			$dataOutput['school'] = $school['data'];
			foreach($dataOutput['school'] as $k => $v){
				$dataOutput['school'][$k] = (array)$v;
			}
		}
		
		$graduation = $this->_model->getTimeGraduation();	
		if(isset($graduation['code']) && $graduation['code'] == 1){
			$dataOutput['graduation'] = $graduation['data'];
			foreach($dataOutput['graduation'] as $k => $v){
				$dataOutput['graduation'][$k] = (array)$v;
			}
		}
		
		$params = array(
			'program_type' => $this->_config['code']
		);
		$postData = '';
		$postData = json_encode($params);
		$program = $this->_model->getListProgramByCode($postData);	
		if(isset($program['code']) && $program['code'] == 1){
			$dataOutput['program'] = $program['data'];
			foreach($dataOutput['program'] as $k => $v){
				$dataOutput['program'][$k] = (array)$v;
			}
		}
			
		//$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
		//$dataOutput['programId'] = $programId;
		//$dataOutput['captcha'] = $captcha->generate();
		//vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
		// clear captcha
		//unset($dataOutput['cv']['captcha']);
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );
        
        
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
    }
	*/
	
	function _showFormSubmitCv(){
        //----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();
		$dataOutput['err'] = array();
		$dataOutput['sucss'] = array();
		$dataOutput['cv'] = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		$dataOutput['captchaurl'] = $this->_config['siteUrl'] . '/captcha/danh-sach.html';
		$tmplastCaptcha = vPortal_Session::get('lastCaptcha');
		$urlCaptcha = PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general/'.$tmplastCaptcha.'.png';
		if(file_exists($urlCaptcha)){
			unlink($urlCaptcha);   
		}

        $captcha=new Zend_Captcha_Image();
        $captcha->setWordLen('6')
                ->setHeight('60')
                ->setFont(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'ariblk.ttf')
                ->setImgDir(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5'); 
		
		$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
		$seoName = isset($this->_params[0]) ? $this->_params[0] : '';
		$dataOutput['programId'] = $programId;
		$dataOutput['name-seo'] = $seoName;
		$res = $this->_model->getCountry(); 

		if(isset($res['code']) && $res['code'] == 1){
			$dataOutput['country']= $res['data'];
		}
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$post = Zend_Registry::get('post');
			$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
			if($programId == 0){
				$dataOutput['err']['process'] = $this->_config['errProcess'];
			}
			$first_name = isset($post['first_name'])? $post['first_name']: '';
			$last_name = isset($post['last_name'])? $post['last_name']: '';
			$email = isset($post['email']) ?  $post['email'] : '';
			$note = isset($post['note']) ?  trim($post['note']) : '';
			$birthday = isset($post['birthday']) ?  $post['birthday'] : '';
			$gender = isset($post['gender']) ?  $post['gender'] : 0;
			$country = isset($post['country']) ?  $post['country'] : 0;
			$phone = isset($post['phone']) ?  $post['phone'] : 0;
			
			$captchaData = $post['captcha'];
			if(!$captcha->isValid($captchaData)){
				$dataOutput['err']['captcha'] = $this->_config['errCaptcha']; 
			}
            // Get the captcha data
            $captchaDataChar = trim($post['captcha']["input"]);
			if($captchaDataChar == ''){
				$dataOutput['err']['captcha'] = $this->_config['emptCaptcha']; 
			}
			$dataOutput['cv']['first_name'] = $first_name;
			$dataOutput['cv']['last_name'] = $last_name;
			$dataOutput['cv']['email'] = $email;
			$dataOutput['cv']['note'] = $note;
			$dataOutput['cv']['birthday'] = $birthday;
			$dataOutput['cv']['gender'] = $gender;
			$dataOutput['cv']['country'] = $country;
			$dataOutput['cv']['phone'] = $phone;
			
			$last_name_check = $this->convertVN($last_name);
			if($last_name == ''){
               $dataOutput['err']['last_name'] = $this->_config['emptyLName']; 
            }elseif(strlen(trim($last_name))< 2 || strlen(trim($last_name)) > 100){
               $dataOutput['err']['last_name'] = $this->_config['errLName'];
            }elseif($last_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $last_name_check)) {
			    $dataOutput['err']['last_name'] = $this->_config['errLName2'];	  
            }
			
			$first_name_check = $this->convertVN($first_name);
			if($first_name == ''){
               $dataOutput['err']['first_name'] = $this->_config['emptyFName']; 
            }elseif(strlen(trim($first_name))<2 || strlen(trim($first_name)) > 100){
               $dataOutput['err']['first_name'] = $this->_config['errFName'];
            }elseif($first_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $first_name_check)) {
			    $dataOutput['err']['first_name'] = $this->_config['errFName2'];	  
            }
			
			if($email == ''){
				$dataOutput['err']['email'] = $this->_config['emptyEmail']; 
            }elseif($email != '' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($email))) {
			    $dataOutput['err']['email'] = $this->_config['errEmail'];	  
            }
			
			if(trim($gender) == ''){
				$dataOutput['err']['gender'] = $this->_config['emptyGender']; 
			}
			
			if($country == 0){
				$dataOutput['err']['country'] = $this->_config['errCountry']; 
			}
			
			if(strlen(trim($note)) > 1000){
				$dataOutput['err']['note'] = $this->_config['errNote']; 
			}
			
			$arrB = explode ('/', $birthday);
			if($birthday == ''){
				$dataOutput['err']['birthday'] = $this->_config['emptyBirthday']; 
            }elseif(strlen(trim($birthday)) > 10 || strlen(trim($birthday)) < 8 || count($arrB) != 3){
				$dataOutput['err']['birthday'] = $this->_config['errBirthday']; 		
			}
			$birthdayIn = trim($arrB['2']).'-'.trim($arrB['1']).'-'.trim($arrB['0']);
			if($phone == 0){
				$dataOutput['err']['phone'] = $this->_config['emptyPhone']; 
            }elseif($phone != 0 && !preg_match("/^[0-9\-\+]{9,14}$/", $phone)){
                $dataOutput['err']['phone'] = $this->_config['errPhone'];                             
            }
			
			if($_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv'] = $this->_config['errCv']; 
			}
			if($_FILES['file']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg');
				$filename = $_FILES['file']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv'] = $this->_config['errCv2']; 
				}
			}
			
			if($_FILES['file']['error'] != 0){
				$dataOutput['err']['cv'] = $this->_config['emptyCv']; 
			}
			
			if($_FILES['file2']['error'] == 0 && $_FILES['file2']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv2'] = $this->_config['errCv3']; 
			}
			if($_FILES['file2']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg', 'RAR', 'rar', 'ZIP', 'zip', '7z', '7Z');
				$filename = $_FILES['file2']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv2'] = $this->_config['errCv4']; 
				}
			}
			
			$dataOutput['captcha'] = $captcha->generate();
			vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
			// clear captcha
			unset($post['captcha']);
			if(!empty($dataOutput['err'])){
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
			}else{
				$content = file_get_contents($_FILES['file']['tmp_name']);
				$dataCv = base64_encode($content);
				$filename = $_FILES['file']['name'];
				
				$content2 = file_get_contents($_FILES['file2']['tmp_name']);
				$dataCv2 = base64_encode($content2);
				$filename2 = $_FILES['file2']['name'];
				
				if($_FILES['file2']['error'] != 0){
					$params = array(
					   'last_name' => $last_name,
					   'first_name' => $first_name,				   
					   'email' => $email,
					   'note' => $note,
					   'birthday' => $birthdayIn,
					   'mobile_phone' => $phone,
					   'country_id' => $country,
					   'gender' => $gender,
					   'resume'=> array(
							'datas' => $dataCv,
							'filename' => $filename
						),					   
					   'post_job_id' => $programId,	
					   'program_type' => $this->_config['code'],				   
					);
				}else{
					$params = array(
					   'last_name' => $last_name,
					   'first_name' => $first_name,				   
					   'email' => $email,
					   'note' => $note,
					   'birthday' => $birthdayIn,
					   'mobile_phone' => $phone,
					   'country_id' => $country,
					   'gender' => $gender,
					   'resume'=> array(
							'datas' => $dataCv,
							'filename' => $filename
						),	
						'compress'=> array(
							'datas' => $dataCv2,
							'filename' => $filename2
						),
					   'post_job_id' => $programId,	
					   'program_type' => $this->_config['code'],				   
					);
				}
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->submitCvOnline($postData); 
				if(isset($result['code']) && $result['code'] == 1){
					$dataOutput['sucss']['process'] = $this->_config['sucssRegister'];
				}else{
					$dataOutput['err']['process'] = $this->_config['errRegister'];
				}
				$dataOutput['captcha'] = $captcha->generate();
				vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
				// clear captcha
				unset($post['captcha']);
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
			}				
        }
		$dataOutput['captcha'] = $captcha->generate();
		vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
		// clear captcha
		unset($dataOutput['cv']['captcha']);
						
		
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
    }
	
	function _showFormRegisterFresher(){
        //----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();
		$dataOutput['err'] = array();
		$dataOutput['sucss'] = array();
		$dataOutput['cv'] = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$dataOutput['captchaurl'] = $this->_config['siteUrl'] . '/captcha/danh-sach.html';
		$tmplastCaptcha = vPortal_Session::get('lastCaptcha');
		$urlCaptcha = PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general/'.$tmplastCaptcha.'.png';
		if(file_exists($urlCaptcha)){
			unlink($urlCaptcha);   
		}

        $captcha=new Zend_Captcha_Image();
        $captcha->setWordLen('6')
                ->setHeight('60')
                ->setFont(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'ariblk.ttf')
                ->setImgDir(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5'); 
				
		$programId = isset($this->_params[1]) ? $this->_params[1] : 0;		
		$params = array(
			'program_event_id' => $programId,
			//$this->_config['code']
		);
		$postData = "";
		$postData = json_encode($params);	
		$school = $this->_model->getListSchool($postData);	
		if(isset($school['code']) && $school['code'] == 1){
			$dataOutput['school'] = $school['data'];
			foreach($dataOutput['school'] as $k => $v){
				$dataOutput['school'][$k] = (array)$v;
			}
		}
		
		foreach($dataOutput['school'] as $k => $v){
			$dataOutput['school'][$k]['name_seo'] = $this->convertTitle($v['name']);
			$dataOutput['school'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
		}
		
		uasort($dataOutput['school'], function($a, $b){
			return strcmp($a["name_seo"], $b["name_seo"]);
		});
		
		$degree = $this->_model->getListDegree();	

		if(isset($degree['code']) && $degree['code'] == 1){
			$dataOutput['degree'] = $degree['data'];
		}else{
			$dataOutput['degree'] = array();
		}
			
		$graduation = $this->_model->getTimeGraduation();	
		if(isset($graduation['code']) && $graduation['code'] == 1){
			$dataOutput['graduation'] = $graduation['data'];
			foreach($dataOutput['graduation'] as $k => $v){
				$dataOutput['graduation'][$k] = (array)$v;
			}
		}
		
		$postData = json_encode($params);		
		$knowfresher = $this->_model->getKnowFresher($postData);	
		if(isset($knowfresher['code']) && $knowfresher['code'] == 1){
			$dataOutput['knowfresher'] = $knowfresher['data'];
			foreach($dataOutput['knowfresher'] as $k => $v){
				$dataOutput['knowfresher'][$k] = (array)$v;
			}
		}
			
		#$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
		$seoName = isset($this->_params[0]) ? $this->_params[0] : '';
		$dataOutput['programId'] = $programId;
		$dataOutput['name-seo'] = $seoName;
		
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $post = Zend_Registry::get('post');
			
			$captchaData = $post['captcha'];
			if(!$captcha->isValid($captchaData)){
				$dataOutput['err']['captcha'] = $this->_config['errCaptcha']; 
			}
            // Get the captcha data
            $captchaDataChar = trim($post['captcha']["input"]);
			if($captchaDataChar == ''){
				$dataOutput['err']['captcha'] = $this->_config['emptCaptcha']; 
			}
            $last_name_check = $this->convertVN($post['member']['last_name']);
            if(empty($post['member']['last_name'])){
               $dataOutput['err']['last_name'] = $this->_config['emptyLName']; 
            }elseif(strlen(trim($post['member']['last_name'])) < 2 || strlen(trim($post['member']['last_name']))> 100){
                $dataOutput['err']['last_name'] = $this->_config['errLName'];
            }elseif($last_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $last_name_check)) {
			    $dataOutput['err']['last_name']= $this->_config['errLName2'];	  
            }
			
			$first_name_check = $this->convertVN($post['member']['first_name']);
            if(empty($post['member']['first_name'])){
               $dataOutput['err']['first_name'] = $this->_config['emptyFName']; 
            }elseif(strlen(trim($post['member']['first_name'])) < 2 || strlen(trim($post['member']['first_name']))> 100){
                $dataOutput['err']['first_name'] = $this->_config['errFName'];
            }elseif($first_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $first_name_check)) {
			    $dataOutput['err']['first_name']= $this->_config['errFName2'];	  
            }
			
			if(empty($post['member']['email'])){
				$dataOutput['err']['email'] = $this->_config['emptyEmail']; 
            }elseif($post['member']['email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['email']))) {
			    $dataOutput['err']['email']= $this->_config['errEmail'];	  
            }
			$birthday = $post['member']['birthday'];
			$arrB = explode ('/', $birthday);
			if($birthday == ''){
				$dataOutput['err']['birthday'] = $this->_config['emptyBirthday']; 
            }elseif(strlen(trim($birthday)) > 10 || strlen(trim($birthday)) < 8 || count($arrB) != 3){
				$dataOutput['err']['birthday'] = $this->_config['errBirthday']; 		
			}
			$birthdayIn = trim($arrB['2']).'-'.trim($arrB['1']).'-'.trim($arrB['0']);
			
			
			if(empty($post['member']['school'])){
               $dataOutput['err']['school'] = $this->_config['emptySchool']; 
            }
			
			if(empty($post['member']['degree'])){
               $dataOutput['err']['degree'] = $this->_config['emptyDegree']; 
            }
			
			if(empty($post['member']['graduation'])){
               $dataOutput['err']['graduation'] = $this->_config['emptyGraduation']; 
            }
			
			if(empty($post['member']['gender'])){
               $dataOutput['err']['gender'] = $this->_config['emptyGender']; 
            }
			
			if(empty($post['member']['knowfresher'])){
               $dataOutput['err']['knowfresher'] = $this->_config['emptyKnowfresher']; 
            }
			
			if(empty($post['member']['speciality'])){
                $dataOutput['err']['speciality'] = $this->_config['emptySpeciality']; 
            }elseif(strlen(trim($post['member']['speciality']))<2 || strlen(trim($post['member']['speciality']))>100){
                $dataOutput['err']['first_name'] = $this->_config['errSpeciality'];
            }
				
			if(empty($post['member']['phone'])){
				$dataOutput['err']['phone'] = $this->_config['emptyPhone']; 
            }elseif(!empty($post['member']['phone']) && !preg_match("/^[0-9\-\+]{9,14}$/", $post['member']['phone'])){
                $dataOutput['err']['phone'] = $this->_config['errPhone'];                             
            }
			
			if(strlen(trim($post['member']['pernament_address'])) > 255){
                $dataOutput['err']['pernament_address'] = $this->_config['errAddress'];
            }
			
			if(!empty($post['member']['identification_no']) 
				&& (strlen(trim($post['member']['identification_no']))<9 
					|| strlen(trim($post['member']['identification_no']))>11
					|| !is_numeric($post['member']['identification_no']))){
                $dataOutput['err']['identification_no'] = $this->_config['errIdentificationNo'];
            }
			
			if(empty($post['member']['cumulative_gpa'])){
				$dataOutput['err']['cumulative_gpa'] = $this->_config['emptyCumulativeGpa']; 
            }elseif(strlen(trim($post['member']['cumulative_gpa']))<1 || strlen(trim($post['member']['cumulative_gpa']))> 4){
                $dataOutput['err']['cumulative_gpa'] = $this->_config['errCumulativeGpa'];
            }elseif(!is_numeric($post['member']['cumulative_gpa']) 
				|| (is_numeric($post['member']['cumulative_gpa']) && intval($post['member']['cumulative_gpa']) > 10)){
				$dataOutput['err']['cumulative_gpa'] = $this->_config['errCumulativeGpa'];
			}

			if($_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv'] = $this->_config['errCv']; 
			}
			if($_FILES['file']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg');
				$filename = $_FILES['file']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv'] = $this->_config['errCv2']; 
				}
			}						
			
			if($_FILES['file']['error'] != 0){
				$dataOutput['err']['cv'] = $this->_config['emptyCv']; 
			}
			
			if($_FILES['file2']['error'] == 0 && $_FILES['file2']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv2'] = $this->_config['errCv3']; 
			}
			if($_FILES['file2']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg', 'RAR', 'rar', 'ZIP', 'zip', '7z', '7Z');
				$filename = $_FILES['file2']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv2'] = $this->_config['errCv4']; 
				}
			}
			
			$dataOutput['member']['first_name'] = $post['member']['first_name'];
			$dataOutput['member']['last_name'] = $post['member']['last_name'];
			$dataOutput['member']['email'] = $post['member']['email'];
			$dataOutput['member']['identification_no'] = $post['member']['identification_no'];
			$dataOutput['member']['birthday'] = $post['member']['birthday'];
			$dataOutput['member']['speciality'] = $post['member']['speciality'];
			$dataOutput['member']['graduation'] = $post['member']['graduation'];
			$dataOutput['member']['phone'] = $post['member']['phone'];
			$dataOutput['member']['cumulative_gpa'] = $post['member']['cumulative_gpa'];
			$dataOutput['member']['knowfresher'] = $post['member']['knowfresher'];
			$dataOutput['member']['school'] = $post['member']['school'];
			$dataOutput['member']['degree'] = $post['member']['degree'];
			$dataOutput['member']['pernament_address'] = $post['member']['pernament_address'];
			$dataOutput['member']['gender'] = $post['member']['gender'];
			
			$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
			if($programId == 0){
				$dataOutput['err']['err'] = $this->_config['errProcess'];
			}
            $dataOutput['captcha'] = $captcha->generate();
			vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
			// clear captcha
			unset($post['captcha']);
			if(!empty($dataOutput['err'])){

				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
			}else{		
				$content = file_get_contents($_FILES['file']['tmp_name']);
				$dataCv = base64_encode($content);
				$filename = $_FILES['file']['name'];
				
				$content2 = file_get_contents($_FILES['file2']['tmp_name']);
				$dataCv2 = base64_encode($content2);
				$filename2 = $_FILES['file2']['name'];
				
				if($_FILES['file2']['error'] != 0){
					$params = array(
					   'last_name' => $post['member']['last_name'],
					   'first_name' => $post['member']['first_name'],
					   'birthday' => $birthdayIn,
					   'gender' => $post['member']['gender'],
					   'mobile_phone' => $post['member']['phone'],
					   'school_id' => $post['member']['school'],
					   'recruitment_degree_id' => $post['member']['degree'],
					   'email' => $post['member']['email'],
					   'speciality' => $post['member']['speciality'],
					   'identification_no' => $post['member']['identification_no'],
					   'pernament_address' => $post['member']['pernament_address'],
					   'cumulative_gpa' => $post['member']['cumulative_gpa'],
					   'recruitment_source_id' => $post['member']['knowfresher'],			   
					   'time_graduation_id' => $post['member']['graduation'],
					   'program_event_id' => $programId,	
					   'program_type' => $this->_config['code'],
					   'resume'=> array(
							'datas' => $dataCv,
							'filename' => $filename
						),	
						
					);
				}else{
					$params = array(
					   'last_name' => $post['member']['last_name'],
					   'first_name' => $post['member']['first_name'],
					   'birthday' => $birthdayIn,
					   'gender' => $post['member']['gender'],
					   'mobile_phone' => $post['member']['phone'],
					   'school_id' => $post['member']['school'],
					   'recruitment_degree_id' => $post['member']['degree'],
					   'email' => $post['member']['email'],
					   'speciality' => $post['member']['speciality'],
					   'identification_no' => $post['member']['identification_no'],
					   'pernament_address' => $post['member']['pernament_address'],
					   'cumulative_gpa' => $post['member']['cumulative_gpa'],
					   'recruitment_source_id' => $post['member']['knowfresher'],			   
					   'time_graduation_id' => $post['member']['graduation'],
					   'program_event_id' => $programId,	
					   'program_type' => $this->_config['code'],
					   'resume'=> array(
							'datas' => $dataCv,
							'filename' => $filename
						),
						'compress'=> array(
							'datas' => $dataCv2,
							'filename' => $filename2
						),						
					);
				}
 				
				$postData = '';
				$postData = json_encode($params);
				if(isset($this->_config['code']) && $this->_config['code'] == 'INTERNSHIP'){
					$result = $this->_model->registerInternship($postData); 
				}else{
					$result = $this->_model->registerFresher($postData); 
				}
				
				if(isset($result['code']) && $result['code'] == 1){
					$dataOutput['sucss']['process'] = $this->_config['sucssRegister'];
				}else{
					$dataOutput['err']['process'] = $this->_config['errRegister'];
				}
				$dataOutput['captcha'] = $captcha->generate();
				vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
				// clear captcha
				unset($post['captcha']);
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
				
            }
        }
		
		$dataOutput['captcha'] = $captcha->generate();
		vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
		// clear captcha
		unset($dataOutput['cv']['captcha']);
		
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );
        
        
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
    }
   	
	function _showFormRegisterInternship(){
	
		//----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();
		$dataOutput['err'] = array();
		$dataOutput['sucss'] = array();
		$dataOutput['cv'] = array();
		
		$dataOutput['captchaurl'] = $this->_config['siteUrl'] . '/captcha/danh-sach.html';
		$tmplastCaptcha = vPortal_Session::get('lastCaptcha');
		$urlCaptcha = PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general/'.$tmplastCaptcha.'.png';
		if(file_exists($urlCaptcha)){
			unlink($urlCaptcha);   
		}

        $captcha=new Zend_Captcha_Image();
        $captcha->setWordLen('6')
                ->setHeight('60')
                ->setFont(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'ariblk.ttf')
                ->setImgDir(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5'); 
				
        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
		$seoName = isset($this->_params[0]) ? $this->_params[0] : '';
		$dataOutput['programId'] = $programId;
		$dataOutput['name-seo'] = $seoName;
		
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$post = Zend_Registry::get('post');
			$programId = isset($this->_params[1]) ? $this->_params[1] : 0;
			if($programId == 0){
				$dataOutput['err']['process'] = $this->_config['errProcess'];
			}
			$last_name = isset($post['last_name'])? $post['last_name']: '';
			$first_name = isset($post['first_name'])? $post['first_name']: '';
			$email = isset($post['email']) ?  $post['email'] : '';
			$note = isset($post['note']) ?  trim($post['note']) : '';
			$phone = isset($post['phone']) ?  $post['phone'] : 0;
			$schoolId = isset($post['school']) ?  $post['school'] : 0;
			$degreeId = isset($post['degree']) ?  $post['degree'] : 0;
			$speciality = isset($post['speciality']) ?  $post['speciality'] : '';
			$jobId = isset($post['job']) ?  $post['job'] : 0;
			
			$captchaData = $post['captcha'];
			if(!$captcha->isValid($captchaData)){
				$dataOutput['err']['captcha'] = $this->_config['errCaptcha']; 
			}
            // Get the captcha data
            $captchaDataChar = trim($post['captcha']["input"]);
			if($captchaDataChar == ''){
				$dataOutput['err']['captcha'] = $this->_config['emptCaptcha']; 
			}
			
			$dataOutput['cv']['last_name'] = $last_name;
			$dataOutput['cv']['first_name'] = $first_name;
			$dataOutput['cv']['email'] = $email;
			$dataOutput['cv']['phone'] = $phone;
			$dataOutput['cv']['note'] = $note;
			$dataOutput['cv']['school'] = $schoolId;
			$dataOutput['cv']['degree'] = $degreeId;
			$dataOutput['cv']['speciality'] = $speciality;
			$dataOutput['cv']['job'] = $jobId;
						
			$last_name_check = $this->convertVN($last_name);
			if(trim($last_name) == ''){
               $dataOutput['err']['last_name'] = $this->_config['emptyLName']; 
            }elseif(strlen(trim($last_name))< 2 || strlen(trim($last_name)) > 100){
               $dataOutput['err']['last_name'] = $this->_config['errLName'];
            }elseif(trim($last_name_check) != '' && !preg_match("/[a-zA-Z0-9 ]$/", $last_name_check)) {
			    $dataOutput['err']['last_name'] = $this->_config['errLName2'];	  
            }
			
			$first_name_check = $this->convertVN($first_name);
			if(trim($first_name) == ''){
               $dataOutput['err']['first_name'] = $this->_config['emptyFName']; 
            }elseif(strlen(trim($first_name))<2 || strlen(trim($first_name)) > 100){
               $dataOutput['err']['first_name'] = $this->_config['errFName'];
            }elseif(trim($first_name_check) != '' && !preg_match("/[a-zA-Z0-9 ]$/", $first_name_check)) {
			    $dataOutput['err']['first_name'] = $this->_config['errFName2'];	  
            }
			
			if($email == ''){
				$dataOutput['err']['email'] = $this->_config['emptyEmail']; 
            }elseif($email != '' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($email))) {
			    $dataOutput['err']['email'] = $this->_config['errEmail'];	  
            }
			
			if(trim($note) == ''){
				$dataOutput['err']['note'] = $this->_config['emptyNote']; 
			}elseif(strlen(trim($note)) > 1000){
				$dataOutput['err']['note'] = $this->_config['errNote']; 
			}
			
			if(trim($speciality) == ''){
				$dataOutput['err']['speciality'] = $this->_config['emptySpeciality']; 
			}elseif(strlen(trim($speciality)) > 100){
				$dataOutput['err']['speciality'] = $this->_config['errSpeciality']; 
			}
			
			if($schoolId == 0){
               $dataOutput['err']['school'] = $this->_config['emptySchool']; 
            }
			
			if($degreeId == 0){
               $dataOutput['err']['degree'] = $this->_config['emptyDegree']; 
            }
			
			if($jobId == 0){
               $dataOutput['err']['job'] = $this->_config['emptyJob']; 
            }
			
			if($phone == 0){
				$dataOutput['err']['phone'] = $this->_config['emptyPhone']; 
            }elseif($phone != 0 && !preg_match("/^[0-9\-\+]{9,14}$/", $phone)){
                $dataOutput['err']['phone'] = $this->_config['errPhone'];                             
            }
			
			if($_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 3145728){ // 3MB
				$dataOutput['err']['cv'] = $this->_config['errCv']; 
			}
			if($_FILES['file']['error'] == 0){
				$allowed =  array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX', 'PPT', 'PPTX', 'JPG', 'jpg', 'RAR', 'rar', 'ZIP', 'zip');
				$filename = $_FILES['file']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) {
					$dataOutput['err']['cv'] = $this->_config['errCv2']; 
				}
			}
			
			if($_FILES['file']['error'] != 0){
				$dataOutput['err']['cv'] = $this->_config['emptyCv']; 
			}
			$school = $this->_model->getListSchool();	
			if(isset($school['code']) && $school['code'] == 1){
				$dataOutput['school'] = $school['data'];
				foreach($dataOutput['school'] as $k => $v){
					$dataOutput['school'][$k] = (array)$v;
				}
			}
			foreach($dataOutput['school'] as $k => $v){
				$dataOutput['school'][$k]['name_seo'] = $this->convertTitle($v['name']);
				$dataOutput['school'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
			}
			
			uasort($dataOutput['school'], function($a, $b){
				return strcmp($a["name_seo"], $b["name_seo"]);
			});	
			
			$params = array(
				'program_type' => $this->_config['code']
			);
			$postData = '';
			$postData = json_encode($params);
			$result = $this->_model->getListProgramByCode($postData);
			$dataIntern = array();
			if(isset($result['code']) && $result['code'] == 1){
				$dataIntern = $result['data'];
				foreach($dataIntern as $k => $v){
					$dataIntern[$k] = (array)$v;
				}
			}
			
			foreach($dataIntern as $kS => $vS){
				if($vS['id'] == $programId && $vS['has_job'] == true){
					$position = (array)$vS['position_ids'];
					break;
				}
			}
			
			if(count($position) > 0){
				foreach($position as $k => $v){
					$dataOutput['job'][$k] = (array)$v;
				}
			}
			
			$dataOutput['captcha'] = $captcha->generate();
			vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
			// clear captcha
			unset($post['captcha']);
			
			if(!empty($dataOutput['err'])){
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
			}else{
				$content = file_get_contents($_FILES['file']['tmp_name']);
				$dataCv = base64_encode($content);
				$filename = $_FILES['file']['name'];
				$params = array(
				   'last_name' => $last_name,
				   'first_name' => $first_name,				   
				   'email' => $email,
				   'note' => $note,
				   'mobile_phone' => $phone,
				   'school_id' => $schoolId,
				   'speciality' => $speciality,
				   'resume'=> array(
						'datas' => $dataCv,
						'filename' => $filename
					),
				   'job_id' => $jobId,	
				   'program_event_id' => $programId,
				   'program_type' => $this->_config['code'],				   
				);
				
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->registerInternship($postData); 

				if(isset($result['code']) && $result['code'] == 1){
					$dataOutput['sucss']['process'] = $this->_config['sucssRegister'];
				}else{
					$dataOutput['err']['process'] = $this->_config['errRegister'];
				}
				
				$school = $this->_model->getListSchool();	
				if(isset($school['code']) && $school['code'] == 1){
					$dataOutput['school'] = $school['data'];
					foreach($dataOutput['school'] as $k => $v){
						$dataOutput['school'][$k] = (array)$v;
					}
				}
				foreach($dataOutput['school'] as $k => $v){
					$dataOutput['school'][$k]['name_seo'] = $this->convertTitle($v['name']);
					$dataOutput['school'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
				}
				
				uasort($dataOutput['school'], function($a, $b){
					return strcmp($a["name_seo"], $b["name_seo"]);
				});
				$params = array(
					'program_type' => $this->_config['code']
				);
				$postData = '';
				$postData = json_encode($params);
				$result = $this->_model->getListProgramByCode($postData);
				$dataIntern = array();
				if(isset($result['code']) && $result['code'] == 1){
					$dataIntern = $result['data'];
					foreach($dataIntern as $k => $v){
						$dataIntern[$k] = (array)$v;
					}
				}
				
				foreach($dataIntern as $kS => $vS){
					if($vS['id'] == $programId && $vS['has_job'] == true){
						$position = (array)$vS['position_ids'];
						break;
					}
				}
				
				if(count($position) > 0){
					foreach($position as $k => $v){
						$dataOutput['job'][$k] = (array)$v;
					}
				}	
				$dataOutput['captcha'] = $captcha->generate();
				vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
				// clear captcha
				unset($post['captcha']);
				
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
			}				
        }
						
		$school = $this->_model->getListSchool();	
		if(isset($school['code']) && $school['code'] == 1){
			$dataOutput['school'] = $school['data'];
			foreach($dataOutput['school'] as $k => $v){
				$dataOutput['school'][$k] = (array)$v;
			}
		}
		foreach($dataOutput['school'] as $k => $v){
			$dataOutput['school'][$k]['name_seo'] = $this->convertTitle($v['name']);
			$dataOutput['school'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
		}
		
		uasort($dataOutput['school'], function($a, $b){
			return strcmp($a["name_seo"], $b["name_seo"]);
		});
		
		$params = array(
			'program_type' => $this->_config['code']
		);
		$postData = '';
		$postData = json_encode($params);
		$result = $this->_model->getListProgramByCode($postData);
		$dataIntern = array();
		if(isset($result['code']) && $result['code'] == 1){
			$dataIntern = $result['data'];
			foreach($dataIntern as $k => $v){
				$dataIntern[$k] = (array)$v;
			}
		}
		
		foreach($dataIntern as $kS => $vS){
			if($vS['id'] == $programId && $vS['has_job'] == true){
				$position = (array)$vS['position_ids'];
				break;
			}
		}
		
		if(count($position) > 0){
			foreach($position as $k => $v){
				$dataOutput['job'][$k] = (array)$v;
			}
		}
		
		$dataOutput['captcha'] = $captcha->generate();
		vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
		// clear captcha
		unset($dataOutput['cv']['captcha']);
		
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
	}
	
	function _showFormShareJob(){
		//----------------------------------------------
        // init value-returned
        //----------------------------------------------
        $output = '';
        $dataOutput = array();
		$dataOutput['err'] = array();
		$dataOutput['sucss'] = array();
		$dataOutput['cv'] = array();
		$dataSend = array();
		
        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		$dataOutput['captchaurl'] = $this->_config['siteUrl'] . '/captcha/danh-sach.html';
		$tmplastCaptcha = vPortal_Session::get('lastCaptcha');
		$urlCaptcha = PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general/'.$tmplastCaptcha.'.png';
		if(file_exists($urlCaptcha)){
			unlink($urlCaptcha);   
		}

        $captcha=new Zend_Captcha_Image();
        $captcha->setWordLen('6')
                ->setHeight('60')
                ->setFont(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'ariblk.ttf')
                ->setImgDir(PUBLIC_HTML_PATH . DS . 'captcha' . DS . 'general')
                ->setDotNoiseLevel('5')
                ->setLineNoiseLevel('5'); 
				 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $msg = array();
            $post = Zend_Registry::get('post');
			
			$captchaData = $post['captcha'];
			if(!$captcha->isValid($captchaData)){
				$dataOutput['err']['captcha'] = $this->_config['errCaptcha']; 
			}
            // Get the captcha data
            $captchaDataChar = trim($post['captcha']["input"]);
			if($captchaDataChar == ''){
				$dataOutput['err']['captcha'] = $this->_config['emptCaptcha']; 
			}
			
			$from_name_check = $this->convertVN($post['member']['from_name']);
            if(empty($post['member']['from_name'])){
               $dataOutput['err']['from_name'] = $this->_config['emptyFName']; 
            }elseif(strlen(trim($post['member']['from_name'])) < 2 || strlen(trim($post['member']['from_name']))> 100){
                $dataOutput['err']['from_name'] = $this->_config['errFName'];
            }elseif($from_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $from_name_check)) {
			    $dataOutput['err']['from_name']= $this->_config['errFName2'];	  
            }
			
			$to_name_check = $this->convertVN($post['member']['to_name']);
			if(empty($post['member']['to_name'])){
                $dataOutput['err']['to_name'] = $this->_config['emptyTName']; 
            }elseif(strlen(trim($post['member']['to_name'])) < 2 || strlen(trim($post['member']['to_name'])) > 100){
                $dataOutput['err']['to_name'] = $this->_config['errTName'];
            }elseif($to_name_check != '' && !preg_match("/[a-zA-Z0-9 ]$/", $to_name_check)) {
			    $dataOutput['err']['to_name']= $this->_config['errTName2'];	  
            }
			
			if(empty($post['member']['from_email'])){
				$dataOutput['err']['from_email'] = $this->_config['emptyFEmail']; 
            }elseif($post['member']['from_email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['from_email']))) {
			    $dataOutput['err']['from_email']= $this->_config['errFEmail'];	  
            }
			
			if(empty($post['member']['to_email'])){
				$dataOutput['err']['to_email'] = $this->_config['emptyTEmail']; 
            }elseif($post['member']['to_email']!='' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", trim($post['member']['to_email']))) {
			    $dataOutput['err']['to_email']= $this->_config['errTEmail'];	  
            }
			
			if(empty($post['member']['note'])){
				$dataOutput['err']['note'] = $this->_config['emptyNote']; 
            }elseif(strlen(trim($post['member']['note'])) > 1000 ){
                $dataOutput['err']['note'] = $this->_config['errNote'];
            }
			
			$dataOutput['member']['from_name'] = $post['member']['from_name'];
			$dataOutput['member']['to_name'] = $post['member']['to_name'];
			$dataOutput['member']['from_email'] = $post['member']['from_email'];
			$dataOutput['member']['to_email'] = $post['member']['to_email'];
			$dataOutput['member']['note'] = $post['member']['note'];
			$dataOutput['member']['linkSend'] = $post['linkSend'];
			
            if(!empty($dataOutput['err'])){
				$dataOutput['captcha'] = $captcha->generate();
				vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
				// clear captcha
				unset($dataOutput['member']['captcha']);
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;
            }else{	
				$content = array();
				if(isset($this->_config['actionDetail']) &&  $this->_config['actionDetail'] == 'recruitment' ){
					$urlArr = parse_url($post['linkSend']);
					$urlPath = explode('/', $urlArr['path']);
					$strJob =  str_replace('.html', '', $urlPath[count($urlPath)-1]);
					$jobIdArr = explode('.', $strJob);
					$jobId = $jobIdArr[count($jobIdArr)-1];
					
					$result = $this->_model->getRequestJob();
			
					if(isset($result['code']) && $result['code'] == 1){
						$data = $result['data'];
					}
					
					foreach($data as $k => $v){
						if($v['id'] == $jobId){
							$content = $v;
							break;
						}
					}
					//var_dump($content);die;
				}
				
				$dataSend = array(
					'from_name' => $post['member']['from_name'],
					'from_email' => $post['member']['from_email'],
					'to_name' => $post['member']['to_name'],
					'to_email' => $post['member']['to_email'],
					'note' => $post['member']['note'],
					'link' => $post['linkSend'],
					'job' => $content,					
				);
				$result = $this->_sendMail($dataSend); 
				
				if($result){
					$dataOutput['sucss']['process'] = $this->_config['sucssSendMail'];
				}else{
					$dataOutput['err']['process'] = $this->_config['errSendMail'];
				}	
				$dataOutput['captcha'] = $captcha->generate();
				vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
				// clear captcha
				unset($dataOutput['member']['captcha']);	
				$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );             
				//----------------------------------------------
				// return html string
				//----------------------------------------------        
				return $output;	
            }

        }
		$dataOutput['captcha'] = $captcha->generate();
		vPortal_Session::set('lastCaptcha', $dataOutput['captcha']);
		// clear captcha
		unset($dataOutput['member']['captcha']);
		
        $output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateForm'],
                                                 $dataOutput, $this->_config );
        
        
        //----------------------------------------------
        // return html string
        //----------------------------------------------        
        return $output;
	}
	
	function _showJobList(){
		$output = '';
        $dataOutput = array();
		$data = array();
        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$action = isset($this->_params[0]) ? trim($this->_params[0]) : '';
		$result = $this->_model->getRequestJob();
		
		if(isset($result['code']) && $result['code'] == 1){
			$data = $result['data'];
		}
		//$data = array_reverse($data);
		//$dataOutput['lastDate'] = strtotime($data[0]['date_start']);
		if($action == 'chi-tiet' || $action == 'detail'){
			$jobId = isset($this->_params[5]) && is_numeric($this->_params[5]) ? $this->_params[5] : 0;
			foreach($data as $k => $v){
				if($v['id'] == $jobId){
					$dataOutput['content'] = $v;
					break;
				}
			}

			$dataOutput['content']['preference'] = str_replace('font-size:','',$dataOutput['content']['preference']);
			$dataOutput['content']['preference'] = str_replace('font-family:','',$dataOutput['content']['preference']);
			$dataOutput['content']['preference'] = str_replace('list-style-type:','',$dataOutput['content']['preference']);
			$dataOutput['content']['preference'] = str_replace('color:','',$dataOutput['content']['preference']);
			$dataOutput['content']['preference'] = str_replace('text-align:','',$dataOutput['content']['preference']);
			
			$dataOutput['content']['requirement'] = str_replace('font-size:','',$dataOutput['content']['requirement']);
			$dataOutput['content']['requirement'] = str_replace('font-family:','',$dataOutput['content']['requirement']);
			$dataOutput['content']['requirement'] = str_replace('list-style-type:','',$dataOutput['content']['requirement']);
			$dataOutput['content']['requirement'] = str_replace('color:','',$dataOutput['content']['requirement']);
			$dataOutput['content']['requirement'] = str_replace('text-align:','',$dataOutput['content']['requirement']);
			
			$dataOutput['content']['description'] = str_replace('font-size:','',$dataOutput['content']['description']);
			$dataOutput['content']['description'] = str_replace('font-family:','',$dataOutput['content']['description']);
			$dataOutput['content']['description'] = str_replace('list-style-type:','',$dataOutput['content']['description']);
			$dataOutput['content']['description'] = str_replace('color:','',$dataOutput['content']['description']);
			$dataOutput['content']['description'] = str_replace('text-align:','',$dataOutput['content']['description']);
			
			$dataOutput['content']['name_seo'] = $this->convertTitle($dataOutput['content']['name']);	
			$dataOutput['content']['cate_seo'] = $this->convertTitle($dataOutput['content']['vhr_job_id']['name']);	
			$dataOutput['content']['cate_en_seo'] = $this->convertTitle($dataOutput['content']['vhr_job_id']['name_en']);	
			$dataOutput['content']['place_seo'] = $this->convertTitle($dataOutput['content']['city_id'][1]);
			
			$output = vPortal_Utility::loadView( $this->_config['featureCode'],
													 $this->_config['templateDetail'],
													 $dataOutput, $this->_config );
													 
		}else{		
			$searchCate = isset($this->_params[1]) && is_numeric($this->_params[1]) ? $this->_params[1] : 0;
			$arrJobtmp = array();
			$place	= 0;	
			$cate = 0;
			$keyword = '';
			
			if($_SERVER['REQUEST_METHOD'] == 'POST'){			
				$post = Zend_Registry::get('post');
				$cate = isset($post['cate']) ? intval($post['cate']) : 0;	
				$place = isset($post['place']) ? intval($post['place']) : 0;
				$keyword = isset($post['keyword']) ? $post['keyword'] : '';			
			}
			
			if($searchCate != 0 || $cate != 0){
				foreach($data as $k => $v){
					if($v['vhr_job_id']['id'] == $searchCate || $v['vhr_job_id']['id'] == $cate){
						$dataOutput['list'][] = $v;
					}
				}
			}else{		
				$dataOutput['list'] = $data;
			}
			
			if($place != 0){
				foreach($dataOutput['list'] as $k => $v){
					if($v['city_id'][0] == $place){
						$arrJobtmp[] = $v;
					}
				}
				$dataOutput['list'] = $arrJobtmp;
				$arrJobtmp = array();
			}
			
			if(trim($keyword) != ''){
				foreach($dataOutput['list'] as $k => $v){
					$mystrVn = strtolower($this->convertVN($v['name']));
					$mystrEn = strtolower($this->convertVN($v['name_en']));
					$mystrCode = strtolower($this->convertVN($v['code']));
					$findme = strtolower(trim($this->convertVN($keyword)));
					$posVn = strpos($mystrVn, $findme);
					$posEn = strpos($mystrEn, $findme);
					$posCode = strpos($mystrCode, $findme);
					if ($posVn !== false || $posEn !== false || $posCode !== false) {
						$arrJobtmp[] = $v;
					}
				}
				$dataOutput['list'] = $arrJobtmp;
				$arrJobtmp = array();
			}

			
			foreach($dataOutput['list'] as $k => $v){
				$dataOutput['list'][$k]['name_seo'] = $this->convertTitle($v['name']);	
				$dataOutput['list'][$k]['cate_seo'] = $this->convertTitle($v['vhr_job_id']['name']);	
				$dataOutput['list'][$k]['cate_en_seo'] = $this->convertTitle($v['vhr_job_id']['name_en']);	
				$dataOutput['list'][$k]['place_seo'] = $this->convertTitle($v['city_id'][1]);
				$dataOutput['list'][$k]['date_start_int'] = strtotime($v['date_start']);
			}
			//$dataOutput['list'] = array_reverse($dataOutput['list']);
			$dataOutput['timeDisplay'] = 14*86400;
			$dataOutput['currentTime'] = time();
			
			$dataReturn = array();
			if(isset($this->_config['isEnglish']) && $this->_config['isEnglish'] == 1 ){
				foreach($dataOutput['list'] as $k => $v){
					if($v['description_en'] != false && $v['requirement_en'] != false ){
						$dataReturn[] = $v;
					}
				}
				
				$dataOutput['list'] = $dataReturn;
			}
			
			$clientIp = Zend_Registry::get('clientIp');		
			if($clientIp == '10.74.25.1044'){
				echo "<pre>";
				var_dump($dataOutput);
				echo "</pre>";
				exit;
			}
			
			$output = vPortal_Utility::loadView( $this->_config['featureCode'],
													 $this->_config['templateList'],
													 $dataOutput, $this->_config );
		}
		
		return $output;
	}
	
	function convertTitle($str,$sSplit="-"){
        $str = strip_tags($str);
        $pattern = array("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/",//Unicode - Unicode to hop
                        "/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/",
                        "/(ì|í|ị|ỉ|ĩ|ì|í|ị|ỉ|ĩ|I|Ì|Í|Ị|Ỉ|Ĩ|I|Ì|Í|Ị|Ỉ|Ĩ)/",
                        "/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/",
                        "/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/",
                        "/(ỳ|ý|ỵ|ỷ|ỹ|ỳ|ý|ỵ|ỷ|ỹ|Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ|Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ)/",
                        "/(đ|đ|D|Đ|Đ)/"
        );
        $replace            = array('a','e','i','o','u','y','d');
        $aUperPattern       = array("/B/","/C/","/F/","/G/","/H/","/J/","/K/","/L/","/M/","/N/","/P/","/Q/","/R/","/S/","/T/","/V/","/W/","/X/","/Z/");
        $aToLowerPattern    = array("b","c","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","z");
        $str = preg_replace($pattern, $replace, $str);

        $str = strtolower($str);
        $str = preg_replace('/[^a-z0-9]/', ' ', $str);
        $str = trim($str);
        $str = preg_replace("/\s{1,}/", ' ', $str);
        if(strlen($sSplit)>0)
            $str = preg_replace("/( )/", $sSplit, $str);
        return $str;
	}
	
	function convertVN($str,$sSplit="-"){
        $str = strip_tags($str);
        $pattern = array("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/",//Unicode - Unicode to hop
                        "/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/",
                        "/(ì|í|ị|ỉ|ĩ|ì|í|ị|ỉ|ĩ|I|Ì|Í|Ị|Ỉ|Ĩ|I|Ì|Í|Ị|Ỉ|Ĩ)/",
                        "/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/",
                        "/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/",
                        "/(ỳ|ý|ỵ|ỷ|ỹ|ỳ|ý|ỵ|ỷ|ỹ|Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ|Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ)/",
                        "/(đ|đ|D|Đ|Đ)/"
        );
        $replace            = array('a','e','i','o','u','y','d');
        $aUperPattern       = array("/B/","/C/","/F/","/G/","/H/","/J/","/K/","/L/","/M/","/N/","/P/","/Q/","/R/","/S/","/T/","/V/","/W/","/X/","/Z/");
        $aToLowerPattern    = array("b","c","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","z");
        $str = preg_replace($pattern, $replace, $str);
		
		return $str;
	}
	
	function _sendMail($dataSend){
		$data = array();
		if($dataSend['to_email'] == ""){
			return false;
		}
		$aEmail[0] = $dataSend['to_email'];		
		$sFromName = $dataSend['from_name'];
		$sFromEmail = $dataSend['from_email'];
		
		$data['content'] = $dataSend['note'];
		$data['fromName'] = $dataSend['from_name'];
		$data['toName'] = $dataSend['to_name'];
		$data['link'] = $dataSend['link'];
		$data['job'] = $dataSend['job'];
		
		$posToMail = strpos($aEmail[0], 'yahoo.com.vn');
		if ($posToMail !== false ) {
			$aEmail[0] = str_replace('yahoo.com.vn','yahoo.com',$aEmail[0]);
		}
		
		$posFromMail = strpos($sFromEmail, 'yahoo.com.vn');
		if ($posFromMail !== false ) {
			$sFromEmail = str_replace('yahoo.com.vn','yahoo.com',$sFromEmail);
		}
		
		if(count($aEmail)>0){
			
			$sSubject = $this->_config['emailSubject'];			

			$sContent = vPortal_Utility::loadView($this->_config['featureCode'],
                                                    $this->_config['templateEmail'],
                                                    $data, $this->_config);						
			
			if ($this->_sendMultiEmailSTML($sSubject,$sContent,$sFromEmail, $sFromName, $aEmail,1)) {			
				return true;
			}
		}
		return false;
    }
	
	function _sendMailVerify($dataSend){
		$data = array();
		if($dataSend['to_email'] == ""){
			return false;
		}
		$aEmail[0] = $dataSend['to_email'];		
		$sFromName = $dataSend['from_name'];
		$sFromEmail = $dataSend['from_email'];
		
		$data['fromName'] = $dataSend['from_name'];
		$data['toName'] = $dataSend['to_name'];
		$data['linkActive'] = $dataSend['linkActive'];
		$data['timeExp'] = $dataSend['timeExp'];
		$data['strPr'] = $dataSend['strPr'];
		
		$posToMail = strpos($aEmail[0], 'yahoo.com.vn');
		if ($posToMail !== false ) {
			$aEmail[0] = str_replace('yahoo.com.vn','yahoo.com',$aEmail[0]);
		}
		
		$posFromMail = strpos($sFromEmail, 'yahoo.com.vn');
		if ($posFromMail !== false ) {
			$sFromEmail = str_replace('yahoo.com.vn','yahoo.com',$sFromEmail);
		}
		
		if(count($aEmail)>0){
			
			$sSubject = $this->_config['emailSubject'];			

			$sContent = vPortal_Utility::loadView($this->_config['featureCode'],
                                                    $this->_config['templateEmail'],
                                                    $data, $this->_config);						
			
			if ($this->_sendMultiEmailSTML($sSubject,$sContent,$sFromEmail, $sFromName, $aEmail,1)) {			
				return true;
			}
		}
		return false;
    }
	
	private function _sendMultiEmailSTML( $sSubject, $sBody, $sFrom, $sFromName, $aTo, $bIsHTML ){	
		$mail = new PHPMailer;		
		try {
			$mail->IsSMTP();                                // Set mailer to use SMTP
			$mail->CharSet="UTF-8";
			$mail->Host = '10.30.21.10';  				// Specify main and backup server
			$mail->Port       = 25;						//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
			//$mail->SMTPSecure = 'tls';                      // Enable encryption, 'ssl' also accepted
			$mail->SMTPAuth   = false;
			//$mail->Username = $this->_config['smtpUser'];   // SMTP username
			//$mail->Password = $this->_config['smtpPass'];   // SMTP password
			$mail->From = $sFrom;
			$mail->FromName = $sFromName;
		
			foreach ($aTo as $value) {
				$mail->AddAddress($value);  // Add a recipient
			}
	
			$mail->WordWrap = 100;                                 // Set word wrap to 50 characters
			$mail->IsHTML($bIsHTML);                                  // Set email format to HTML		
			
			$mail->Subject = $sSubject;
			$mail->Body    = $sBody;
			$mail->AltBody = $sBody;
					
			if(!$mail->Send()) {
			    return "Error: " . $mail->ErrorInfo;
			}else {
				return true;
			}			
		}
		catch (Exception $e) {
			$sError =  "Error: " . $e;
			return false;
		}
	}
	
	function encodeLinkVerify($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		for ($i = 0; $i < $strLen; $i++) {
			$ordStr = ord(substr($string,$i,1));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
		}
		return $hash;
	}

	function decodeLinkVerify($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		for ($i = 0; $i < $strLen; $i+=2) {
			$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= chr($ordStr - $ordKey);
		}
		return $hash;
	}
    /**
    * function: output
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function output()
    {
        return $this->_output;
    }

   /**
    * function: _loadModel
    * description: get news model (if null)
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    private function _loadModel()
    {
        if( !$this->_model )
        {
            $this->_model = vPortal_Utility::loadModel( $this->_config['featureCode'],
                                                        'Application_Feature_Recruitment_Model_Recruitment' );
        }
    }
    
    
}