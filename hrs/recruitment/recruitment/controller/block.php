<?php
class Application_Feature_Recruitment_Controller_Block extends Application_Feature_Feature
{
    
   /**
    * @access  private
    * @name $_model
    * @var Object
    */
    private $_model;
         
   /**
    * @access  private
    * @name $_isAjax
    * @var Object
    */
    private $_isAjax;

   /**
    * function: __construct
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function __construct( )
    {
        //----------------------------------------------
        // get ajax status
        //----------------------------------------------
        $this->_isAjax = Zend_Registry::get('isAjax');
    }

   /**
    * function: execute
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2013 VNG
    */
    public function execute( )
    {
		$this->_config['siteUrl'] = vPortal_Utility::getConfig( 'application', 'siteUrl' );
        
        //----------------------------------------------
        // get common info and store in config
        //----------------------------------------------
        $this->_config['sectionPath'] = Zend_Registry::get( 'sectionPath' );                     
		
		//----------------------------------------------
		// check input
		//----------------------------------------------
		if( empty( $this->_config ) )
		{			
			//----------------------------------------------
			// define error message
			//----------------------------------------------
			$errorMsg = 'There has a problem in getting configuration of ' .
						__CLASS__ . '. Maybe its caused by I/O of harddisk ' .
						'or has an error in generating this configuration file. ' .
						'Please check config again.';

			//----------------------------------------------
			// process error
			//----------------------------------------------
			vPortal_Error::userError( 'feature', $this->_config['featureCode'],
									  vPortalX_Error_Code::VPORTAL_MODULE_CONFIG_ERROR,
									  $errorMsg );
		}


		//----------------------------------------------
		// get common info
		//----------------------------------------------
		$this->_config['isAjax'] = $this->_isAjax;

		//----------------------------------------------
		// balancing
		//----------------------------------------------
				
		switch( $this->_config['action'] )
		{
			case 'tour':
			case 'fresher':
			case 'internship':
				$this->_output = $this->_showProgramListByCode();
				break;
			case 'face-fresher':
				$this->_output = $this->_showListFaceFresher();                                
				break;
			case 'jobcate':
				$this->_output = $this->_showListJobCate();                                
				break;
			case 'jobsearch':
				$this->_output = $this->_showFormSearchJob();                                
				break;
			case 'otherjob':
				$this->_output = $this->_showListOtherJob();                                
				break;
			case 'jobseo':
				$this->_output = $this->_showJobSeo();                                
				break;
			default:
				break;			
		}
        
    }
	
	function _showProgramListByCode(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$params = array(
			'program_type' => $this->_config['code']
		);
		$postData = '';
		$postData = json_encode($params);
		$result = $this->_model->getListProgramByCode($postData);

		if(isset($result['code']) && $result['code'] == 1){
			$dataOutput['list'] = $result['data'];
			foreach($dataOutput['list'] as $k => $v){
				$dataOutput['list'][$k] = (array)$v;
			}
		}
		// Convert name SEO
		foreach($dataOutput['list'] as $kS => $vS){
			$dataOutput['list'][$kS]['name_seo'] = $this->convertTitle($vS['name']);
			$dataOutput['list'][$kS]['name_en_seo'] = $this->convertTitle($vS['name_en']);
		}
		$dataOutput['CurrentId'] = isset($this->_params[2]) ? intval($this->_params[2]) : 0;
		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                 $this->_config['templateList'],
                                                 $dataOutput, $this->_config );
		return $output;									
	}
	
	function _showListOtherJob(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$searchCate = isset($this->_params[1]) && is_numeric($this->_params[1]) ? $this->_params[1] : 0;
		$cJob = isset($this->_params[5]) && is_numeric($this->_params[5]) ? $this->_params[5] : 0;
		
		$result = $this->_model->getRequestJob();
		if(isset($result['code']) && $result['code'] == 1){
			$data = $result['data'];
		}
		$dataTmp = array();
		if(count($data) > 0){
			foreach($data as $k => $vD){
				if($vD['vhr_job_id']['id'] == $searchCate && $vD['id'] != $cJob && $cJob != 0){
					$dataOutput['list'][] = $vD;
				}
			}
			foreach($data as $kTmp => $vTmp){
				if($vTmp['vhr_job_id']['id'] == $searchCate ){
					$dataTmp[] = $vTmp;
				}
			}
			if(count($dataOutput['list']) > $this->_config['rowShow']){
				$dataOutput['list'] = array_slice($dataOutput['list'], 0, $this->_config['rowShow']);
			}
			foreach($dataOutput['list'] as $k => $v){
				$dataOutput['list'][$k]['name_seo'] = $this->convertTitle($v['name']);	
				$dataOutput['list'][$k]['cate_seo'] = $this->convertTitle($v['vhr_job_id']['name']);	
				$dataOutput['list'][$k]['cate_en_seo'] = $this->convertTitle($v['vhr_job_id']['name_en']);	
				$dataOutput['list'][$k]['place_seo'] = $this->convertTitle($v['city_id'][1]);
				$dataOutput['list'][$k]['date_start_int'] = strtotime($v['date_start']);
			}
		}
		//var_dump($dataOutput);die;
		//$dataOutput['lastDate'] = strtotime($dataTmp[0]['date_start']);
		$dataOutput['timeDisplay'] = 14*86400;
		$dataOutput['currentTime'] = time();
		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                $this->_config['templateList'],
                                                $dataOutput, $this->_config );
		return $output;	
	}
	
	function _showListFaceFresher(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		$result = $this->_model->getListFaceFresher();
		
		if(isset($result['code']) && $result['code'] == 1){
			$dataOutput = $result['data'];
			foreach($dataOutput as $k => $v){
				$dataOutput[$k] = (array)$v;
			}
		}
		$dataOutput['CurrentId'] = isset($this->_params[1]) ? intval($this->_params[1]) : 0;
		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                $this->_config['templateList'],
                                                $dataOutput, $this->_config );
		return $output;									
	}
	
	function _showListJobCate(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		$result = $this->_model->getJob();
		
		if(isset($result['code']) && $result['code'] == 1){
			$dataOutput['cate'] = $result['data'];
			foreach($dataOutput['cate'] as $k => $v){
				$dataOutput['cate'][$k] = (array)$v;
			}
		}
		
		foreach($dataOutput['cate'] as $k => $v){
			$dataOutput['cate'][$k]['name_seo'] = $this->convertTitle($v['name']);
			$dataOutput['cate'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
		}
		
		if(isset($this->_config['isEnglish']) && $this->_config['isEnglish'] == 1){
			uasort($dataOutput['cate'], function($a, $b){
				return strcmp($a["name_en_seo"], $b["name_en_seo"]);
			});
		}else{
			uasort($dataOutput['cate'], function($a, $b){
				return strcmp($a["name_seo"], $b["name_seo"]);
			});
		}
							
		//$dataOutput['cate'] = asort($dataOutput['cate']);	
		$dataOutput['CurrentId'] = isset($this->_params[1]) ? intval($this->_params[1]) : 0;	
		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                $this->_config['templateList'],
                                                $dataOutput, $this->_config );
		return $output;									
	}
		
	function _showFormSearchJob(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		
		$result = $this->_model->getOffice();	
		if(isset($result['code']) && $result['code'] == 1){
			$dataOutput['office'] = $result['data'];
			foreach($dataOutput['office'] as $k => $v){
				$dataOutput['office'][$k] = (array)$v;
			}
		}
		
		$resJob = $this->_model->getJob();	
		if(isset($resJob['code']) && $resJob['code'] == 1){
			$dataOutput['jobcate'] = $resJob['data'];
			foreach($dataOutput['jobcate'] as $k => $v){
				$dataOutput['jobcate'][$k] = (array)$v;
			}
		}
		
		foreach($dataOutput['jobcate'] as $k => $v){
			$dataOutput['jobcate'][$k]['name_seo'] = $this->convertTitle($v['name']);
			$dataOutput['jobcate'][$k]['name_en_seo'] = $this->convertTitle($v['name_en']);
		}
		
		uasort($dataOutput['jobcate'], function($a, $b){
			return strcmp($a["name_seo"], $b["name_seo"]);
		});
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $post = Zend_Registry::get('post');			
			$dataOutput['currentOffice'] = isset($post['place']) ? intval($post['place']) : 0;
			$dataOutput['currentJobCate'] = isset($post['cate']) ? intval($post['cate']) : 0;
			$dataOutput['currentKeyword'] = isset($post['keyword']) ? $post['keyword'] : '';
		}
		
		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                $this->_config['templateForm'],
                                                $dataOutput, $this->_config );
		return $output;									
	}
	
	function _showJobSeo(){
		$output = '';
        $dataOutput = array();

        $this->_loadModel();
        if( !$this->_model )
        {
            return $output;
        }
		$jobCateId = isset($this->_params[1]) && is_numeric($this->_params[1]) ? $this->_params[1] : 0;
		$jobId = isset($this->_params[5]) && is_numeric($this->_params[5]) ? $this->_params[5] : 0;
		if($jobId != 0){	
			// Get list job
			$result = $this->_model->getRequestJob();
			if(isset($result['code']) && $result['code'] == 1){
				$data = $result['data'];
			}

			if(count($data) > 0){
				foreach($data as $k => $vD){
					if( $vD['id'] == $jobId){
						$dataOutput['title'] = $vD;
						break;
					}
				}			
			}
		}elseif($jobCateId != 0){
			// Get list job cate
			$result = $this->_model->getJob();
			$dataTmp = array();
			if(isset($result['code']) && $result['code'] == 1){
				$dataTmp = $result['data'];
				foreach($dataTmp as $k => $v){
					$dataTmp[$k] = (array)$v;
				}
			}
			
			if(count($dataTmp) > 0){
				foreach($dataTmp as $k => $v){
					if( $v['id'] == $jobCateId){
						$dataOutput['title'] = $v;
						break;
					}
				}			
			}		
		}else{
			$dataOutput['title']['name'] = $this->_config['titleSeoCate'];
		}

		$output = vPortal_Utility::loadView( $this->_config['featureCode'],
                                                $this->_config['templateSeo'],
                                                $dataOutput, $this->_config );
		return $output;	
	}
	
    function convertTitle($str,$sSplit="-"){
        $str = strip_tags($str);
        $pattern = array("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/",//Unicode - Unicode to hop
                        "/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/",
                        "/(ì|í|ị|ỉ|ĩ|ì|í|ị|ỉ|ĩ|I|Ì|Í|Ị|Ỉ|Ĩ|I|Ì|Í|Ị|Ỉ|Ĩ)/",
                        "/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/",
                        "/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/",
                        "/(ỳ|ý|ỵ|ỷ|ỹ|ỳ|ý|ỵ|ỷ|ỹ|Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ|Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ)/",
                        "/(đ|đ|D|Đ|Đ)/"
        );
        $replace            = array('a','e','i','o','u','y','d');
        $aUperPattern       = array("/B/","/C/","/F/","/G/","/H/","/J/","/K/","/L/","/M/","/N/","/P/","/Q/","/R/","/S/","/T/","/V/","/W/","/X/","/Z/");
        $aToLowerPattern    = array("b","c","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","z");
        $str = preg_replace($pattern, $replace, $str);

        $str = strtolower($str);
        $str = preg_replace('/[^a-z0-9]/', ' ', $str);
        $str = trim($str);
        $str = preg_replace("/\s{1,}/", ' ', $str);
        if(strlen($sSplit)>0)
            $str = preg_replace("/( )/", $sSplit, $str);
        return $str;
	}
	
	
   /**
    * function: output
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function output()
    {
        return $this->_output;
    }

   /**
    * function: _loadModel
    * description: get news model (if null)
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    private function _loadModel()
    {
        if( !$this->_model )
        {
            $this->_model = vPortal_Utility::loadModel( $this->_config['featureCode'],
                                                        'Application_Feature_Recruitment_Model_Recruitment' );
        }
    }
    
    
}