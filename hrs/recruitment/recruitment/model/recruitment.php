<?php
class Application_Feature_Recruitment_Model_Recruitment
{
	const AUTHKEY 		= "6a75acf3bcaec07d16df3355cfa2e5c7";
	const SERVICE_URL 	= "http://10.30.7.171/wsdl/MainSite/MainsiteService.wsdl";
	const RECRUITMENT 	= 'recruitment';
	
    private $_connection = null;

   /**
    * function: __construct
    * description: construct
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function __construct()
    {

    }

   /**
    * function: __destruct
    * description: destructor
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    public function __destruct()
    {
    }

   /**
    * function: _loadDbConnection
    * description: get DB connection (if null)
    * @author tynb@vng.com.vn
    * @copyright 2014 VNG
    */
    private function _loadDbConnection()
    {
        if( !$this->_connection )
        {
            $this->_connection = vPortal_Database::getInstance('data');
        }
    }
    
	private function doQueryList( $sFuncName, $aParam ){
		ini_set("soap.wsdl_cache_enabled", 1); 
		$sURL 		= self::SERVICE_URL;
		$sAuthkey 	= self::AUTHKEY;
		try{
			$client = new SoapClient($sURL);
			$result = $client->requestFunction($sFuncName, $aParam, $sAuthkey);				
		}catch (SoapFault $ex){
			echo $ex->getMessage();
		}
		return $result;
	}
	
	/**
     * function: subscribeNewsLetter
     * description: Dang ky nhan tin tuyen dung hoac bang tin danh cho sinh vien
    */   
	
    function subscribeNewsLetter($postData = ''){
        try {  		
            $result = $this->doQueryList('getSubscribe',$postData);   
			return $result;                 
         }catch (Exception $ex) {
            throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
         } 
    }    
	
	/**
     * function: getListProgramByCode
     * description: Lay danh sach cach chuong trinh: Fresher, Internship, Tour
    */
	
	function getListProgramByCode($postData = ''){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getListProgramByCode_".$postData.'_'.self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getProgramEvent',$postData);  
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}       
    }	
	
	/**
     * function: getListFaceFresher
     * description: Lay danh sach guong mat Fresher
    */
	
	function getListFaceFresher(){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getListFaceFresher_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getTypicalFace',''); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}
    }
	
	/**
     * function: getListSchool
     * description: Lay danh sach cac truong dai hoc
    */
	
	function getListSchool($postData = array()){
        static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getListSchool_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getSchool', $postData); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}	
    }
	
	/**
     * function: getListSchool
     * description: Lay danh sach cac truong dai hoc
    */
	
	function getListDegree($postData = array()){
        static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getListDegree_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getDegree', $postData); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}	
    }
	
	/**
     * function: getTimeGraduation
     * description: Lay danh sach thoi diem tot nghiep
    */
	
	function getTimeGraduation(){
        static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getTimeGraduation_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getTimeGraduation',''); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}		
    }
	
	/**
     * function: getKnowFresher
     * description: Lay danh sach: biet den Fresher thong qua
    */
	
	function getKnowFresher($postData=''){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getKnowFresher_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getKnowFresher',$postData); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}       
    }
	
	/**
     * function: registerTour
     * description: Dang ky tham gia VNG Tour
    */
	
	function registerTour($postData = ''){
        try {  		
            $result = $this->doQueryList('registerTour',$postData);   
			return $result;                 
         }catch (Exception $ex) {
            throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
         } 
    }	
	
	/**
     * function: registerFresher
     * description: Dang ky tham gia VNG Fresher
    */
	
	function registerFresher($postData = ''){
        try {  		
            $result = $this->doQueryList('registerFresher',$postData);   
			return $result;                 
         }catch (Exception $ex) {
            throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
         } 
    }
	
	function registerFresherDev($postData = ''){
        try {  		
            $result = $this->doQueryList('registerFresherDev',$postData);   
			return $result;                 
         }catch (Exception $ex) {
            throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
         } 
    }
	
	
	/**
     * function: submitCvOnline
     * description: Nop CV Online
    */
	
	function submitCvOnline($postData = ''){
        try {  		
            $result = $this->doQueryList('cvOnline',$postData);   
			return $result;                 
         }catch (Exception $ex) {
            throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
         } 
    }
	
	/**
     * function: getJob
     * description: Lay danh sach nhanh nghe nghiep
    */
	
	function getJob(){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getJob_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getJob',''); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}		 
    }
	
	/**
     * function: getRequestJob
     * description: Lay danh sach vi tri dang tuyen dung
    */
	
	function getRequestJob(){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getRequestJob_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getRequestJob',''); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}
    }
	
	/**
     * function: getOffice
     * description: Lay danh sach khu vuc tuyen dung
    */
	
	function getOffice(){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getOffice_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getOffice',''); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}	
    }
	
	/**
     * function: registerInternship
     * description: Dang ky tham gia VNG Internship
    */
	
	function registerInternship($postData = ''){
        try {  		
            $result = $this->doQueryList('registerInternship',$postData);   
			return $result;                 
         }catch (Exception $ex) {
            throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
         } 
    }	
	
	/**
     * function: getCountry
     * description: Lay danh sach quoc gia
    */
	
	function getCountry(){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("getCountry_".self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('getCountry',''); 
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}        
    }

/**
     * function: activeSubscribeNewsLetter
     * description: Active email sau khi xac nhan
    */
	
	function activeSubscribeNewsLetter($postData = ''){
		static $results = array();
        //----------------------------------------------
        // check cache
        //----------------------------------------------	
		
        $key = md5("activeSubscribeNewsLetter_".$postData.'_'.self::RECRUITMENT);

        if( isset( $results[$key] ) ){
            return $results[$key];
        }
		//----------------------------------------------
        //@hook before exec sql
        //----------------------------------------------
        $results = array();
        include vPortal_Hook::fetchHook( 'system', 'before_exec_sql' );          
			
		if( !empty($results[$key]))
        {
            return $results[$key];
        }		 
		//----------------------------------------------
		// get server list
		//----------------------------------------------
		try {
			$result = $this->doQueryList('activeEmailSubscription',$postData);  
			if( $result['code'] == 1 && is_array($result) )
	        {
	            $results[$key] = $result;									
	        }
			//----------------------------------------------
	        //@hook after exec sql
	        //----------------------------------------------
	        $tags = array( self::RECRUITMENT);
	        include vPortal_Hook::fetchHook( 'system', 'after_exec_sql' );			
		
			return $results[$key];
		}
		catch (Exception $ex) {
			throw new Exception('File: "'.__FILE__ .'", Line: "'. __LINE__ .'", '.ERR_PREFIX."0002 Exception : ".$ex->getMessage());
		}       
    }	
	
}