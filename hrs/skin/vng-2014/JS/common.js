jQuery(document).ready(function() {

    setActiveNavigation('.nav ul li a ', 'Active');
    setActiveNavigation('.subNav ul li a ', 'activeLink');
    // setActiveNavigation('#sidenavMenu li a', 'Hilite');

    // jQuery('#frmNewsSearch1').attr('action', window.location.href);
    checkProduct();

    jQuery('.topHeadRight li a').click(function() {
        var _valTranlate = getTranlate();
        var _default = window.location.protocol + '//' + window.location.hostname + jQuery(this).attr('href');
        if (_valTranlate != 'none') {
            window.open(_valTranlate, '_self');
        } else {
            window.open(_default, '_self');
        }
        return false;
    });

     initPlugin();
    jQuery('a.FacebookBtn').click(function() {
        popupShareFB(1497767220491786);
        return false;
    });
});


function checkProduct() {
    var hrefName = window.location.pathname;
    var choice = hrefName.split('/')[2];

    if (choice == 'san-pham' || choice == 'products') {
        jQuery('.footer').css('position', 'fixed');
    }
}




var enumType = {
    'vn': 'en',
    'en': 'vn',

    'gioi-thieu': 'about-us',
    'about-us': 'gioi-thieu',

    'lich-su-vng.html': 'history.html',
    'history.html': 'lich-su-vng.html',

    'van-hoa-doanh-nghiep.html': 'culture.html',
    'culture.html': 'van-hoa-doanh-nghiep.html',

    'ban-lanh-dao.html': 'director.html',
    'director.html': 'ban-lanh-dao.html',

    'doi-tac.html': 'partnership.html',
    'partnership.html': 'doi-tac.html',

    'san-pham': 'products',
    'products': 'san-pham',

    'noi-dung-so.html': 'media.html',
    'media.html': 'noi-dung-so.html',

    'lien-ket-cong-dong.html': 'social.html',
    'social.html': 'lien-ket-cong-dong.html',

    'phan-mem-tien-ich.html': 'software.html',
    'software.html': 'phan-mem-tien-ich.html',

    'thuong-mai-dien-tu.html': 'ecommerce.html',
    'ecommerce.html': 'thuong-mai-dien-tu.html',

    'tin-tuc-vng': 'news-vng',
    'news-vng': 'tin-tuc-vng',

    'lien-he': 'contact',
    'contact': 'lien-he',

    'thong-tin.html': 'infomation.html',
    'infomation.html': 'thong-tin.html',

    'sitemap.html': 'sitemap.html',

    'bao-mat.html': 'privacy.html',
    'privacy.html': 'bao-mat.html',
}

function getTranlate() {
    var _pathLink = window.location.pathname;
    var _listAgurment = _pathLink.split('/');
    var _hostname = window.location.protocol + '//' + window.location.hostname;
    var _rs = "";

    for (var i = 1; i < _listAgurment.length; i++) {
        if (enumType[_listAgurment[i]] != undefined) {
            _rs += '/' + enumType[_listAgurment[i]];
        } else {
            _rs = undefined;
        }
    };

    if (_rs != undefined) {
        return _hostname + _rs;
    } else {
        return 'none';
    }

}




function initPlugin() {
    jQuery('html').append('<div id="fb-root"></div>');
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}

function popupShareFB(appIdFB) {
    var title = document.title;
    url = window.location.href;
    if (appIdFB == undefined) {
        if (navigator.userAgent.indexOf('MSIE') != -1) {
            winDef = 'scrollbars=no,status=no,toolbar=no,location=nnoo,menubar=no,resizable=yes,height=430,width=550,top='.concat((screen.height - 500) / 2).concat(',left=').concat((screen.width - 500) / 2);
        } else {
            winDef = 'scrollbars=no,status=no,toolbar=no,location=no,menubar=no,resizable=no,height=400,width=550,top='.concat((screen.height - 500) / 2).concat(',left=').concat((screen.width - 500) / 2);
        }
        var url = '//www.facebook.com/sharer/sharer.php?u=' + url + '&t=' + title;

        var win = window.open(url, '_blank', winDef);
        var timer = setInterval(function() {
            if (win.closed) {
                clearInterval(timer);
            }
        }, 1000);
    } else {
        FB.init({
            appId: appIdFB,
            status: true,
            cookie: true
        });

        FB.ui({
            method: 'feed',
            link: url,
            caption: title,
            picture : 'http://img.zing.vn/vng/skin/vng-2014/image/vng-logo-share.jpg'
        }, function(response) {

        });
    }
}