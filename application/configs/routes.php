<?php

return array(

    'studentindex' => new Zend_Controller_Router_Route_Static(
        'danh-cho-sinh-vien',
        array(
            'module'     => 'default',
            'controller' => 'student',
            'action'     => 'index',
        ), array(
        ),
        'danh-cho-sinh-vien'
    ),
    'errorindex' => new Zend_Controller_Router_Route_Static(
        'error',
        array(
            'module'     => 'default',
            'controller' => 'index',
            'action'     => 'error',
        ), array(
        ),
        'error'
    ),
    'studentfresher' => new Zend_Controller_Router_Route_Static(
        'danh-cho-sinh-vien/chuong-trinh-vng-fresher',
        array(
            'module'     => 'default',
            'controller' => 'student',
            'action'     => 'fresher',
        ), array(
        ),
        'danh-cho-sinh-vien/chuong-trinh-vng-fresher'
    ),
     'emailactive' => new Zend_Controller_Router_Route_Regex(
        'xac-nhan-email/(.*)',
        array(
            'module'     => 'default',
            'controller' => 'student',
            'action'     => 'emailactive',
        ), array(
            '1' => 'data',
        ),
        'xac-nhan-email/%s'
    ),
     'studentdetail' => new Zend_Controller_Router_Route_Regex(
        'danh-cho-sinh-vien/([0-9]+)-(.*)',
        array(
            'module'     => 'default',
            'controller' => 'student',
            'action'     => 'detail',
        ), array(
            '1' => 'programId',
            '2' => 'programName',
        ),
        'danh-cho-sinh-vien/%d-%s.html'
    ),
     'eventdetail' => new Zend_Controller_Router_Route_Regex(
        'danh-cho-sinh-vien/([0-9]+)-([a-zA-Z0-9-]+)/([0-9]+).([0-9]+)-([a-zA-Z0-9-]+)',
        array(
            'module'     => 'default',
            'controller' => 'student',
            'action'     => 'detail',
        ), array(
            '1' => 'programId',
            '2' => 'programName',
            '3' => 'event_id',
            '4' => 'program_type',
            '5' => 'eventName',
        ),
        'danh-cho-sinh-vien/%d-%s/%d.%d-%s'
    ),
     'jobcategory' => new Zend_Controller_Router_Route_Regex(
        'co-hoi-nghe-nghiep/([0-9]+)-([a-zA-Z0-9-]+)(?:/trang\-(\d+))?',
        array(
            'module'     => 'default',
            'controller' => 'job',
            'action'     => 'category',
        ), array(
            '1' => 'id',
            '2' => 'name',
            '3' => 'page'
        ),
        'co-hoi-nghe-nghiep/%d-%s/trang-%d'
    ),
     'jobdetail' => new Zend_Controller_Router_Route_Regex(
        'co-hoi-nghe-nghiep/chi-tiet/([0-9]+).([0-9]+)-(.*)',
        array(
            'module'     => 'default',
            'controller' => 'job',
            'action'     => 'detail',
        ), array(
            '1' => 'catId',
            '2' => 'id',
            '3' => 'name',
        ),
        'co-hoi-nghe-nghiep/chi-tiet/%d.%d-%s.html'
    ),
     'jobsearch' => new Zend_Controller_Router_Route_Regex(
        'co-hoi-nghe-nghiep/tim-kiem(?:/trang\-(\d+))?',
        array(
            'module'     => 'default',
            'controller' => 'job',
            'action'     => 'search',
        ), array(
            '1' => 'page',
        ),
        'co-hoi-nghe-nghiep/tim-kiem/trang-%d'
    ),
     'jobindex' => new Zend_Controller_Router_Route_Regex(
        'co-hoi-nghe-nghiep(?:/trang\-(\d+))?',
        array(
            'module'     => 'default',
            'controller' => 'job',
            'action'     => 'index',
        ), array(
            '1' => 'page'
        ),
        'co-hoi-nghe-nghiep/trang-%d'
    ),
     'jobadvise' => new Zend_Controller_Router_Route_Regex(
        'co-hoi-nghe-nghiep/loi_khuyen',
        array(
            'module'     => 'default',
            'controller' => 'job',
            'action'     => 'advise',
        ), array(
            '1' => 'catId',
            '2' => 'id',
            '3' => 'name',
        ),
        'co-hoi-nghe-nghiep/loi_khuyen'
    ),
     'jobadvisedetail' => new Zend_Controller_Router_Route_Regex(
        'co-hoi-nghe-nghiep/loi_khuyen/([0-9]+)-(.*)',
        array(
            'module'     => 'default',
            'controller' => 'job',
            'action'     => 'advisedt',
        ), array(
            '1' => 'id',
            '2' => 'name',
        ),
        'co-hoi-nghe-nghiep/loi_khuyen/%d-%s.html'
    ),

    'lifeindex' => new Zend_Controller_Router_Route_Regex(
        'cuoc-song-vng',
        array(
            'module'     => 'default',
            'controller' => 'life',
            'action'     => 'index',
        ), array(
        ),
        'cuoc-song-vng'
    ),
     'lifedetail' => new Zend_Controller_Router_Route_Regex(
        'cuoc-song-vng/([0-9]+)-(.*)',
        array(
            'module'     => 'default',
            'controller' => 'life',
            'action'     => 'detail',
        ), array(
            '1' => 'id',
            '2' => 'name',
        ),
        'ccuoc-song-vng/%d-%s.html'
    ),
     'studentfaqs' => new Zend_Controller_Router_Route_Regex(
        'danh-cho-sinh-vien/hoi-dap',
        array(
            'module'     => 'default',
            'controller' => 'student',
            'action'     => 'faqs',
        ), array(
        ),
        'danh-cho-sinh-vien/hoi-dap'
    )
);