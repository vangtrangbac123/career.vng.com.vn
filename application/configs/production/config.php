<?php

// PHP settings
ini_set('display_startup_errors',       false);
ini_set('display_errors',               false);
ini_set('error_reporting',              32759); // E_ALL ^ E_NOTICE
ini_set('date.timezone',                'Asia/Ho_Chi_Minh');
class Config extends Config_Default {

    const IS_PRODUCTION = true;


    //const MEMCACHE_IP   = '123f-main.memcached.123.vn';
    //const MEMCACHE_PORT = '11218';
    const MEMCACHE_IP   = '127.0.0.1';
    const MEMCACHE_PORT = '11211';
    const MEMCACHE_TIME = 300;

}