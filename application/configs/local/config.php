<?php

// PHP settings
ini_set('display_startup_errors',       true);
ini_set('display_errors',               true);
ini_set('error_reporting',              E_ALL); // E_ALL ^ E_NOTICE
ini_set('date.timezone',                'Asia/Ho_Chi_Minh');

class Config extends Config_Default {

    const IS_LOCAL       = true;
    const PROXY_IP       = null;
    const PROXY_PORT     = null;
    const STATIC_VERSION = null;

}