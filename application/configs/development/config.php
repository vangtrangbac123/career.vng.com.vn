<?php

// PHP settings
ini_set('display_startup_errors',       true);
ini_set('display_errors',               true);
ini_set('error_reporting',              E_ALL); // E_ALL ^ E_NOTICE
ini_set('date.timezone',                'Asia/Ho_Chi_Minh');

class Config extends Config_Default {

    const IS_DEVELOPMENT = true;

    const PROXY_IP       = '10.76.0.57';
    const PROXY_PORT     = '3128';

    const MEMCACHE_IP    = '10.40.20.104';
    const MEMCACHE_PORT  = '11211';

}


// check access
// require_once 'Zend/Session.php';
// Zend_Session::start();

// if (!isset($_SESSION['allow_access'])) {

//     if (in_array(App::getClientIp(), array('115.77.66.229', '118.102.7.146'))
//         || (isset($_COOKIE['passcode']) ? $_COOKIE['passcode'] : (isset($_GET['passcode']) ? $_GET['passcode'] : '') == 'tpfdev')
//         || strstr($_SERVER['HTTP_USER_AGENT'], 'facebookexternalhit')) {

//         $_SESSION['allow_access'] = true;

//     } else {

//         header('HTTP/1.0 404 Not Found');
//         die;

//     }

// }