<?php

class Default_Bootstrap extends Zend_Application_Module_Bootstrap {

	protected function _initSession() {
        Zend_Session::start();
    }
    protected function _initRouter() {
        $registry = My_Registry::getInstance();
        $front = Zend_Controller_Front::getInstance();
        $front->setRouter($registry->getRouter());
        return $front;
    }
}
