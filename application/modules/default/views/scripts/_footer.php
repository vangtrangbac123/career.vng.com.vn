<!-- Begin block footer-ver-vn_footer-ver-vn - MjU2fGZvb3Rlci12ZXItdm58NTIzfHZuX3ZuZy1mcmVzaGVyfGZvb3Rlci12ZXItdm58SFRNTA --><div class="footer">
                    <div class="container footer_container">
                        <div class="footerBlock footerLeft">
                            <div class="footerLink">
                                <ul>
                                    <li><a href="https://www.vng.com.vn/vn/gioi-thieu/sitemap.html">Sơ đồ trang web</a>
                                    </li>&nbsp;|&nbsp;
                                    <li><a href="https://www.vng.com.vn/vn/gioi-thieu/bao-mat.html">Bảo mật</a>
                                    </li>&nbsp;|&nbsp;
                                    <li><a href="https://www.vng.com.vn/vn/lien-he/thong-tin.html">Liên hệ</a>
                                    </li>
                                </ul>
                            </div>
                            <!--end footerLink -->
                        </div>
                        <!--end footerLeft -->
                        <div class="footerBlock footerMid">&copy; 2014 Công ty Cổ phần VNG</div>
                        <!--end footerMid -->
                        <div class="footerBlock footerRight">
                            <div class="connectUs">
                                <ul>
                                    <li>
                                        <a rel="nofollow" target="_blank" href="https://www.facebook.com/VNGCorporation.Page?hc_location=stream">
                                            <img width="26" height="26" src="https://img.zing.vn/vng/skin/vng-2014/image/footer/facebook.png">
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" rel="publisher" href="https://plus.google.com/107030863331028600810">
                                            <img width="26" height="26" src="https://img.zing.vn/vng/skin/vng-2014/image/footer/icon-gplus.png">
                                        </a>
                                    </li>

                                    <li>
                                        <a target="_blank" rel="nofollow" href="https://twitter.com/vngcorporation">
                                            <img width="26" height="26" src="https://img.zing.vn/vng/skin/vng-2014/image/footer/twitter-icon.png">
                                        </a>
                                    </li>


                                    <li>
                                        <a target="_blank" rel="nofollow" href="https://www.linkedin.com/company/vng_2">
                                            <img width="26" height="26" src="https://img.zing.vn/vng/skin/vng-2014/image/footer/linkedIn_icon.png">
                                        </a>
                                    </li>

                                    <li>
                                        <a rel="nofollow" target="_blank" href="https://me.zing.vn/b/vng">
                                            <img width="26" height="26" src="https://img.zing.vn/vng/skin/vng-2014/image/footer/zing.png">
                                        </a>
                                    </li>
                                </ul>
                                <p style="float:right">Kết nối với chúng tôi: &nbsp;</p>
                                <div class="clearFloat"></div>
                                <!--end clearFloat -->
                            </div>
                            <!--end connectUs -->
                        </div>
                        <!--end footerRight -->
                        <div class="clearFloat"></div>
                        <!--end clearFloat -->
                    </div>
                    <!--end footer_container -->
                </div>


<div class="overlay" id="overlay-book">
    <div class="model">
        <div class="modal-body"></div>
        <div class="modal-footer">
            <span class="btn btn-close">Đóng</span>
        </div>
    </div>
</div>