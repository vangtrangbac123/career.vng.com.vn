    <div class="header">
        <div class="topHeader">
            <div class="container topHeaderContainer">
                <!-- Begin block top-header-block-ver-vn_top-header-block-ver-vn - MjU4fHRvcC1oZWFkZXItYmxvY2stdmVyLXZufDUyM3x2bl92bmctZnJlc2hlcnx0b3AtaGVhZGVyLWJsb2NrLXZlci12bnxIVE1M --><div class="topHeadBlock topHeadLeft">
                                <a class="logo" href="/vn/index.html">
                                   <!-- <img src="https://img.zing.vn/vng/skin/vng-2014/image/header/vngLogo.png" width="217" height="105" /> -->
                                </a>
                            </div>
                            <div class="topHeadBlock topHeadRight">
                                <ul>
                                    <li>
                                        <a href="/en/index.html">English</a>
                                    </li>
                                    <br>
                                    <li>
                                        <a class="logo10-vn" href="//10.vng.com.vn/" target="_blank" title="VNG 10">
                                            <!--<img width="728" height="90" src="https://img.zing.vn/vng/skin/vng-2014/image/header/topBanner.png"> -->
</a>
                                    </li>

                                </ul>
                                <div class="clearFloat"></div>
                            </div><!-- End block top-header-block-ver-vn_top-header-block-ver-vn -->
                <!--end topHeadRight -->
                <div class="clearFloat"></div>
                <!--end clearFloat --> </div>
            <!--end topHeaderContainer --> </div>
        <!--end topHeader -->
        <div class="nav">
            <div class="container headerContainer">
                <!-- Begin block navigation-version-vn_navigation-version-vn - MjU3fG5hdmlnYXRpb24tdmVyc2lvbi12bnw1MjN8dm5fdm5nLWZyZXNoZXJ8bmF2aWdhdGlvbi12ZXJzaW9uLXZufEhUTUw --><ul>
                                <li>
                                    <a href="https://www.vng.com.vn/vn/index.html">
                                        TRA<span class="navSpan ">NG</span> CHỦ
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.vng.com.vn/vn/gioi-thieu/lich-su-vng.html" onmouseout="doOut(2)" onmouseover="doOver(2)">
                                        GIỚ<span id="navSpan_2">I TH</span>IỆU
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.vng.com.vn/vn/san-pham/noi-dung-so.html" onmouseout="doOut(3)" onmouseover="doOver(3)">
                                        SẢ<span id="navSpan_3">N PH</span>ẨM
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.vng.com.vn/vn/tin-tuc-vng/danh-sach.tin-tuc-vn-vng.html" onmouseout="doOut(4)" onmouseover="doOver(4)">
                                        TIN TỨ<span id="navSpan_4">C - S</span>Ự KIỆN
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.vng.com.vn/vn/tuyen-dung" onmouseout="doOut(5)" onmouseover="doOver(5)">
                                        TUY<span id="navSpan_5">ỂN D</span>ỤNG
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.vng.com.vn/vn/lien-he/thong-tin.html" onmouseout="doOut(6)" onmouseover="doOver(6)">
                                        LI<span id="navSpan_6">ÊN</span> HỆ
                                    </a>
                                </li>
                            </ul><!-- End block navigation-version-vn_navigation-version-vn -->
            </div>
            <!--end headerContainer --> </div>
        <!--end nav -->

        <!--subNav -->
        <div class="subNav">
            <div class="container headerContainer">
                <ul>
                    <li class="subNavActive">
                        <?php 
                            $arr = array(
                                    'student' => array('/danh-cho-sinh-vien','Dành cho sinh viên'),
                                    'job'     => array('/co-hoi-nghe-nghiep','Cơ hội nghề nghiệp'),
                                    'life'    => array('/cuoc-song-vng','Cuộc sống VNG')
                                );
                        ?>
                        <a href="<?php echo $arr[$this->controllerName][0]?>">TUYỂN DỤNG  &gt; <?php echo $arr[$this->controllerName][1]?></a>
                    </li>
                    <li style="width:240px;">
                        <a href="/co-hoi-nghe-nghiep" class="<?php echo ($this->controllerName == 'job') ?'activeLink':'' ?>">Cơ hội nghề nghiệp</a>
                    </li>
                    <li style="width:216px;">
                        <a href="/cuoc-song-vng" class="<?php echo ($this->controllerName == 'life') ?'activeLink':'' ?>">Cuộc sống VNG</a>
                    </li>
                    <li style="width:236px;">
                        <a href="/danh-cho-sinh-vien" class="<?php echo ($this->controllerName == 'student') ?'activeLink':'' ?>">Dành cho sinh viên</a>
                    </li>
                </ul>
            </div>
            <!--end headerContainer -->
        </div>
        <!--end subNav --> </div>
    <!--end header -->