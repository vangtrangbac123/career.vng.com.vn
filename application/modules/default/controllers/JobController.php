<?php

class Default_JobController extends My_Controller_Action {

    public function init() {

    }

    public function getCareer(){
    	$key = 'getCareerProgram';
        $memcache = My_Memcache::getInstance();
        $school = $memcache->get($key);

        if (!$school){
        	$school =  Api_Erp::getSchool();
            $school->renderform = $this->getEventDetail($school);
            $memcache->set($key, $school, 86400);
        }


        return $school;
    }

    public function getEventDetail($result){
       $fields = $result->field_ids;
       $renderform = array();
       //Div begin
       foreach ($fields as $key => $field) {
          switch ($field->field_type) {
            case 'char':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'interger':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'float':
              $renderform[] = My_View_Register::textboxfloat($field);
              break;
            case 'text':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'datetime':
              $renderform[] = My_View_Register::datetime($field);
              break;
            case 'time':
              $renderform[] = My_View_Register::time($field);
              break;
            case 'binary':
              $renderform[] = My_View_Register::file($field);
              break;
            case 'selection':
              $renderform[] = My_View_Register::selectbox($field);
              break;
            case 'many2one':
              $renderform[] = My_View_Register::selectbox($field);
              break;
            case 'many2many':
              $renderform[] = My_View_Register::checkbox($field);
              break;
          }
       }


       //$renderform[] = My_View_Register::captcha();
       //Program_type
        $renderform[] = My_View_Register::program_event_id($result->id);
        //Button regiter
        $renderform[] = My_View_Register::button_submit(false);
        //End
     return $renderform;
  }

    public function indexAction() {
        $params = $this->getRequest()->getParams();

        $page     = isset($params['page']) ? abs(intval($params['page'])) : 1;
        $count    = 8;
        $offset   = $count * ($page - 1);
        $pageLink = '/co-hoi-nghe-nghiep/trang-%d/';
        $school = $this->getCareer();

        $this->view->school = $school;

        $total = count($school->job_program_ids);
        $school->job_program_ids = array_slice($school->job_program_ids, $offset, $count);

        $this->view->paginator = Utility_Paginator::create($pageLink, $page, $count, $total);
    }

    public function categoryAction() {
        $params = $this->getRequest()->getParams();
        $list_job = array();

        if (!isset($params['id'])) {
            $this->_redirect('/error');
        }

        $page     = isset($params['page']) ? abs(intval($params['page'])) : 1;
        $count    = 8;
        $offset   = $count * ($page - 1);
        $pageLink = "/co-hoi-nghe-nghiep/".$params['id']."-".$params['name']."/trang-%d/";

        $job_category_id = $params['id'];

        //$school =  Api_Erp::getSchool();

        $school = $this->getCareer();

        foreach ($school->job_program_ids as $key => $item) {
            if($item->vhr_job_id[0] == $job_category_id){
                $list_job[] = $item;
            }
        }
        $total = count($list_job);
        $list_job = array_slice($list_job, $offset, $count);

        $this->view->school = $school;
        $this->view->catId = $job_category_id;
        $this->view->list_job = $list_job;
        $this->view->paginator = Utility_Paginator::create($pageLink, $page, $count, $total);
    }


    public function detailAction() {
    	$params = $this->getRequest()->getParams();

        if (!isset($params['catId']) || !isset($params['id'])) {
            $this->_redirect('/error');
        }

        $job_category_id = $params['catId'];
        $id = $params['id'];
        $list_job_related = array();


    	//$school =  Api_Erp::getSchool();

        $school = $this->getCareer();

    	foreach ($school->job_cat_ids as $key => $cat) {
    		if($cat->id == $job_category_id){
                $job_category_name = $cat->name;
    			foreach ($cat->job_program_vn_ids as $key => $item) {
                    if($item->id == $id){
                        $job_item = $item;
                    }else{
                        $list_job_related[] = $item;
                    }
                 }
    		}
    	}

    	$this->view->school = $school;
        $this->view->job_item = $job_item;
        $this->view->list_relate = $list_job_related;
        $this->view->catId = $job_category_id;
        $this->view->catName = $job_category_name;
    }

     public function searchAction() {
    	$params = $this->getRequest()->getParams();


        if (!isset($params['keyword']) || !isset($params['cate_id']) || !isset($params['city_id'])) {
            $this->_redirect('/error');
        }

        $job_category_id = $params['cate_id'];
        $city_id = $params['city_id'];
        $keyword = $params['keyword'];

        $page     = isset($params['page']) ? abs(intval($params['page'])) : 1;
        $count    = 8;
        $offset   = $count * ($page - 1);
       $pageLink = "/co-hoi-nghe-nghiep/tim-kiem/trang-%d/?keyword=".$params['keyword']."&cate_id=".$params['cate_id']."&city_id=".$params['city_id'];



    	//$school =  Api_Erp::getSchool();

        $school = $this->getCareer();

    	$list = $this->searchJob($school, $keyword, $job_category_id, $city_id );
        $total = count($list);
        $list = array_slice($list, $offset, $count);


    	$school->job_program_ids = $list;
    	$this->view->school = $school;
    	$this->view->post = $params;
        $this->view->paginator = Utility_Paginator::create($pageLink, $page, $count, $total);
        $this->_helper->viewRenderer('index');
    }

    public function adviseAction(){
    	$key = 'getCareerProgram.advise';
        $memcache = My_Memcache::getInstance();
        $advise = $memcache->get($key);

        if (!$advise){
        	$advise =  Api_Erp::getDynamicInfo(array('method' =>'get_advive','data' => array()));
            $memcache->set($key, $advise, 86400);
        }

        $this->view->advise = $advise;
        $this->view->school = $this->getCareer();

    }

    public function advisedtAction(){

    	$params = $this->getRequest()->getParams();

        if (!isset($params['id'])) {
            $this->_redirect('/error');
        }

    	$key = 'getCareerProgram.advise';
        $memcache = My_Memcache::getInstance();
        $advise = $memcache->get($key);

        if (!$advise){
        	$advise =  Api_Erp::getDynamicInfo(array('method' =>'get_advive','data' => array()));
            $memcache->set($key, $advise, 86400);
        }

        foreach ($advise as $key => $item) {
        	if($item->id == $params['id']){
        		$advise_item = $item;
        	}
        }

        $this->view->advise_item = $advise_item;
        $this->view->school = $this->getCareer();

    }


    private function searchJob($school, $keyword, $job_category_id, $city_id ){
        $list_job = array();

        if($job_category_id == 0 && $city_id == 0 ){
            foreach ($school->job_program_ids as $key => $item) {
                $chk_key = true;
                $chk_id = true;
            	if(!empty($keyword)) {
                    $chk_key = strpos(strtolower($item->name),strtolower($keyword))!== false;
                    $chk_id  = strpos(strtolower($item->code),strtolower($keyword))!== false;
                }

            	if($chk_key || $chk_id){
                    $list_job[] = $item;
                }
            }

        }

        if($job_category_id != 0 && $city_id == 0 ){
            foreach ($school->job_program_ids as $key => $item) {
            	$chk_key = true;
                $chk_id = true;
                if(!empty($keyword)) {
                    $chk_key = strpos(strtolower($item->name),strtolower($keyword))!== false;
                    $chk_id  = strpos(strtolower($item->code),strtolower($keyword))!== false;
                }
            	if(($chk_key || $chk_id) && $job_category_id == $item->vhr_job_id[0]){
                    $list_job[] = $item;
                }
            }

        }

        if($job_category_id == 0 && $city_id != 0 ){
            foreach ($school->job_program_ids as $key => $item) {
            	$chk_key = true;
                $chk_id = true;
                if(!empty($keyword)) {
                    $chk_key = strpos(strtolower($item->name),strtolower($keyword))!== false;
                    $chk_id  = strpos(strtolower($item->code),strtolower($keyword))!== false;
                }
            	if(($chk_key || $chk_id) && $city_id == $item->city_id[0]){
                    $list_job[] = $item;
                }
            }

        }

        if($job_category_id != 0 && $city_id != 0 ){
            foreach ($school->job_program_ids as $key => $item) {
            	$chk_key = true;
                $chk_id = true;
                if(!empty($keyword)) {
                    $chk_key = strpos(strtolower($item->name),strtolower($keyword))!== false;
                    $chk_id  = strpos(strtolower($item->code),strtolower($keyword))!== false;
                }
            	if(($chk_key || $chk_id) && $city_id == $item->city_id[0] && $job_category_id == $item->vhr_job_id[0]){
                    $list_job[] = $item;
                }
            }

        }

        return $list_job;

    }


}
