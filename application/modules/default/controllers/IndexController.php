<?php

class Default_IndexController extends My_Controller_Action {

    public function init() {
    }

    public function indexAction() {
        $this->redirect('/co-hoi-nghe-nghiep');
    }

    public function errorAction(){

    }

    public function gencaptchaAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $string = '';
        for ($i = 0; $i < 5; $i++) {
            $string .= chr(rand(97, 122));
        }

        $_SESSION['captcha'] = $string; //store the captcha

        $dir = "/layouts/web/font/";
        $image = imagecreatetruecolor(165, 50); //custom image size
        $font = "UnisectHeadlineVnu-1.ttf"; // custom font style
        $color = imagecolorallocate($image, 113, 193, 217); // custom color
        $white = imagecolorallocate($image, 255, 255, 255); // custom background color
        imagefilledrectangle($image,0,0,399,99,$white);
        imagettftext ($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);

        header("Content-type: image/png");
        imagepng($image);
    }

}
