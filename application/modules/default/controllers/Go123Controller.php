<?php

class Default_Go123Controller extends My_Controller_Action {

    const ERR_EXT = 'Sai định dạng file cho phép';
    const ERR_SIZE = 'Kích thước file vượt quá giới hạn cho phép';

    public function init() {
    }

    public function uploadAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->_request->isPost()){// Người dùng đã ấn submit
		    $total_size = 0;
            $name = '';
            if(isset($_FILES['upload']['name'])){
                foreach ($_FILES['upload']['name'] as $key => $file) {
                    if(empty($file)) continue;
                    $ex = explode('.',$file);
                    $ex = $ex[count($ex) - 1];
                    if(count($ex) > 0) $name = str_replace( '.'.$ex, '', $file);
                    $arrEx = explode(',', $_POST['upload']['filter'][$key]);

                    if(!in_array($ex, $arrEx) &&  !in_array(strtoupper($ex), $arrEx)){
                        $this->returnData(false, self::ERR_EXT);
                    }

                    if($_FILES['upload']['size'][$key] > $_POST['upload']['file'][$key]*1024*1024){
                        $this->returnData(false, self::ERR_SIZE);
                    }

                    $path = "upload/";
                    $rand = md5(uniqid('123phim'));
                    $systemName = $name.'_'.substr($rand, 0, 3). time();

                    $name = $systemName.'.'.$ex;
                    $file = $path.$name;

                    move_uploaded_file($_FILES['upload']['tmp_name'][$key], $file);
                    $this->returnData(true, array(
                    							'url' => 'http://'.$_SERVER['HTTP_HOST'].'/'.$file,
                    							'filename' => $name));

                }
            }
		}
    }

}
