<?php

class Default_IframeController extends My_Controller_Action {

    public function init() {
    }

    public function fresherAction() {
    	$this->_helper->layout->disableLayout();
    	$captcha = $this->genCaptcha($this);
        $this->view->captcha = $captcha;
        $this->view->captchaUrl = $captcha->url;
    }

     /**
     *
     * Generate new captcha
     * @param Zend_Controller_Action $controller
     * @var Zend_Captcha_Image
     */
    public function genCaptcha($controller){

        $module = $this->getRequest()->getModuleName();
        $captcha = new Zend_Captcha_Image();
        $captcha->setImgDir(ROOT . "/layouts/capcha/")
                ->setImgUrl($controller->view->baseUrl("/layouts/capcha"))
                ->setFont(ROOT . "/layouts/web/font/UnisectHeadlineVnu-1.TTF")
                ->setWordlen(4)
                ->setFontSize(14)
                ->setDotNoiseLevel(2)
                ->setLineNoiseLevel(2)
                ->setWidth(100)
                ->setHeight(40)
                ->generate();
        $captcha->url = $captcha->getImgUrl().$captcha->getId().$captcha->getSuffix();
        return $captcha;
    }

}
