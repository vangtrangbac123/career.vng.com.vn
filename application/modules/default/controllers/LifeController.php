<?php

class Default_LifeController extends My_Controller_Action {

    public function init() {

    }

    public function indexAction() {

    	$key = 'getLifeVng';
        $memcache = My_Memcache::getInstance();
        $life = $memcache->get($key);

        if (!$life){
        	$life =  Api_Erp::getDynamicInfo(array('method' =>'get_life_at_vng','data' => array()));
            $memcache->set($key, $life, 86400);
        }

        $this->view->life = $life;

    }

    public function benefitAction(){

    }

    public function detailAction(){
    	$params = $this->getRequest()->getParams();

        if (!isset($params['id'])) {
            $this->_redirect('/error');
        }

        $key = 'getLifeVng';
        $memcache = My_Memcache::getInstance();
        $life = $memcache->get($key);

        if (!$life){
        	$life =  Api_Erp::getDynamicInfo(array('method' =>'get_life_at_vng','data' => array()));
            $memcache->set($key, $life, 86400);
        }

        foreach ($life as $key => $item) {
        	if($item->id == $params['id']){
        		$this->view->life_item = $item;
        	}
        }


    }

    public function enviromentAction(){

    }


}
