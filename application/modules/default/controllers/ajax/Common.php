<?php

class Default_Ajax_Common{


    public $_controller;
	protected $_object;
    private $api;

	function __construct($obj = null, $controller = null) {
		$this->_object     = $obj;
		$this->_controller = $controller;
	}

  public function delCache($params){
      if(!isset($params['key'])){
        return array('message' => 'Params key not found', 'code' => false);
      }

      $memcache = My_Memcache::getInstance();
      $result = $memcache->delete($params['key']);
      return array('message' => '', 'code' => $result);

  }

  public function delAllCache($params){

      $memcache = My_Memcache::getInstance();
      $result = $memcache->deletekeyall();
      return array('message' => '', 'code' => $result);

  }

  public function getEventDetail($params){
       if (!isset($params['event_id'])) return false;
       if (!isset($params['program_type'])) return false;

      $key = sprintf('getRecruitmentProgram.event.%d.%s', $params['event_id'], $params['program_type']);
      $memcache = My_Memcache::getInstance();
      $result = $memcache->get($key);
      if (!$result){
       $result = Api_Erp::getProgramEvent($params);
       $memcache->set($key, $result, 86400);
      }

       $has_ques = false;
       if(!isset($result->field_ids)) return $result;
       if(isset($result->part_ids) && count($result->part_ids) > 0) $has_ques = true;
       $fields = $result->field_ids;
       $renderform[] = array();
       //Div begin
       foreach ($fields as $key => $field) {
          switch ($field->field_type) {
            case 'char':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'interger':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'float':
              $renderform[] = My_View_Register::textboxfloat($field);
              break;
            case 'text':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'datetime':
              $renderform[] = My_View_Register::datetime($field);
              break;
            case 'time':
              $renderform[] = My_View_Register::time($field);
              break;
            case 'binary':
              $renderform[] = My_View_Register::file($field);
              break;
            case 'selection':
              $renderform[] = My_View_Register::selectbox($field);
              break;
            case 'many2one':
              $renderform[] = My_View_Register::selectbox($field);
              break;
            case 'many2many':
              $renderform[] = My_View_Register::checkbox($field);
              break;
          }
       }


       //$renderform[] = My_View_Register::captcha();
       //Program_type
        $renderform[] = My_View_Register::program_event_id($params['event_id']);
        //Event Name
        $renderform[] = My_View_Register::event_name($result->name);

        //Button regiter
        $renderform[] = My_View_Register::button_submit($has_ques);
        //Message
        $renderform[] = My_View_Register::popup_success($result->popup_success);
        $renderform[] = My_View_Register::popup_fail($result->popup_fail);
        //End
       $result->renderform = $renderform;


       if($has_ques){
          $questions = $result->part_ids;
          $questionform[] = array();
            $questionform[] = My_View_Register::title('Phần câu hỏi');
             foreach ($questions as $i => $part) {
                $questionform[] = My_View_Register::part($part);
                foreach ($part->question_ids as $j => $question) {
                  $key = $j+1;

                  switch ($question->question_type_code) {
                    case 'YESNO':
                      $questionform[] = My_View_Register::selectquestion($question, $key);
                      break;
                    case 'MULTICHOICE_MA':
                      $questionform[] = My_View_Register::selectquestion($question, $key);
                      break;
                    case 'MULTICHOICE_RICHTEXT':
                      $questionform[] = My_View_Register::checkboxrichtextquestion($question, $key);
                      break;
                    case 'MULTICHOICE':
                      $questionform[] = My_View_Register::checkboxquestion($question, $key);
                      break;
                    case 'TEXT':
                      $questionform[] = My_View_Register::textareaquestion($question, $key);
                      break;
                  }
               }
            }

          $questionform[] = My_View_Register::button_back();
          $result->questionform = $questionform;
       }
     return $result;
  }

  /**
     *
     * Generate new captcha
     * @param Zend_Controller_Action $controller
     * @var Zend_Captcha_Image
     */
    public function genCaptcha($controller){
        $captcha = new Zend_Captcha_Image();
        $captcha->setImgDir(ROOT . "/layouts/capcha/")
                ->setImgUrl($this->_controller->view->baseUrl("/layouts/capcha"))
                ->setFont(ROOT . "/layouts/web/font/UnisectHeadlineVnu-1.TTF")
                ->setWordlen(4)
                ->setUseNumbers(false)
                ->setFontSize(14)
                ->setDotNoiseLevel(2)
                ->setLineNoiseLevel(2)
                ->setWidth(100)
                ->setHeight(40)
                ->generate();
        $captcha->url = $captcha->getImgUrl().$captcha->getId().$captcha->getSuffix();
        return $captcha;
    }

    public function refreshcaptcha(){
        $captcha = $this->genCaptcha($this);
        $data = array();
        $data["id"]  = $captcha->getId();
        $data["url"] = $captcha->url;
        return $data;
    }

    public function checkcaptchaRegister($params){

      $data = array();
      if (empty($params['captcha']['input'])) return array('error' => 'Vui lòng nhập mã xác nhận');
      $capCode = $params['captcha']['input'];
      //$capSession = new Zend_Session_Namespace('Zend_Form_Captcha_'.$capId);
      if ($capCode != $_SESSION['captcha']){
        return array('error' => 'Mã xác nhận không đúng');
      }
      return true;

    }

    public function regReceiveNews($params){
      if (!isset($params['member']) ) return array('code'=> false, 'message' => 'Dữ liệu không hợp lệ.');
      $result = Api_Erp::getSubscribe($params['member']);

      if($result && $result->code){
          if($result->code == 1 ){
              return  array('code'=> true, 'message' => 'Cảm ơn bạn đã đăng ký nhận tin qua email, vui lòng hoàn tất đăng ký bằng việc xác nhận email trong vòng 24h.');
          }else{
              return  array('code'=> false, 'message' => 'Có  lỗi. Vui lòng đăng ký lại.');
          }
      }

       return  array('code'=> false, 'message' => 'Có  lỗi. Vui lòng đăng ký lại.');

    }

}
