<?php

class Default_StudentController extends My_Controller_Action {
    const  SUCCESS = 1;
    const  ERROR = 0;
    private $success;
    private $fail;

    public function init() {

    }

    public function indexAction() {
        $key = 'getRecruitmentProgram';
        $memcache = My_Memcache::getInstance();
        $listProgram = $memcache->get($key);

        if (!$listProgram){
            $listProgram =  Api_Erp::getRecruitmentProgram();
            $memcache->set($key, $listProgram, 86400);
        }



        $this->view->listProgram = $listProgram;
    }

    public function emailactiveAction(){
        $params = $this->getRequest()->getParams();
        $message = 'Dữ  liệu không hợp lệ';

        if (!isset($params['data'])) {
            $message = 'Url không hợp lệ';
        }

        $data =  base64_decode($params['data']);


        parse_str($data, $output);
        $result =  Api_Erp::activeEmailSubscription($output);
        // var_dump($active);die;
        // if($active){
        //     $message = 'Kích  hoạt email thành công';
        // }

        if($result && $result->code){
              if($result->code == 1 ){
                  $message = 'Kích  hoạt email thành công';
              }else{
                  $message = 'Có  lỗi. Vui lòng đăng ký lại.('.$result->message.')';
              }
        }

        $this->view->message = $message;
    }


    public function registerAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost() && isset($_POST['member']))
        {

            if(isset($_POST['file'])){
                foreach ($_POST['file'] as $key => $file) {
                    if(!empty($file['url'])){
                        $_POST['member'][$key] = $_POST['file'][$key];
                    }
                }
            }


            $questions = array();
            if(isset($_POST['question'])){
                foreach ($_POST['question'] as $key => $question) {
                    if(!is_array($question)) $question = array($question);
                    $questions[] = array('id' => $key, 'value' => $question);
                }
                $_POST['member']['test_result_ids'] = $questions;
            }

            //Popup
            $this->success = "Thanh cong";
            $this->fail = "That bai";

            if(isset($_POST['popup'])){
              $this->success = $_POST['popup']['success'];
              $this->fail = $_POST['popup']['fail'];
            }

            //echo json_encode($_POST['member']);die;
            $result =  Api_Erp::receiveApplicationFromFe($_POST['member']);

            if($result && $result->code){
                if($result->code == 1 ){
                    $this->returnData(true, $this->success);
                }else{
                    $this->returnData(false, $this->fail);
                    //Log
                    $txt = $_POST['member']['email'].'-'.$result->message."\r\n";
                    $myfile = fopen("logemail.txt", "a");
                    fwrite($myfile, $txt);
                    //End Log
                }
            }

            $this->returnData(false, $this->fail);
        }
    }
    public function fresherAction(){
        $id = 13;
        $key = sprintf('getRecruitmentProgram.%s', $id);
        $memcache = My_Memcache::getInstance();
        $program = $memcache->get($key);
        if (!$program){
            $program =  Api_Erp::getRecruitmentProgram(array('program_id' => $id));
            $memcache->set($key, $program, 86400);
        }
        $this->view->program = $program;
    }

     public function detailAction(){
        $params = $this->getRequest()->getParams();

        if (!isset($params['programId']) || !isset($params['programName'])) {
            $this->_redirect('/error');
        }

        $key = sprintf('getRecruitmentProgram.%s', $params['programId']);
        $memcache = My_Memcache::getInstance();
        $program = $memcache->get($key);

        if (!$program){
            $program =  Api_Erp::getRecruitmentProgram(array('program_id' => $params['programId']));
            $memcache->set($key, $program, 86400);
        }
        $this->view->program = $program;
        $this->_helper->viewRenderer('fresher');
    }

    public function faqsAction(){

        $key = 'question.awnser';
        $memcache = My_Memcache::getInstance();
        $awnser = $memcache->get($key);

        if (!$awnser){
            $awnser =  Api_Erp::getDynamicInfo(array('method' =>'get_faqs','data' => array()));
            $memcache->set($key, $awnser, 86400);
        }

        $key = 'getRecruitmentProgram';
        $memcache = My_Memcache::getInstance();
        $listProgram = $memcache->get($key);

        if (!$listProgram){
            $listProgram =  Api_Erp::getRecruitmentProgram();
            $memcache->set($key, $listProgram, 86400);
        }



        $this->view->listProgram = $listProgram;
        $this->view->awnser = $awnser;
    }

  public function eventdetailAction(){

        $params = $this->getRequest()->getParams();

        if (!isset($params['programId']) || !isset($params['programName'])) {
            $this->_redirect('/error');
        }

       if (!isset($params['event_id']) || !isset($params['program_type'])){
            $this->_redirect('/error');
        }

        $key = sprintf('getRecruitmentProgram.%s', $params['programId']);
        $memcache = My_Memcache::getInstance();
        $program = $memcache->get($key);

        if (!$program){
            $program =  Api_Erp::getRecruitmentProgram(array('program_id' => $params['programId']));
            $memcache->set($key, $program, 86400);
        }
       


      $key = sprintf('getRecruitmentProgram.event.%d.%s', $params['event_id'], $params['program_type']);
      $memcache = My_Memcache::getInstance();
      $result = $memcache->get($key);
      if (!$result){
       $result = Api_Erp::getProgramEvent($params);
       $memcache->set($key, $result, 86400);
      }

       $has_ques = false;
       if(isset($result->part_ids) && count($result->part_ids) > 0) $has_ques = true;
       $fields = $result->field_ids;
       $renderform[] = array();
       //Div begin
       foreach ($fields as $key => $field) {
          switch ($field->field_type) {
            case 'char':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'interger':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'float':
              $renderform[] = My_View_Register::textboxfloat($field);
              break;
            case 'text':
              $renderform[] = My_View_Register::textbox($field);
              break;
            case 'datetime':
              $renderform[] = My_View_Register::datetime($field);
              break;
            case 'time':
              $renderform[] = My_View_Register::time($field);
              break;
            case 'binary':
              $renderform[] = My_View_Register::file($field);
              break;
            case 'selection':
              $renderform[] = My_View_Register::selectbox($field);
              break;
            case 'many2one':
              $renderform[] = My_View_Register::selectbox($field);
              break;
            case 'many2many':
              $renderform[] = My_View_Register::checkbox($field);
              break;
          }
       }


       //$renderform[] = My_View_Register::captcha();
       //Program_type
        $renderform[] = My_View_Register::program_event_id($params['event_id']);
        //Event Name
        $renderform[] = My_View_Register::event_name($result->name);

        //Button regiter
        $renderform[] = My_View_Register::button_submit($has_ques);
        //End
       $result->renderform = $renderform;


       if($has_ques){
          $questions = $result->part_ids;
          $questionform[] = array();
            $questionform[] = My_View_Register::title('Phần câu hỏi');
             foreach ($questions as $i => $part) {
                $questionform[] = My_View_Register::part($part);
                foreach ($part->question_ids as $j => $question) {
                  $key = $j+1;

                  switch ($question->question_type_code) {
                    case 'YESNO':
                      $questionform[] = My_View_Register::selectquestion($question, $key);
                      break;
                    case 'MULTICHOICE_MA':
                      $questionform[] = My_View_Register::selectquestion($question, $key);
                      break;
                    case 'MULTICHOICE_RICHTEXT':
                      $questionform[] = My_View_Register::checkboxrichtextquestion($question, $key);
                      break;
                    case 'MULTICHOICE':
                      $questionform[] = My_View_Register::checkboxquestion($question, $key);
                      break;
                    case 'TEXT':
                      $questionform[] = My_View_Register::textareaquestion($question, $key);
                      break;
                  }
               }
            }

          $questionform[] = My_View_Register::button_back();
          $result->questionform = $questionform;
       }
     $this->view->program = $program;
     $this->view->event = $result;
  }





}
