<?php

class Default_AjaxController extends Zend_Controller_Action {

	public function init() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_request->setActionName('index');
	}

	public function indexAction() {

		$params = $this->getRequest()->getParams();

		if (!isset($params['method'])) die('Request Invalid');

		list($object, $function) = explode('.', $params['method'], 2);

		$path = dirname(__FILE__);


		$file    = "$path/ajax/$object.php";

		if (!file_exists($file)) die('Object not found');

		require_once $file;

		$controller = "Default_Ajax_$object";

		if (!class_exists($controller)) die('Class not found');

		$service = new $controller($object, $this);

		if (!method_exists($service, $function)) die(0); // Function not found

		unset($params['method']);
		unset($params['module']);
		unset($params['action']);
		unset($params['controller']);

		$results = call_user_func(array($service, $function), $params);

		header('content-type: application/json; charset=utf-8');
		echo Zend_Json::encode($results);
		die;
	}

}
